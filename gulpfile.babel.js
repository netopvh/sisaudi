import gulp from 'gulp';
import babel from 'gulp-babel';
import less from 'gulp-less';
import concat from 'gulp-concat';
import rename from 'gulp-rename';
import cleanCss from 'gulp-clean-css';
import uglify from 'gulp-uglify-es';
import autoprefixer from 'gulp-autoprefixer'
import sourcemaps from 'gulp-sourcemaps';
import browserify from 'browserify';
import babelify from 'babelify';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';

let path = 'resources/assets/';

let styleDist = './public/css/';
let styleWatch = path + 'less/_main_full/**/*.less';

let jsDist = './public/js/';
const jsFiles = [
    path + 'js/actions/core.js',
    path + 'js/actions/app.js',
    path + 'js/actions/access/register.js',
    path + 'js/actions/access/users.js',
    path + 'js/actions/access/roles.js',
    path + 'js/actions/access/permissions.js',
    path + 'js/actions/access/departamentos.js',
    path + 'js/actions/access/audit.js',
    path + 'js/actions/audit/fonte_informacao.js',
    path + 'js/actions/audit/fundamentacao_legal.js',
    path + 'js/actions/audit/possivel_achado.js',
    path + 'js/actions/audit/unidades_administrativas.js',
    path + 'js/actions/audit/tipo_documentos.js',
    path + 'js/actions/audit/follow_up_status.js',
    path + 'js/actions/audit/recomendacao_categorias.js',
    path + 'js/actions/audit/recomendacao_gravidade.js',
    path + 'js/actions/audit/recomendacao_tipos.js',
    path + 'js/actions/audit/riscos.js',
    path + 'js/actions/audit/processo_tipo.js',
    path + 'js/actions/audit/processo/main.js',
    path + 'js/actions/audit/matriz_plan.js',
    path + 'js/actions/audit/auditorias.js',
    path + 'js/actions/audit/demanda.js',
    path + 'js/actions/audit/demanda_file.js'
];


gulp.task('less', function () {
    return gulp.src(styleWatch)
        .pipe(sourcemaps.init())
        .pipe(less({
            compress: true
        }))
        .on('error', console.error.bind(console))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(concat('all.min.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(styleDist))
});

gulp.task('icons', function () {
    return gulp.src([
        path + 'icons/icomoon/styles.css',
    ])
        .pipe(rename("icons.min.css"))
        .pipe(cleanCss())
        .pipe(gulp.dest('public/css'));
});

gulp.task('bundle', function () {
    return gulp.src([
        path + 'js/plugins/loaders/pace.min.js',
        path + 'js/core/libraries/jquery.js',
        path + 'js/core/libraries/sugar.js',
        path + 'js/core/libraries/jquerymy.js',
        path + 'js/core/libraries/bootstrap.min.js',
        path + 'js/plugins/loaders/blockui.min.js',
        path + 'js/plugins/ui/easyui/jquery.easyui.min.js',
        path + 'js/plugins/ui/easyui/locale/easyui-lang-pt_BR.js',
        path + 'js/core/libraries/jquery_ui/jquery-ui.js',
        path + 'js/core/libraries/jquery_ui/i18n/datepicker-pt-BR.js',
        path + 'js/core/libraries/jquery_ui/globalize/globalize.js',
        path + 'js/core/libraries/jquery_ui/globalize/cultures/globalize.culture.pt-BR.js',
        path + 'js/plugins/ui/nicescroll.min.js',
        path + 'js/plugins/forms/inputs/duallistbox.min.js',
        path + 'js/plugins/ui/moment/moment.min.js',
        path + 'js/plugins/ui/drilldown.js',
        path + 'js/plugins/forms/mask/inputmask/jquery.inputmask.bundle.js',
        path + 'js/plugins/notifications/sweet_alert.min.js',
        path + 'js/plugins/forms/wizards/form_wizard/form.min.js',
        path + 'js/plugins/forms/wizards/form_wizard/form_wizard.min.js',
        path + 'js/plugins/forms/styling/uniform.min.js',
        path + 'js/plugins/forms/selects/select2.min.js',
        path + 'js/plugins/tables/datatables/datatables.min.js',
        path + 'js/plugins/tables/datatables/extensions/responsive.min.js',
        path + 'js/plugins/tables/datatables/extensions/buttons.min.js',
        path + 'js/plugins/forms/validation/validate.min.js',
        path + 'js/plugins/forms/validation/localization/messages_pt_BR.js',
        path + 'js/plugins/trees/jstree.js',
        path + 'js/plugins/editors/jquery.classyedit.js',
        path + 'js/plugins/forms/selects/jquery.multiselect.js',
        path + 'js/plugins/forms/selects/jquery.multiselect.filter.js',
        path + 'js/plugins/forms/selects/jquery.multiselect.br.js',
        path + 'js/plugins/trees/fancytree_all.min.js',
        path + 'js/plugins/trees/fancytree_childcounter.js'
    ])
        .pipe(concat('bundle.min.js'))
        //.pipe(uglify())
        .pipe(gulp.dest(jsDist))
});

gulp.task('app', () => {
    return gulp.src(jsFiles)
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(concat('app.min.js'))
        //.pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(jsDist))
});

gulp.task('images', function () {
    return gulp.src([
        path + 'images/**/**'
    ])
        .pipe(gulp.dest('public/images'));
});

gulp.task('ckeditor', function () {
    return gulp.src([
        PATH_JS + 'plugins/editors/ckeditor/**/**'
    ])
        .pipe(gulp.dest('public/js/ckeditor'));
});

gulp.task('fonts', function () {
    return gulp.src([
        path + 'js/icons/fontawesome/fonts/*',
        path + 'js/icons/glyphicons/*',
        path + 'js/icons/icomoon/fonts/*',
        path + 'js/icons/summernote/*'
    ])
        .pipe(gulp.dest('public/fonts'));
});

//mix.copy('resources/assets/template/js/','public/js/layout.js');
//mix.copy('resources/assets/template/icons/icomoon/styles.css','public/css/icomoon.min.css');


//gulp.task('default', ['bundle','app']);
gulp.task('default',gulp.parallel('app','bundle'),() =>{
    gulp.watch(path + 'js/action/**/**/*.js',gulp.parallel('app','bundle'))
});