<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LaratrustSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return  void
     */
    public function run()
    {
        $this->command->info('Limpando Tabela de Usuarios, Grupos, Perfis e Permissões');

        $this->command->info('Criando Grupos');

        $grupos = [
            ['name' => 'Administração','parent_id' => null],
            ['name' => 'Usuários','parent_id' => 1],
            ['name' => 'Perfis','parent_id' => 1],
            ['name' => 'Permissões','parent_id' => 1],
            ['name' => 'Auditoria','parent_id' => 1],
            ['name' => 'Tabelas Básicas','parent_id' => null], //6
            ['name' => 'Tipo Recomendação','parent_id' => 6], //7
            ['name' => 'Categoria Recomendação','parent_id' => 6], //8
            ['name' => 'Gravidade Recomendação','parent_id' => 6], //9
            ['name' => 'Status Acompanhamento','parent_id' => 6], //10
            ['name' => 'Possíveis Achados','parent_id' => 6], //11
            ['name' => 'Tipo Documentos','parent_id' => 6], //12
            ['name' => 'Unidades Administrativas','parent_id' => 6], //13
            ['name' => 'Departamentos','parent_id' => 6], //14
            ['name' => 'Processos Internos','parent_id' => null],//15
            ['name' => 'Planejamento','parent_id' => null],//16
            ['name' => 'Análise Preliminar','parent_id' => 16], //17
            ['name' => 'Processos Auditáveis','parent_id' => 16], //18
            ['name' => 'Matriz de Planejamento','parent_id' => 16], //19
            ['name' => 'Execução','parent_id' => null], //20
            ['name' => 'Auditorias','parent_id' => 20], //21
            ['name' => 'Acompanhamento','parent_id' => null], //22
            ['name' => 'Relatórios','parent_id' => null], //23
            ['name' => 'Relatórios de Auditoria','parent_id' => 23], //24
            ['name' => 'Relatórios de Manifestações','parent_id' => 23], //25
            ['name' => 'Acompanhamento Pendências','parent_id' => 23], //26

        ];

        foreach ($grupos as $grupo) {
            \App\Domains\Access\Models\PermissionGroup::create([
                'name' => $grupo['name'],
                'parent_id' => $grupo['parent_id']
            ]);
        }

        $permissoes = [
            ['name' => 'ver-administracao', 'display_name' => 'Visualizar', 'group_id' => 1],
            ['name' => 'ver-usuario', 'display_name' => 'Visualizar', 'group_id' => 2],
            ['name' => 'criar-usuario', 'display_name' => 'Criar', 'group_id' => 2],
            ['name' => 'editar-usuario', 'display_name' => 'Editar', 'group_id' => 2],
            ['name' => 'desativar-usuario', 'display_name' => 'Desativar', 'group_id' => 2],
            ['name' => 'ver-perfil', 'display_name' => 'Visualizar', 'group_id' => 3],
            ['name' => 'criar-perfil', 'display_name' => 'Criar', 'group_id' => 3],
            ['name' => 'editar-perfil', 'display_name' => 'Editar', 'group_id' => 3],
            ['name' => 'ver-permissoes', 'display_name' => 'Visualizar', 'group_id' => 4],
            ['name' => 'criar-permissoes', 'display_name' => 'Criar', 'group_id' => 4],
            ['name' => 'editar-permissoes', 'display_name' => 'Editar', 'group_id' => 4],
            ['name' => 'ver-auditoria', 'display_name' => 'Visualizar', 'group_id' => 5],
            ['name' => 'ver-tipo-recomendacao', 'display_name' => 'Visualizar', 'group_id' => 7],
            ['name' => 'criar-tipo-recomendacao', 'display_name' => 'Criar', 'group_id' => 7],
            ['name' => 'editar-tipo-recomendacao', 'display_name' => 'Editar', 'group_id' => 7],
            ['name' => 'remover-tipo-recomendacao', 'display_name' => 'Excluir', 'group_id' => 7],
            ['name' => 'ver-categoria-recomendacao', 'display_name' => 'Visualizar', 'group_id' => 8],
            ['name' => 'criar-categoria-recomendacao', 'display_name' => 'Criar', 'group_id' => 8],
            ['name' => 'editar-categoria-recomendacao', 'display_name' => 'Editar', 'group_id' => 8],
            ['name' => 'remover-categoria-recomendacao', 'display_name' => 'Excluir', 'group_id' => 8],
            ['name' => 'ver-gravidade-recomendacao', 'display_name' => 'Visualizar', 'group_id' => 9],
            ['name' => 'criar-gravidade-recomendacao', 'display_name' => 'Criar', 'group_id' => 9],
            ['name' => 'editar-gravidade-recomendacao', 'display_name' => 'Editar', 'group_id' => 9],
            ['name' => 'remover-gravidade-recomendacao', 'display_name' => 'Excluir', 'group_id' => 9],
            ['name' => 'ver-status-acompanhamento', 'display_name' => 'Visualizar', 'group_id' => 10],
            ['name' => 'criar-status-acompanhamento', 'display_name' => 'Criar', 'group_id' => 10],
            ['name' => 'editar-status-acompanhamento', 'display_name' => 'Editar', 'group_id' => 10],
            ['name' => 'remover-status-acompanhamento', 'display_name' => 'Excluir', 'group_id' => 10],
            ['name' => 'ver-possiveis-achados', 'display_name' => 'Visualizar', 'group_id' => 11],
            ['name' => 'criar-possiveis-achados', 'display_name' => 'Criar', 'group_id' => 11],
            ['name' => 'editar-possiveis-achados', 'display_name' => 'Editar', 'group_id' => 11],
            ['name' => 'remover-possiveis-achados', 'display_name' => 'Excluir', 'group_id' => 11],
            ['name' => 'ver-tipo-documentos', 'display_name' => 'Visualizar', 'group_id' => 12],
            ['name' => 'criar-tipo-documentos', 'display_name' => 'Criar', 'group_id' => 12],
            ['name' => 'editar-tipo-documentos', 'display_name' => 'Editar', 'group_id' => 12],
            ['name' => 'remover-tipo-documentos', 'display_name' => 'Excluir', 'group_id' => 12],
            ['name' => 'ver-unidades', 'display_name' => 'Visualizar', 'group_id' => 13],
            ['name' => 'criar-unidades', 'display_name' => 'Criar', 'group_id' => 13],
            ['name' => 'editar-unidades', 'display_name' => 'Editar', 'group_id' => 13],
            ['name' => 'remover-unidades', 'display_name' => 'Excluir', 'group_id' => 13],
            ['name' => 'ver-departamentos', 'display_name' => 'Visualizar', 'group_id' => 14],
            ['name' => 'criar-departamentos', 'display_name' => 'Criar', 'group_id' => 14],
            ['name' => 'editar-departamentos', 'display_name' => 'Editar', 'group_id' => 14],
            ['name' => 'remover-departamentos', 'display_name' => 'Excluir', 'group_id' => 14],
            ['name' => 'ver-processos-auditaveis', 'display_name' => 'Visualizar', 'group_id' => 18],
            ['name' => 'criar-processos-auditaveis', 'display_name' => 'Criar', 'group_id' => 18],
            ['name' => 'editar-processos-auditaveis', 'display_name' => 'Editar', 'group_id' => 18],
            ['name' => 'ver-matriz', 'display_name' => 'Visualizar', 'group_id' => 19],
            ['name' => 'criar-matriz', 'display_name' => 'Criar', 'group_id' => 19],
            ['name' => 'editar-matriz', 'display_name' => 'Editar', 'group_id' => 19],
            ['name' => 'remover-matriz', 'display_name' => 'Remover', 'group_id' => 19],
        ];

        $permissions = [];

        $this->command->info('Criando Permissões');
        foreach ($permissoes as $permissao) {
            $permissions[] = \App\Domains\Access\Models\Permission::firstOrCreate([
                'name' => $permissao['name'],
                'display_name' => $permissao['display_name'],
                'group_id' => $permissao['group_id']
            ])->id;
        }

        $this->command->info('Criando Perfil Super Administrador');
        $roleSuper = \App\Domains\Access\Models\Role::create([
            'name' => 'Super Administrador',
            'display_name' => 'Super Administrador',
            'description' => 'Administrador Máximo do Sistema'
        ]);

        $this->command->info('Criando Perfil Gerente');
        $roleGerente = \App\Domains\Access\Models\Role::create([
            'name' => 'Gerente',
            'display_name' => 'Gerente',
            'description' => 'Gerente do Sistema'
        ]);

        $this->command->info('Usuário');
        $roleUser = \App\Domains\Access\Models\Role::create([
            'name' => 'Usuário',
            'display_name' => 'Usuário Comum',
            'description' => 'Usuário padrão do Sistema'
        ]);

        $this->command->info('Atribuindo Permissões');
        $roleSuper->permissions()->sync($permissions);
        $roleGerente->permissions()->sync($permissions);
        $roleUser->permissions()->sync($permissions);

        $departamentos = [
            'DAD','DEA','DOP','DGT','DRF','ASTEC','GABINETE CONTROLADOR','PROTOCOLO'
        ];

        $this->command->info('Criando Departamentos');

        foreach ($departamentos as $departamento) {
            \App\Domains\Access\Models\Departamento::create(['descricao' => $departamento]);
        }

        $this->command->info('Criando Usuários');
        $user = \App\Domains\Access\Models\User::create([
            'nome' => 'Angelo Neto',
            'cpf' => '00545841240',
            'username' => '302547',
            'email' => 'netopvh@gmail.com',
            'password' => bcrypt('123456'),
            'remember_token' => str_random(10),
            'funcao' => 1,
            'departamento_id' => 2,
            'active' => 1
        ]);

        $this->command->info('Atribuindo Perfil ao Usuário');
        $user->attachRole($roleSuper);
    }

    /**
     * Truncates all the laratrust tables and the users table
     *
     * @return    void
     */
    public function truncateLaratrustTables()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('permission_role')->truncate();
        DB::table('permission_user')->truncate();
        DB::table('role_user')->truncate();
        \App\Domains\Access\Models\User::truncate();
        \App\Domains\Access\Models\Role::truncate();
        \App\Domains\Access\Models\Permission::truncate();
        \App\Domains\Access\Models\PermissionGroup::truncate();
        Schema::enableForeignKeyConstraints();
    }
}
