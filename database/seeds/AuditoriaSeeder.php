<?php

use Illuminate\Database\Seeder;

class AuditoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $unidades = [
            ['nome' => 'CONTROLADORIA GERAL DO MUNICÍPIO','sigla' => 'CGM','adm_direta' => 1],
            ['nome' => 'EMPRESA DE DESENVOLVIMENTO URBANO','sigla' => 'EMDUR','adm_direta' => 1],
            ['nome' => 'FUNDAÇÃO CULTURAL','sigla' => 'FUNCULTURAL','adm_direta' => 1],
            ['nome' => 'FUNDAÇÃO CULTURAL','sigla' => 'FUNCULTURAL','adm_direta' => 1],
            ['nome' => 'SECRETARIA GERAL DE GOVERNO','sigla' => 'SGG','adm_direta' => 1],
            ['nome' => 'COORDENARIA MUNICIPAL DE TECNOLOGIA DA INFORMAÇÃO','sigla' => 'CMTI','adm_direta' => 1],
            ['nome' => 'SUPERINTENDÊNCIA DE GASTOS PÚBLICOS','sigla' => 'SGP','adm_direta' => 1],
            ['nome' => 'INSTITUTO DE PREVIDÊNCIA E ASSISTÊNCIA DOS SERVIDORES','sigla' => 'IPAM','adm_direta' => 1],
            ['nome' => 'PROCURADORIA GERAL DO MUNICÍPIO','sigla' => 'PGM','adm_direta' => 1],
            ['nome' => 'SECRETARIA MUNICIPAL DE ADMINISTRAÇÃO','sigla' => 'SEMAD','adm_direta' => 1],
            ['nome' => 'SUPERINTENDÊNCIA MUNICIPAL DE LICITAÇÕES','sigla' => 'SML','adm_direta' => 1],
            ['nome' => 'SECRETARIA DE ASSISTÊNCIA SOCIAL E DA FAMÍLIA','sigla' => 'SEMASF','adm_direta' => 1],
            ['nome' => 'SECRETARIA DE MUNICIPAL DE EDUCAÇÃO','sigla' => 'SEMED','adm_direta' => 1],
            ['nome' => 'SECRETARIA DE MUNICIPAL DE ESPORTE','sigla' => 'SEMES','adm_direta' => 1],
            ['nome' => 'SECRETARIA MUNICIPAL DE FAZENDA','sigla' => 'SEMFAZ','adm_direta' => 1],
            ['nome' => 'SECRETARIA MUNICIPAL DE SAÚDE','sigla' => 'SEMUSA','adm_direta' => 1],
            ['nome' => 'SECRETARIA MUNICIPAL DE INTEGRAÇÃO','sigla' => 'SEMI','adm_direta' => 1],
            ['nome' => 'SUBSECRETARIA MUNICIPAL DE MEIO AMBIENTE','sigla' => 'SEMA','adm_direta' => 1],
            ['nome' => 'SUBSECRETARIA MUNICIPAL DE AGRICULTURA E ABASTECIMENTO','sigla' => 'SEMAGRIC','adm_direta' => 1],
            ['nome' => 'SUBSECRETARIA MUNICIPAL DA INDÚSTRIA, COMÉRCIO TURISMO E TRABALHO','sigla' => 'SEMDESTUR','adm_direta' => 1],
            ['nome' => 'SECRETARIA MUNICIPAL DE INFRAESTRUTURA URBANA E SERVIÇOS BÁSICOS','sigla' => 'SEMISB','adm_direta' => 1],
            ['nome' => 'SUBSECRETARIA MUNICIPAL DE OBRAS E PAVIMENTAÇÃO','sigla' => 'SUOP','adm_direta' => 1],
            ['nome' => 'SUBSECRETARIA MUNICIPAL DE SERVIÇOS BÁSICOS','sigla' => 'SEMUSB','adm_direta' => 1],
            ['nome' => 'SECRETARIA MUNICIPAL DE PLANEJAMENTO, ORÇAMENTO E GESTÃO','sigla' => 'SEMPOG','adm_direta' => 1],
            ['nome' => 'SECRETARIA MUNICIPAL DE RESOLUÇÃO ESTRATÉGICA DE CONVÊNIOS E CONTRATOS','sigla' => 'SEMESC','adm_direta' => 1],
            ['nome' => 'SECRETARIA MUNICIPAL DE TRÃNSITO','sigla' => 'SEMTRAM','adm_direta' => 1],
            ['nome' => 'SECRETARIA MUNICIPAL  DE REGULARIZAÇÃO FUNDIÁRIA E HABITAÇÃO URBANISMO','sigla' => 'SEMUR','adm_direta' => 1],
        ];

        foreach ($unidades as $unidade) {
            \App\Domains\Auditoria\Models\UnidadeAdministrativa::create([
                'nome' => $unidade['nome'],
                'sigla' => $unidade['sigla'],
                'adm_direta' => $unidade['adm_direta'],
            ]);
        }

        \App\Domains\Auditoria\Models\Configuracao::create([
            'item' => 'num_processo_atual',
            'valor' => '100'
        ]);
    }
}
