<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDemandaFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasColumn('demanda_files', 'prazo')) {
            Schema::table('demanda_files',function (Blueprint $table){
                //$table->dropColumn('prazo');
                $table->integer('prazo')->after('conteudo')->default(15);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demanda_files');
    }
}
