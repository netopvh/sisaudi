<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('numero_processo');
            $table->date('autuacao');
            $table->enum('tipo_processo',\App\Domains\Auditoria\Enum\ProcessoTipoEnum::keys());
            $table->text('assunto');
            $table->char('auditoria_obrigatoria',1)->nullable();
            $table->date('inicio_proposto')->nullable();
            $table->date('fim_proposto')->nullable();
            $table->string('num_portaria_inicial')->nullable();
            $table->date('data_portaria_inicial')->nullable();
            $table->enum('status',\App\Domains\Auditoria\Enum\ProcessoStatusEnum::keys())->default('E');
            $table->unsignedInteger('gestor_id')->nullable(0);
            $table->unsignedInteger('departamento_id');
            $table->unsignedInteger('criador_id');
            $table->char('tipo',1);
            $table->timestamps();

            $table->index(["gestor_id"]);
            $table->index(["departamento_id"]);
            $table->index(["criador_id"]);

            $table->foreign('gestor_id')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('departamento_id')
                ->references('id')->on('departamentos')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('criador_id')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processos');
    }
}
