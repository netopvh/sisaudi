<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateDemandasTable.
 */
class CreateDemandasTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('demandas', function(Blueprint $table) {
            $table->increments('id');
            $table->date('autuacao');
            $table->string('assunto');
            $table->enum('tipo_demanda',\App\Domains\Auditoria\Enum\DemandaTipoEnum::keys());
            $table->char('procedencia',1);
            $table->unsignedInteger('departamento_id');
            $table->unsignedInteger('orgao_id')->nullable();
            $table->unsignedInteger('criador_id');
            $table->timestamps();

            $table->foreign('departamento_id')->references('id')->on('departamentos');
            $table->foreign('orgao_id')->references('id')->on('unidades_administrativas');
            $table->foreign('criador_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('demandas');
	}
}
