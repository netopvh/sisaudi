<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessoMatrizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processo_matriz', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('processo_id');
            $table->unsignedInteger('questao_id')->nullable();
            $table->text('questao')->nullable();
            $table->char('subquestao',1)->nullable();
            $table->text('risco')->nullable();
            $table->text('procedimento')->nullable();
            $table->text('criterio')->nullable();
            $table->text('informacoes_requeridas')->nullable();
            $table->text('fonte_informacao')->nullable();
            $table->text('limitacoes')->nullable();
            $table->text('possiveis_achados')->nullable();
            $table->timestamps();

            $table->index(["processo_id"]);

            $table->foreign('processo_id')
                ->references('id')->on('processos')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('questao_id')
                ->references('id')->on('processo_matriz')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processo_matriz');
    }
}
