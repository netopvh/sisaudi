<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessoUnidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processo_unidades', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('processo_id');
            $table->unsignedInteger('unidade_administrativa_id');

            $table->foreign('processo_id')
                ->references('id')->on('processos')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('unidade_administrativa_id')
                ->references('id')->on('unidades_administrativas')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processo_unidades');
    }
}
