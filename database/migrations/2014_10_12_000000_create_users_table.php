<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descricao');
            $table->timestamps();
            $table->softDeletes();
        });

        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('username');
                $table->string('cpf');
                $table->string('nome');
                $table->string('email')->nullable()->unique();
                $table->string('password');
                $table->boolean('active')->default(false);
                $table->integer('departamento_id')->unsigned();
                $table->foreign('departamento_id')->references('id')->on('departamentos');
                $table->integer('funcao')->nullable();
                $table->char('api_token',60)->nullable();
                $table->rememberToken();
                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('departamentos');
    }
}
