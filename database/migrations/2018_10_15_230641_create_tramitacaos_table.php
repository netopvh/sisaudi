<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateTramitacaosTable.
 */
class CreateTramitacaosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tramitacaos', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('demanda_id')->nullable();
            $table->unsignedInteger('processo_id')->nullable();
            $table->string('descricao');
            $table->date('data');
            $table->char('tipo',1);
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('origem_id')->nullable();
            $table->unsignedInteger('destino_id')->nullable();
            $table->timestamps();

            $table->foreign('demanda_id')->references('id')->on('demandas');
            $table->foreign('processo_id')->references('id')->on('processos');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('origem_id')->references('id')->on('departamentos');
            $table->foreign('destino_id')->references('id')->on('departamentos');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tramitacaos');
	}
}
