<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDemandaFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demanda_files', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('demanda_id');
            $table->unsignedInteger('tipo_doc_id');
            $table->char('tipo_file',1);
            $table->string('descricao')->nullable();
            $table->string('path')->nullable();
            $table->longText('conteudo')->nullable();
            $table->boolean('arquivado')->default(false);
            $table->integer('prazo')->default(15);
            $table->timestamps();

            $table->foreign('demanda_id')->references('id')->on('demandas');
            $table->foreign('tipo_doc_id')->references('id')->on('tipo_documentos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demanda_files');
    }
}
