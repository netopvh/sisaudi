@extends('layouts.app')

@section('content')
    <div class="page-container">

        <div class="page-content">

            <div class="content-wrapper">

                <div class="text-center content-group">
                    <h1 class="error-title">503</h1>
                    <h3>A aplicação está em manutenção, por favor aguarde que em breve estará em funcionamento normal!</h3>
                </div>

            </div>

        </div>

    </div>
@endsection