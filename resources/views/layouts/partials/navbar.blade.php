<div class="navbar navbar-inverse navbar-fixed-top bg-blue">
    <div class="navbar-header">
        <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('images/logo_light.png') }}" alt=""></a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <p class="navbar-text"><span class="label bg-success-400">Online</span></p>
    </div>
</div>