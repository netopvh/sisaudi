@auth
<div class="navbar navbar-default navbar-second-fixed-top"  id="navbar-second">
    <ul class="nav navbar-nav no-border visible-xs-block">
        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-second-toggle">
        <ul class="nav navbar-nav navbar-nav-material">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                     <i class="icon-table2"></i> Tabelas Básicas <span class="caret"></span>
                </a>
                <ul class="dropdown-menu width-400" style="width: 230px">
                    <li class="dropdown-submenu dropdown-submenu-hover">
                        <a href="#">
                            <i class="icon-book"></i>
                            <span class="text-bold">Recomendação</span>
                        </a>
                        <ul class="dropdown-menu width-300" style="width: 200px;">
                            @permission('ver-tipo-recomendacao')
                            <li class="{{ active(['recomendacao_tipos.*']) }}">
                                <a href="{{ route('recomendacao_tipos.index') }}">
                                    <i class="icon-stack2"></i>
                                    <span class="text-bold">Tipo Recomendação</span>
                                </a>
                            </li>
                            @endpermission
                            @permission('ver-categoria-recomendacao')
                            <li class="{{ active(['recomendacao_categorias.*']) }}">
                                <a href="{{ route('recomendacao_categorias.index') }}">
                                    <i class="icon-menu2"></i>
                                    <span class="text-bold">Categoria</span>
                                </a>
                            </li>
                            @endpermission
                            @permission('ver-gravidade-recomendacao')
                            <li class="{{ active(['recomendacao_gravidades.*']) }}">
                                <a href="{{ route('recomendacao_gravidades.index') }}">
                                    <i class="icon-spam"></i>
                                    <span class="text-bold">Gravidade</span>
                                </a>
                            </li>
                            @endpermission
                        </ul>
                    </li>
                    <li class="dropdown-submenu dropdown-submenu-hover">
                        <a href="#">
                            <i class="icon-bookmark"></i>
                            <span class="text-bold">Análise de Risco</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="{{ active(['risco.*']) }}">
                                <a href="{{ route('risco.index') }}">
                                    <i class="icon-insert-template"></i>
                                    <span class="text-bold">Riscos da Auditoria</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @permission('ver-status-acompanhamento')
                    <li class="dropdown-submenu dropdown-submenu-hover">
                        <a href="#">
                            <i class="icon-file-check2"></i>
                            <span class="text-bold">Acompanhamento</span>
                        </a>
                        <ul class="dropdown-menu width-250">
                            <li class="{{ active(['follow_up_status.*']) }}">
                                <a href="{{ route('follow_up_status.index') }}">
                                    <i class="icon-file-upload2"></i>
                                    <span class="text-bold">Status do Acompanhamento</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endpermission
                    @permission('ver-possiveis-achados')
                    <li class="{{ active(['possivel_achado.*']) }}">
                        <a href="{{ route('possivel_achado.index') }}">
                            <i class="icon-price-tags"></i>
                            <span class="text-bold">Possíveis Achados</span>
                        </a>
                    </li>
                    @endpermission
                    @permission('ver-tipo-documentos')
                    <li class="{{ active(['tipo_documentos.*']) }}">
                        <a href="{{ route('tipo_documentos.index') }}">
                            <i class="icon-file-text"></i>
                            <span class="text-bold">Tipo de Documentos</span>
                        </a>
                    </li>
                    @endpermission
                    @permission('ver-unidades')
                    <li class="{{ active(['administrative_units.*']) }}">
                        <a href="{{ route('administrative_units.index') }}">
                            <i class="icon-portfolio"></i>
                            <span class="text-bold">Unidades Administrativas</span>
                        </a>
                    </li>
                    @endpermission
                    @permission('ver-departamentos')
                    <li class="{{ active(['departamentos.*']) }}">
                        <a href="{{ route('departamentos.index') }}">
                            <i class="icon-insert-template"></i>
                            <span class="text-bold">Departamentos</span>
                        </a>
                    </li>
                    @endpermission
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-folder-open2 position-left"></i> Acompanhamento de Demandas <span class="caret"></span>
                </a>
                <ul class="dropdown-menu width-250" style="width: 230px;">
                    <li class="{{ active(['demanda.*']) }}">
                        <a href="{{ route('demanda.index') }}">
                            <i class="icon-paste"></i>
                            <span class="text-bold">Minhas Demandas</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="#">
                            <i class="icon-folder-search"></i>
                            <span class="text-bold">Consultar Demandas</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-folder-open2 position-left"></i> Auditorias <span class="caret"></span>
                </a>
                <ul class="dropdown-menu width-250" style="width: 230px;">
                    <li class="dropdown-header">Planejamento</li>
                    <li class="">
                        <a href="#">
                            <i class="icon-files-empty"></i>
                            <span class="text-bold">Análise Preliminar</span>
                        </a>
                    </li>
                    @permission('ver-processos-auditaveis')
                    <li class="{{ active(['processo.index','processo.create','processo.edit','processo.show']) }}">
                        <a href="{{ route('processo.index') }}">
                            <i class="icon-folder-check"></i>
                            <span class="text-bold">Planejamento Específico</span>
                        </a>
                    </li>
                    @endpermission
                    <li class="dropdown-header">Execução</li>
                    <li class="{{ active(['auditoria.*']) }}">
                        <a href="{{ route('auditoria.index') }}">
                            <i class="icon-stack-up"></i>
                            <span class="text-bold">Auditorias</span>
                        </a>
                    </li>
                    <li class="dropdown-header">Acompanhamento</li>
                    <li class="{{ active(['acompanhamento.*']) }}">
                        <a href="{{ route('acompanhamento.index') }}">
                            <i class="icon-clipboard6 position-left"></i>
                            <span class="text-bold">Recomendações</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-files-empty2"></i>
                    <span class="text-bold">Relatórios</span> <span class="caret"></span>
                </a>
                <ul class="dropdown-menu width-300" style="width: 250px;">
                    <li class="">
                        <a href="">
                            <i class="icon-file-presentation2"></i>
                            <span class="text-bold">Relatórios Auditoria</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="">
                            <i class="icon-file-eye2"></i>
                            <span class="text-bold">Relatórios Manifestações</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="">
                            <i class="icon-stack-star"></i>
                            <span class="text-bold">Acompanhamento Pendencias</span>
                        </a>
                    </li>
                </ul>
            </li>
            @permission('ver-administracao')
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-cog2 position-left"></i> Administração <span class="caret"></span>
                </a>

                <ul class="dropdown-menu width-250">
                    <li class="dropdown-submenu dropdown-submenu-hover">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-file-locked"></i>
                            <span class="text-semibold">Gerenciamento de Acesso</span>
                        </a>
                        <ul class="dropdown-menu">
                            @permission('ver-usuario')
                            <li class="{{ active(['users.*']) }}">
                                <a href="{{ route('users.index') }}">
                                    <i class="icon-user"></i>
                                    <span class="text-bold">Usuários</span>
                                </a>
                            </li>
                            @endpermission
                            @permission('ver-perfil')
                            <li class="{{ active(['roles.*']) }}">
                                <a href="{{ route('roles.index') }}">
                                    <i class="icon-users4"></i>
                                    <span class="text-bold">Perfil de Acesso</span>
                                </a>
                            </li>
                            @endpermission
                            @permission('ver-permissoes')
                            <li class="{{ active(['permissions.*']) }}">
                                <a href="{{ route('permissions.index') }}">
                                    <i class="icon-shield-notice"></i>
                                    <span class="text-bold">Permissões</span>
                                </a>
                            </li>
                            @endpermission
                        </ul>
                    </li>
                    <li>
                        <a href="">
                            <i class="icon-cog3"></i>
                            <span class="text-semibold">Parâmetros do Sistema</span>
                        </a>
                    </li>
                    @permission('ver-auditoria')
                    <li class="{{ active(['audit.*']) }}">
                        <a href="{{ route('audit.index') }}">
                            <i class="icon-stack-star"></i>
                            <span class="text-semibold">Auditoria</span>
                        </a>
                    </li>
                    @endpermission
                </ul>
            </li>
            @endpermission
        </ul>
        <ul class="nav navbar-nav navbar-right">
            @guest
            <li><a href="{{ route('login') }}">Login</a></li>
            @else
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <span>{{ auth()->user()->nome }}</span>
                        <i class="caret"></i>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-user-plus"></i> Meu perfil</a></li>
                        <li><a href="#"><span class="badge badge-warning pull-right">58</span> <i
                                        class="icon-comment-discussion"></i> Mensagens</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="icon-cog5"></i> Configurações de Conta</a></li>
                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="icon-switch2"></i> Sair do Sistema
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
                @endguest
        </ul>
    </div>
</div>
@endauth