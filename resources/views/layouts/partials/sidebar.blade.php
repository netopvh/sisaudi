@auth
<div class="sidebar sidebar-main sidebar-default sidebar-fixed">
    <div class="sidebar-content">
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">
                    <li class="navigation-header">
                        <span>Menu Principal</span>
                        <i class="icon-menu" title="Main pages"></i>
                    </li>
                    <li class="{{ active(['home']) }}">
                        <a href="{{ route('home') }}">
                            <i class="icon-home4"></i>
                            <span class="text-bold">Início</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="icon-folder-open2"></i>
                            <span class="text-bold">Planejamento</span>
                        </a>
                        <ul>
                            <li class="">
                                <a href="">
                                    <i class="icon-drawer3"></i>
                                    <span class="text-bold">Matriz de Planejamento</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="">
                                    <i class="icon-certificate"></i>
                                    <span class="text-bold">Matriz de Procedimentos</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <i class="icon-clipboard6"></i>
                            <span class="text-bold">Follow Up</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="icon-files-empty2"></i>
                            <span class="text-bold">Relatórios</span>
                        </a>
                        <ul>
                            <li class="{{ active(['processo.orgao','processo.orgao.*']) }}">
                                <a href="/processo/orgaos/">
                                    <i class="icon-file-presentation2"></i>
                                    <span class="text-bold">Relatórios Auditoria</span>
                                </a>
                            </li>
                            <li class="{{ active(['processo.departamento','processo.departamento.*']) }}">
                                <a href="/processo/departamento">
                                    <i class="icon-file-eye2"></i>
                                    <span class="text-bold">Relatórios Manifestações</span>
                                </a>
                            </li>
                            <li class="{{ active(['processo.departamento','processo.departamento.*']) }}">
                                <a href="/processo/departamento">
                                    <i class="icon-stack-star"></i>
                                    <span class="text-bold">Acompanhamento de Pendencias</span>
                                </a>
                            </li>
                            <li class="{{ active(['processo.departamento','processo.departamento.*']) }}">
                                <a href="/processo/departamento">
                                    <i class="icon-thumbs-up2"></i>
                                    <span class="text-bold">Avaliação do Auditor</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @permission('ver-administracao')
                    <li class="navigation-header">
                        <span>Menu Administrativo</span>
                        <i class="icon-menu" title="Main pages"></i>
                    </li>
                    <li>
                        <a href="#">
                            <i class="icon-file-locked"></i>
                            <span class="text-bold">Gerenciamento de Acesso</span>
                        </a>
                        <ul>
                            @permission('ver-usuario')
                            <li class="{{ active(['users.*']) }}">
                                <a href="{{ route('users.index') }}">
                                    <i class="icon-user"></i>
                                    <span class="text-bold">Usuários</span>
                                </a>
                            </li>
                            @endpermission
                            @permission('ver-perfil')
                            <li class="{{ active(['roles.*']) }}">
                                <a href="{{ route('roles.index') }}">
                                    <i class="icon-users4"></i>
                                    <span class="text-bold">Perfil de Acesso</span>
                                </a>
                            </li>
                            @endpermission
                            @permission('ver-permissoes')
                            <li class="{{ active(['permissions.*']) }}">
                                <a href="{{ route('permissions.index') }}">
                                    <i class="icon-shield-notice"></i>
                                    <span class="text-bold">Permissões</span>
                                </a>
                            </li>
                            @endpermission
                        </ul>
                    </li>
                    <li>
                        <a href="">
                            <i class="icon-cog"></i>
                            <span class="text-bold">Parâmetros do Sistema</span>
                        </a>
                        <ul>
                            <li class="{{ active(['roles.*']) }}">
                                <a href="{{ route('roles.index') }}">
                                    <i class="icon-book"></i>
                                    <span class="text-bold">Recomendação</span>
                                </a>
                                <ul>
                                    <li class="{{ active(['permissions.*']) }}">
                                        <a href="{{ route('permissions.index') }}">
                                            <i class="icon-stack2"></i>
                                            <span class="text-bold">Tipo Recomendação</span>
                                        </a>
                                    </li>
                                    <li class="{{ active(['permissions.*']) }}">
                                        <a href="{{ route('permissions.index') }}">
                                            <i class="icon-menu2"></i>
                                            <span class="text-bold">Categoria</span>
                                        </a>
                                    </li>
                                    <li class="{{ active(['permissions.*']) }}">
                                        <a href="{{ route('permissions.index') }}">
                                            <i class="icon-spam"></i>
                                            <span class="text-bold">Gravidade</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icon-bookmark"></i>
                                    <span class="text-bold">Análise de Risco</span>
                                </a>
                                <ul>
                                    <li class="">
                                        <a href="/processo/orgaos/">
                                            <i class="icon-portfolio"></i>
                                            <span class="text-bold">Critérios</span>
                                        </a>
                                        <ul>
                                            <li class="">
                                                <a href="">
                                                    <i class="icon-notebook"></i>
                                                    <span class="text-bold">Tipo de Critério</span>
                                                </a>
                                            </li>
                                            <li class="">
                                                <a href="">
                                                    <i class="icon-typewriter"></i>
                                                    <span class="text-bold">Gerenciar Critério</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="">
                                        <a href="/processo/departamento">
                                            <i class="icon-insert-template"></i>
                                            <span class="text-bold">Risco</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="">
                                <a href="">
                                    <i class="icon-file-check2"></i>
                                    <span class="text-bold">Follow Up</span>
                                </a>
                                <ul>
                                    <li class="">
                                        <a href="">
                                            <i class="icon-file-upload2"></i>
                                            <span class="text-bold">Status do Follow Up</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="">
                                <a href="">
                                    <i class="icon-price-tags"></i>
                                    <span class="text-bold">Categoria Relatório</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="">
                                    <i class="icon-paste3"></i>
                                    <span class="text-bold">Tipos de Auditoria</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="">
                                    <i class="icon-file-text"></i>
                                    <span class="text-bold">Tipo de Documentos</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="">
                                    <i class="icon-portfolio"></i>
                                    <span class="text-bold">Unidades Administrativas</span>
                                </a>
                            </li>
                            <li class="{{ active(['departamentos.*']) }}">
                                <a href="asd">
                                    <i class="icon-insert-template"></i>
                                    <span class="text-bold">Departamentos</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="">
                                    <i class="icon-copy2"></i>
                                    <span class="text-bold">Critérios Avaliação</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    @endpermission
                </ul>
            </div>
        </div>
    </div>
</div>
@endauth