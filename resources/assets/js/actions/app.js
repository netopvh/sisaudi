//Cria componente deixa letra maiuscula
$.fn.capitalize = function () {
    //palavras para ser ignoradas
    let wordsToIgnore = ["DA", "DO", "DE", "DAS", "DOS", "E", "A","da","do","de","das","dos"],
        minLength = 3;

    function getWords(str) {
        return str.match(/\S+\s*/g);
    }
    this.each(function () {
        let words = getWords(this.value);
        $.each(words, function (i, word) {
            // somente continua se a palavra nao estiver na lista de ignorados
            if (wordsToIgnore.indexOf($.trim(word)) == -1 && $.trim(word).length > minLength) {
                words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1).toLowerCase();
            }else{
                words[i] = words[i].toLowerCase();
            }
        });
        this.value = words.join("");
    });
};

function clearChildren(element) {
    element.find('input:text, input:password, textarea').val('');
    element.find('select').val('').trigger('change');
    return element;
}

function objToString(obj){
    let objeto = obj;
    let text = "";
    $.each(objeto, function (i, val) {
        text += val + "\n"
    });
    return text;
}

let getParams = function (url) {
    var params = {};
    var parser = document.createElement('a');
    parser.href = url;
    var query = parser.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        params[pair[0]] = decodeURIComponent(pair[1]);
    }
    return params;
};

function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

jQuery.validator.addMethod('ckrequired', function (value, element, params) {
    var idname = jQuery(element).attr('id');
    var messageLength =  jQuery.trim ( CKEDITOR.instances[idname].getData() );
    return !params  || messageLength.length !== 0;
}, "Este campo é requerido");

$(function () {

    //Select Personalizado
    $('.select').select2({
        minimumResultsForSearch: Infinity
    });
    //Select2 Personalizado com busca
    $('.select-search').select2({
        language: {
            errorLoading: function () {
                return 'Os resultados não puderam ser carregados.';
            },
            inputTooLong: function (args) {
                let overChars = args.input.length - args.maximum;

                let message = 'Apague ' + overChars + ' caracter';

                if (overChars != 1) {
                    message += 'es';
                }

                return message;
            },
            inputTooShort: function (args) {
                let remainingChars = args.minimum - args.input.length;
                return 'Digite ' + remainingChars + ' ou mais caracteres';
            },
            loadingMore: function () {
                return 'Carregando mais resultados…';
            },
            maximumSelected: function (args) {
                let message = 'Você só pode selecionar ' + args.maximum + ' ite';

                if (args.maximum == 1) {
                    message += 'm';
                } else {
                    message += 'ns';
                }

                return message;
            },
            noResults: function () {
                return 'Nenhum resultado encontrado';
            },
            searching: function () {
                return 'Buscando…';
            }
        }
    });

    $('.select-ui').selectmenu();

    //Input Field Personalizado
    $(".file-styled").uniform({
        fileButtonClass: 'action btn bg-blue',
        fileButtonHtml: 'Selecione os Arquivos',
        fileDefaultHtml: 'Nenhum arquivo foi selecionado'
    });

    //Input Radio Personalizado
    $(".styled, .multiselect-container input").uniform({
        radioClass: 'choice'
    });

    //Deixa todas as letras maisculas
    $(".upper").bind('keyup', function (e) {
        $(this).val(($(this).val()).toUpperCase());
    });

    //Primeira Letra Maiuscula
    $(".upper-first").bind('keyup', function (e) {
        $(this).capitalize();
    });

    //Somente Numeros
    $(".numbers").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))                    {
            return false;
        }
    });

    /**
     *  Efetua Validações de formulários
     */
    let validator = $(".form-validate").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function(error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container') ) {
                if(element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo( element.parent().parent().parent().parent() );
                }
                else {
                    error.appendTo( element.parent().parent().parent().parent().parent() );
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo( element.parent().parent().parent() );
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo( element.parent() );
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo( element.parent().parent() );
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo( element.parent().parent() );
            }

            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        rules: {
            password: {
                minlength: 6
            },
            repeat_password: {
                equalTo: "#password"
            },
            email: {
                email: true
            },
            repeat_email: {
                equalTo: "#email"
            },
            minimum_characters: {
                minlength: 10
            },
            maximum_characters: {
                maxlength: 10
            },
            minimum_number: {
                min: 10
            },
            maximum_number: {
                max: 10
            },
            number_range: {
                range: [10, 20]
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            date_iso: {
                dateISO: true
            },
            numbers: {
                number: true
            },
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            },
            basic_checkbox: {
                minlength: 2
            },
            styled_checkbox: {
                minlength: 2
            },
            switchery_group: {
                minlength: 2
            },
            switch_group: {
                minlength: 2
            }
        }
    });

    // Reset form
    $('#reset').on('click', function() {
        validator.resetForm();
    });

    $('form').submit(function () {
        if(! $('form').hasClass('form-validate')){
            $('button').attr('disabled',false);
        }else if(validator.valid()){
            $('button').attr('disabled',true);
        }
    });

    //Fix Datatable Width
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $($.fn.dataTable.tables(true)).css('width', '100%');
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw();
    });

    $('.datepicker').datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true
    }).inputmask('datetime',{
        inputFormat: "dd/mm/yyyy",
        showMaskOnHover: true,
        showMaskOnFocus: true
    });

    $(".multi-select").multiselect({
        minWidth: 500,
        listbox: true
    }).multiselectfilter({
        label: 'Filtro',
        placeholder: 'Item à Pesquisar'
    });

    if($('#editor').length){
        CKEDITOR.replace('editor', {
            height: 170,
            enterMode: CKEDITOR.ENTER_BR,
            extraPlugins: 'tableresize,tabletools,justify,imageresizerowandcolumn',
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        });
    }
});
let token = $('meta[name="api_token"]').attr('content');



//Define padrões para o jquery ajax
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        'Authorization': token
    },
    data: {
        api_token: token
    }
});

//Default authenticated token
$.extend($.fn.dataTable.defaults, {
    autoWidth: false,
    language: {
        "sEmptyTable": "Nenhum registro encontrado",
        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_ resultados por página",
        "sLoadingRecords": "Carregando...",
        "sProcessing": "Processando...",
        "sZeroRecords": "Nenhum registro encontrado",
        "sSearch": "",
        "oPaginate": {
            "sNext": "&rarr;",
            "sPrevious": "&larr;",
            "sFirst": "Primeiro",
            "sLast": "Último"
        },
        "oAria": {
            "sSortAscending": ": Ordenar colunas de forma ascendente",
            "sSortDescending": ": Ordenar colunas de forma descendente"
        }
    },
    dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
    drawCallback: function () {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
    },
    preDrawCallback: function () {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
    }
});

function alert_success(message){
    swal({
        showConfirmButton:true,
        type:"success",
        confirmButtonText:"OK",
        allowOutsideClick:false,
        title: message
    });
}

function alert_error(message) {
    swal({
        showConfirmButton:true,
        type:"error",
        confirmButtonText:"OK",
        allowOutsideClick:false,
        title: message
    });
}