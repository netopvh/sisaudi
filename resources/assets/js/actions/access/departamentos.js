document.addEventListener('DOMContentLoaded', function() {
//Caixas de Diálogo e Forms
    let dlgDepartaments = $('#dlgDepartaments');
    let frmDepartaments = $('#frmDepartaments');

//Botoes
    let btnNew = $('#newDepartament');

//Campos
    let txtDescricao = $('input[name="descricao"]');
    let txtAction = $('input:hidden[name="action"]');
    let txtId = $('input:hidden[name="item"]');

    let dtDepartaments = $('#tblDepartaments').DataTable({
        dom: "<'row'<'col-xs-12'f<'col-xs-12'>>r>" +
        "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        responsive: true,
        columnDefs: [
            {
                targets: 2,
                className: "text-center",
            }
        ],
        ajax: '/api/access/departaments',
        columns: [
            {data: 'id', name: 'id', width: '70px'},
            {data: 'descricao', name: 'descricao'},
            {data: 'action', orderable: false, searchable: false, width: '100px'}
        ],
        createdRow: function (nRow, aData, iDisplayIndex) {
            $(nRow).attr('data-id', aData.id)
        }
    });

    //Validação do Formulário
    frmDepartaments.validate({
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        }
    });

    //Configuração da Caixa de Diálogo
    dlgDepartaments.dialog({
        closed: true,
        modal: true,
        width: 400,
        buttons: [{
            text:'Gravar',
            handler:function(){
                if(frmDepartaments.valid()){
                    if(txtAction.val() === 'insert'){
                        $.ajax({
                            type: "POST",
                            url: '/api/access/departaments?api_token=' + $('meta[name="api_token"]').attr('content'),
                            data: frmDepartaments.serializeArray(),
                            success: function( response ) {
                                dlgDepartaments.dialog('close');
                                txtDescricao.val('');
                                alert_success(response);
                                dtDepartaments.ajax.reload( null, false );
                            },
                            error: function (response, error) {
                                dlgDepartaments.dialog('close');
                                txtDescricao.val('');
                                alert_error(response.responseJSON.descricao)
                            }
                        });
                    }else if(txtAction.val() === 'update'){
                        $.ajax({
                            type: "PATCH",
                            url: '/api/access/departaments/'+ txtId.val() + '?api_token=' + $('meta[name="api_token"]').attr('content'),
                            data: frmDepartaments.serializeArray(),
                            success: function( response ) {
                                dlgDepartaments.dialog('close');
                                txtDescricao.val('');
                                txtId.val('');
                                alert_success(response);
                                dtDepartaments.ajax.reload( null, false );
                            },
                            error: function (response, error) {
                                dlgDepartaments.dialog('close');
                                txtDescricao.val('');
                                txtId.val('');
                                alert_error(response.responseJSON.descricao);
                            }
                        });
                    }
                }
            }
        }, {
            text:'Cancelar',
            handler:function(){
                txtDescricao.val('');
                dlgDepartaments.dialog('close');
            }
        }]
    });

    //Ações dos Botões
    btnNew.on('click', function() {
        $('div.panel-title').text('Formulário de Cadastro');
        dlgDepartaments.dialog('open');
        txtAction.val('insert');
        txtDescricao.focus();
    });

    //Abre o Dialog de Edição
    dtDepartaments.on('click','#editReg', function (e) {
        txtAction.val('update');
        let id = $(this).closest('tr').attr('data-id');
        $.ajax({
            type: "GET",
            url: '/api/access/departaments/'+ id +'/edit?api_token=' + $('meta[name="api_token"]').attr('content'),
            success: function( response ) {
                txtDescricao.val(response.descricao);
                txtId.val(response.id);
                $('div.panel-title').text('Formulário de Alteração');
                dlgDepartaments.dialog('open');
                txtDescricao.focus();
            }
        });
        return false;
    });

    //Abre o Dialog de Confirmação de Remoção
    dtDepartaments.on('click','#deleteReg', function (e) {
        let id = $(this).closest('tr').attr('data-id');
        $.messager.confirm('Confirmação','Tem certeza que quer remover este registro?',function(r){
            if (r){
                $.ajax({
                    type: "DELETE",
                    url: '/api/access/departaments/'+ id +'?api_token=' + $('meta[name="api_token"]').attr('content'),
                    success: function( response ) {
                        alert_success(response);
                        dtDepartaments.ajax.reload( null, false );
                    }
                });
            }
        });
    });
});