$(function () {
        let optionsProcessoCreate = {
            height: 250,
            removePlugins: 'links,tools,about,document',
            removeButtons: 'Cut,Copy,Paste,Undo,Redo,Anchor,Underline,Scayt,Source,Strike,Subscript,Superscript,Image,Link,Unlink,Anchor,Maximize,SpecialChar',
            enterMode: CKEDITOR.ENTER_P,
            shiftEnterMode: CKEDITOR.ENTER_BR
        };
//DIVS E FORMS DO FORMULÁRIO

        let tbProcesso = $('#tabProcesso');
        let dvDemOrgProc = $('#DemandaOrgProc');

//FIELDS

        let txtOrgao = $('select[name="orgao_id"]');
        let rbProcedencia = $('input:radio[name="procedencia"]');


//DATATABLE DA PÁGINA INICIAL - INDEX
        let dtDemandas = $('#tblProcessInternal').DataTable({
            dom: "<'row'<'col-xs-12'<'col-xs-12'>>r>" +
                "<'row'<'col-xs-12't>>" +
                "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
            processing: true,
            serverSide: true,
            responsive: true,
            columnDefs: [
                {
                    targets: 3,
                    className: "text-center",
                }
            ],
            ajax: {
                url: '/api/audits/demands/',
                data: function (d) {
                    d.assunto = $('#demandaAssunto').val();
                    d.autuacao = $('#demandaAutuacao').val();
                    d.criador = $('#demandaCriador').val();
                }
            },
            columns: [
                {data: 'autuacao', name: 'demandas.autuacao', width: '120px'},
                {data: 'assunto', name: 'demandas.assunto', width: '350px'},
                {data: 'interessados', name: 'interessados.nome', width: '200px'},
                {data: 'criador', name: 'criador.nome', width: '150px'},
                {data: 'action', orderable: false, searchable: false, width: '15px'}
            ]
        });

        $('#demanda-search').on('submit',function (e) {
            e.preventDefault();
            dtDemandas.draw();
        });

        rbProcedencia.click(function () {
            if($(this).val() === 'I'){
                dvDemOrgProc.collapse('hide');
                rbProcedencia.attr('required',false);
                //rbProcedencia.val('').trigger('change');
            }else if($(this).val() === 'E'){
                dvDemOrgProc.collapse('show');
                rbProcedencia.attr('required',true);
            }
        });
    }

);

$(".tree-files").fancytree({
    init: function(event, data) {
        $('.has-tooltip .fancytree-title').tooltip();
    },
    focus: function(event, data) {
        let node = data.node;
        // Auto-activate focused node after 1 second
        if(node.data.href){
            node.scheduleAction("activate", 1000);
        }
    },
    activate: function(event, data){
        let node = data.node,
            orgEvent = data.originalEvent;

        if(node.data.href){
            //window.open(node.data.href, (orgEvent.ctrlKey || orgEvent.metaKey) ? "_blank" /*node.data.target*/ : node.data.target);
            window.location.href=node.data.href;
        }
    },
    click: function(event, data){ // allow re-loads
        let node = data.node,
            orgEvent = data.originalEvent;

        if(node.isActive() && node.data.href){
            // data.tree.reactivate();
            window.open(node.data.href, (orgEvent.ctrlKey || orgEvent.metaKey) ? "_blank" : node.data.target);
        }
    }
});