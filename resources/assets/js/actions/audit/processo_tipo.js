document.addEventListener('DOMContentLoaded', function () {
//Caixas de Diálogo e Forms
    let dlgProcessType = $('#dlgProcessType');
    let frmProcessType = $('#frmProcessType');

//Botoes
    let btnNewProcessType = $('#newProcessType');

    let manifestProcessType = {
        data: {
            id: "",
            descricao: "",
            modalidade: "",
            status: "",
        },
        ui: {
            "#id": {bind: "id"},
            "#descricao": {bind: "descricao"},
            "#modalidade": {bind: "modalidade"},
            "#status": {bind: "status"},
        }
    };

    //Campos
    let txtAction = $('input:hidden[name="action"]');
    let txtId = $('input:hidden[name="item"]');

    let dtProcessType = $('#tblProcessType').DataTable({
        dom: "<'row'<'col-xs-12'f<'col-xs-12'>>r>" +
        "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        responsive: true,
        columnDefs: [
            {
                targets: 3,
                className: "text-center",
            }
        ],
        ajax: '/api/audits/tables/process-type',
        columns: [
            {data: 'descricao', name: 'descricao'},
            {data: 'modalidade', name: 'modalidade'},
            {data: 'status', name: 'status', width: '110px', searchable: false},
            {data: 'action', orderable: false, searchable: false, width: '100px'}
        ],
        createdRow: function (nRow, aData, iDisplayIndex) {
            $(nRow).attr('data-id', aData.id)
        }
    });

    //Validação do Formulário
    let frmInstaceProcessType = frmProcessType.validate({
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        }
    });


    //Configuração da Caixa de Diálogo
    dlgProcessType.dialog({
        closed: true,
        modal: true,
        width: 400,
        buttons: [{
            text: 'Gravar',
            handler: function () {
                if (frmProcessType.valid()) {
                    if (txtAction.val() === 'insert') {
                        $.ajax({
                            type: "POST",
                            url: '/api/audits/tables/process-type',
                            data: frmProcessType.my("data")
                        }).done(function (response) {
                            dlgProcessType.dialog('close');
                            frmProcessType.my("remove");
                            dtProcessType.ajax.reload(null, false);
                            alert_success(response);
                        }).fail(function (response, error) {
                            dlgProcessType.dialog('close');
                            frmProcessType.my("remove");
                            if (Object.isObject(response.responseJSON)) {
                                alert_error(objToString(response.responseJSON));
                            } else {
                                alert_error(response.responseJSON);
                            }
                        })
                    } else if (txtAction.val() === 'update') {
                        $.ajax({
                            type: "PATCH",
                            url: '/api/audits/tables/process-type/' + txtId.val(),
                            data: frmProcessType.my("data")
                        }).done(function (response) {
                            dlgProcessType.dialog('close');
                            frmProcessType.my("remove");
                            txtId.val('');
                            alert_success(response);
                            dtProcessType.ajax.reload(null, false);
                        }).fail(function (response, error) {
                            dlgProcessType.dialog('close');
                            frmProcessType.my("remove");
                            if (Object.isObject(response.responseJSON)) {
                                alert_error(objToString(response.responseJSON));
                            } else {
                                alert_error(response.responseJSON);
                            }
                        });
                    }
                }
            }
        }, {
            text: 'Cancelar',
            handler: function () {
                frmProcessType.my("remove");
                frmInstaceProcessType.resetForm();
                dlgProcessType.dialog('close');
            }
        }]
    });

    //Ações dos Botões
    btnNewProcessType.on('click', function () {
        $('div.panel-title').text('Formulário de Cadastro');
        dlgProcessType.dialog('open');
        frmProcessType.my(manifestProcessType);
        frmProcessType.my("data", {id: "", descricao: "",modalidade: "", status: ""});
        txtAction.val('insert');
        $('#descricao').focus();
    });

    //Abre o Dialog de Edição
    dtProcessType.on('click', '#editReg', function (e) {
        txtAction.val('update');
        let id = $(this).closest('tr').attr('data-id');
        $.ajax({
            type: "GET",
            url: '/api/audits/tables/process-type/' + id + '/edit'
        }).done(function (response) {
            $('div.panel-title').text('Formulário de Alteração');
            dlgProcessType.dialog('open');
            frmProcessType.my(manifestProcessType, response);
            $('#descricao').focus();
        });
        return false;
    });

    //Abre o Dialog de Confirmação de Remoção
    dtProcessType.on('click', '#deleteReg', function (e) {
        let id = $(this).closest('tr').attr('data-id');
        $.messager.confirm('Confirmação', 'Tem certeza que quer remover este registro?', function (r) {
            if (r) {
                $.ajax({
                    type: "DELETE",
                    url: '/api/audits/tables/process-type/' + id
                }).done(function (response) {
                    alert_success(response);
                    dtProcessType.ajax.reload(null, false);
                });
            }
        });
    });
});