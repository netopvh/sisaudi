document.addEventListener('DOMContentLoaded', function() {
    //DIVS E FORMS DO FORMULÁRIO

    //FIELDS
    let selAuditoriaProcessoCreate = $('#selAuditoriaProcesso');
    let txtAuditoriaDescricaoCreate = $('#auditoriaDescricaoCreate');
    let txtAuditoriaObjetivoCreate = $('#auditoriaObjetivoCreate');
    let txtAuditoriaEscopoCreate = $('#auditoriaEscopoCreate');
    let selAuditoriaDepartamentoCreate = $('#auditoriaDepartamentoCreate');

    //DIVERSOS
    //let dlgAdministrativeUnitsProcess = $('#dlgAdministrativeUnitsProcess');
    //let frmAdministrativeUnitsProcessCreate = $('#frmAdministrativeUnits');


    let dtAuditing = $('#tblAuditoria').DataTable({
        dom: "<'row'<'col-xs-12'f<'col-xs-12'>>r>" +
        "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        responsive: true,
        columnDefs: [
            {
                targets: 4,
                className: "text-center",
            }
        ],
        ajax: '/api/audits/auditing',
        columns: [
            {data: 'status', name: 'status',width: '140px'},
            {data: 'inicio_proposto', name: 'inicio_proposto', width: '350px'},
            {data: 'fim_proposto', name: 'fim_proposto', width: '120px'},
            {data: 'action', orderable: false, searchable: false, width: '15px'}
        ],
        createdRow: function (nRow, aData, iDisplayIndex) {
            $(nRow).attr('data-id', aData.id)
        }
    });

    selAuditoriaProcessoCreate.change(function () {
        if(this.value !== ''){
            $.get('/api/audits/process/' + this.value, function (response) {
                CKEDITOR.instances['auditoriaDescricaoCreate'].setData(response.descricao);
                CKEDITOR.instances['auditoriaObjetivoCreate'].setData(response.objetivo);
                CKEDITOR.instances['auditoriaEscopoCreate'].setData(response.escopo);
            });
        }else{
            dvProcessoCreate.collapse('hide');
        }
    });

    /**
    //Configuração da Caixa de Diálogo
    dlgPosFindings.dialog({
        closed: true,
        modal: true,
        width: 400,
        buttons: [{
            text:'Gravar',
            handler:function(){
                if(frmAdministrativeUnitsProcessCreate.valid()){
                    $.ajax({
                        type: "PATCH",
                        url: '/api/audits/tables/possible-findings/'+ txtId.val(),
                        data: frmPosFindings.my("data")
                    }).done(function (response) {
                        dlgPosFindings.dialog('close');
                        frmPosFindings.my("remove");
                        txtId.val('');
                        alert_success(response);
                        dtPosFindings.ajax.reload( null, false );
                    }).fail(function (response) {
                        dlgPosFindings.dialog('close');
                        frmPosFindings.my("remove");
                        if(Object.isObject(response.responseJSON)){
                            alert_error(objToString(response.responseJSON));
                        }else{
                            alert_error(response.responseJSON);
                        }
                    });
                }
            }
        }, {
            text:'Cancelar',
            handler:function(){
                frmPosFindings.my("remove");
                frmInstacePosFindings.resetForm();
                dlgPosFindings.dialog('close');
            }
        }]
    });

    //Ações dos Botões
    btnNewPosFindings.on('click', function() {
        $('div.panel-title').text('Formulário de Cadastro');
        frmPosFindings.my(manifestPosFindings);
        frmPosFindings.my("data",{id: "", nome: "", status: ""});
        dlgPosFindings.dialog('open');
        $('#nome').focus();
        txtAction.val('insert');
    }); */
});