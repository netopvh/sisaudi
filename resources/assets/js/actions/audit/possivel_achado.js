document.addEventListener('DOMContentLoaded', function() {
//Caixas de Diálogo e Forms
    let dlgPosFindings = $('#dlgPosFindings');
    let frmPosFindings = $('#frmPosFindings');

//Botoes
    let btnNewPosFindings = $('#newPosFindings');

    let manifestPosFindings = {
        data: {
            id: "",
            nome: "",
            status: "",
        },
        ui:{
            "#id": { bind: "id" },
            "#nome": { bind: "nome" },
            "#status": { bind: "status" },
        }
    };

    //Campos
    let txtAction = $('input:hidden[name="action"]');
    let txtId = $('input:hidden[name="item"]');

    let dtPosFindings = $('#tblPosFindings').DataTable({
        dom: "<'row'<'col-xs-12'f<'col-xs-12'>>r>" +
        "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        responsive: true,
        columnDefs: [
            {
                targets: 2,
                className: "text-center",
            }
        ],
        ajax: '/api/audits/tables/possible-findings',
        columns: [
            {data: 'nome', name: 'nome'},
            {data: 'status', name: 'status', width: '110px',searchable: false},
            {data: 'action', orderable: false, searchable: false, width: '100px'}
        ],
        createdRow: function (nRow, aData, iDisplayIndex) {
            $(nRow).attr('data-id', aData.id)
        }
    });

    //Validação do Formulário
    let frmInstacePosFindings = frmPosFindings.validate({
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        }
    });


    //Configuração da Caixa de Diálogo
    dlgPosFindings.dialog({
        closed: true,
        modal: true,
        width: 400,
        buttons: [{
            text:'Gravar',
            handler:function(){
                if(frmPosFindings.valid()){
                    if(txtAction.val() === 'insert'){
                        $.ajax({
                            type: "POST",
                            url: '/api/audits/tables/possible-findings',
                            data: frmPosFindings.my("data")
                        }).done(function (response) {
                            dlgPosFindings.dialog('close');
                            frmPosFindings.my("remove");
                            dtPosFindings.ajax.reload( null, false );
                            alert_success(response);
                        }).fail(function (response) {
                            dlgPosFindings.dialog('close');
                            frmPosFindings.my("remove");
                            if(Object.isObject(response.responseJSON)){
                                alert_error(objToString(response.responseJSON));
                            }else{
                                alert_error(response.responseJSON);
                            }
                        });
                    }else if(txtAction.val() === 'update'){
                        $.ajax({
                            type: "PATCH",
                            url: '/api/audits/tables/possible-findings/'+ txtId.val(),
                            data: frmPosFindings.my("data")
                        }).done(function (response) {
                            dlgPosFindings.dialog('close');
                            frmPosFindings.my("remove");
                            txtId.val('');
                            alert_success(response);
                            dtPosFindings.ajax.reload( null, false );
                        }).fail(function (response) {
                            dlgPosFindings.dialog('close');
                            frmPosFindings.my("remove");
                            if(Object.isObject(response.responseJSON)){
                                alert_error(objToString(response.responseJSON));
                            }else{
                                alert_error(response.responseJSON);
                            }
                        });
                    }
                }
            }
        }, {
            text:'Cancelar',
            handler:function(){
                frmPosFindings.my("remove");
                frmInstacePosFindings.resetForm();
                dlgPosFindings.dialog('close');
            }
        }]
    });

    //Ações dos Botões
    btnNewPosFindings.on('click', function() {
        $('div.panel-title').text('Formulário de Cadastro');
        frmPosFindings.my(manifestPosFindings);
        frmPosFindings.my("data",{id: "", nome: "", status: ""});
        dlgPosFindings.dialog('open');
        $('#nome').focus();
        txtAction.val('insert');
    });

    //Abre o Dialog de Edição
    dtPosFindings.on('click','#editReg', function (e) {
        txtAction.val('update');
        let id = $(this).closest('tr').attr('data-id');
        $.ajax({
            type: "GET",
            url: '/api/audits/tables/possible-findings/'+ id +'/edit'
        }).done(function (response) {
            $('div.panel-title').text('Formulário de Alteração');
            dlgPosFindings.dialog('open');
            $('#nome').focus();
            frmPosFindings.my(manifestPosFindings, response);
        });
        return false;
    });

    //Abre o Dialog de Confirmação de Remoção
    dtPosFindings.on('click','#deleteReg', function (e) {
        let id = $(this).closest('tr').attr('data-id');
        $.messager.confirm('Confirmação','Tem certeza que quer remover este registro?',function(r){
            if (r){
                $.ajax({
                    type: "DELETE",
                    url: '/api/audits/tables/possible-findings/'+ id
                }).done(function (response) {
                    alert_success(response);
                    dtPosFindings.ajax.reload( null, false );
                });
            }
        });
    });
});