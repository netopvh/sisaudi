document.addEventListener('DOMContentLoaded', function() {
    let rbSubquestao = $('input:radio[name="subquestao"]');
    let txtMatrizRisco = $('textarea[name="risco"]');
    let txtMatrizProcedimento = $('textarea[name="procedimento"]');
    let txtMatrizCriterio = $('textarea[name="criterio"]');
    let txtMatrizInfoReq = $('textarea[name="informacoes_requeridas"]');
    let txtMatrizFonteInfo = $('textarea[name="fonte_informacao"]');
    let txtMatrizLimitacoes = $('textarea[name="limitacoes"]');
    let txtMatrizPosAchados = $('textarea[name="possiveis_achados"]');
    let dvMatrizItens = $('#matrizItens');

    let tblProcessoMatrizPlan = $('#tblMatrizPlan');



    rbSubquestao.change(()=>{
        if ($('input[name=subquestao]:checked').val() === 'S'){
            dvMatrizItens.collapse('hide');
            txtMatrizRisco.attr('required',false);
            txtMatrizProcedimento.attr('required',false);
            txtMatrizCriterio.attr('required',false);
            txtMatrizInfoReq.attr('required',false);
            txtMatrizFonteInfo.attr('required',false);
            txtMatrizLimitacoes.attr('required',false);
            txtMatrizPosAchados.attr('required',false);
        }else if($('input[name=subquestao]:checked').val() === 'N'){
            dvMatrizItens.collapse('show');
            txtMatrizRisco.attr('required',true);
            txtMatrizProcedimento.attr('required',true);
            txtMatrizCriterio.attr('required',true);
            txtMatrizInfoReq.attr('required',true);
            txtMatrizFonteInfo.attr('required',true);
            txtMatrizLimitacoes.attr('required',true);
            txtMatrizPosAchados.attr('required',true);
        }
    });

    if(rbSubquestao.length){
        if ($('input[name=subquestao]:checked').val() === 'S'){
            dvMatrizItens.collapse('hide');
            txtMatrizRisco.attr('required',false);
            txtMatrizProcedimento.attr('required',false);
            txtMatrizCriterio.attr('required',false);
            txtMatrizInfoReq.attr('required',false);
            txtMatrizFonteInfo.attr('required',false);
            txtMatrizLimitacoes.attr('required',false);
            txtMatrizPosAchados.attr('required',false);
        }else if($('input[name=subquestao]:checked').val() === 'N'){
            dvMatrizItens.collapse('show');
            txtMatrizRisco.attr('required',true);
            txtMatrizProcedimento.attr('required',true);
            txtMatrizCriterio.attr('required',true);
            txtMatrizInfoReq.attr('required',true);
            txtMatrizFonteInfo.attr('required',true);
            txtMatrizLimitacoes.attr('required',true);
            txtMatrizPosAchados.attr('required',true);
        }
    }

    tblProcessoMatrizPlan.on('click','#deleteMatrizQuestao',function (e) {
        e.preventDefault();
        let trQuestao = $(this).closest('tr');
        let id = trQuestao.data('id');
        let type = trQuestao.data('type');
        $.messager.confirm('Confirm','Tem certeza que deseja remover este registros?',function(r){
            if (r){
                if(type === 'q'){
                    $.ajax({
                        type: 'DELETE',
                        url: '/api/audits/process/matriz/destroy/' + id
                    }).done((res) => {
                        trQuestao.slideUp('slow', function () {
                            trQuestao.add(trQuestao.next()).remove();
                        });
                    });
                }else if(type === 's'){
                    $.ajax({
                        type: 'GET',
                        url: '/api/audits/process/matriz/total-subquestions/' + id
                    }).done((response) => {
                        if(response.total >= 1){
                            $.messager.alert('Atenção','Para remover esta questão, é necessário remover as subquestões.');
                        }else{
                            $.ajax({
                                type: 'DELETE',
                                url: '/api/audits/process/matriz/destroy/' + id
                            }).done((res) => {
                                trQuestao.slideUp('slow', function () {
                                    trQuestao.remove();
                                });
                            });
                        }
                    })
                }
            }
        });
    });

    tblProcessoMatrizPlan.on('click','#deleteMatrizSubQuestao',function (e) {
        e.preventDefault();
        let trSubQuestao = $(this).closest('tr');
        let id = trSubQuestao.data('id');
        $.messager.confirm('Confirm','Tem certeza que deseja remover este registros?',function(r){
            if (r){
                $.ajax({
                    type: 'DELETE',
                    url: '/api/audits/process/matriz/destroy/' + id
                }).done((res) => {
                    trSubQuestao.slideUp('slow', function () {
                        trSubQuestao.remove();
                    });
                });
            }
        });
    });
});