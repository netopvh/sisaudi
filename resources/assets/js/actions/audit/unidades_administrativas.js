document.addEventListener('DOMContentLoaded', function() {
//Caixas de Diálogo e Forms
    let dlgAdministrativeUnits = $('#dlgAdministrativeUnits');
    let frmAdministrativeUnits = $('#frmAdministrativeUnits');

//Botoes
    let btnNewAdministrativeUnits = $('#newAdministrativeUnits');

//Campos
    let txtAction = $('input:hidden[name="action"]');
    let txtId = $('input:hidden[name="item"]');

    let manifestAdministrativeUnits = {
        data: {
            id: "",
            nome: "",
            sigla: "",
            adm_direta: "",
            secretario: "",
        },
        ui:{
            "#id": { bind: "id" },
            "#nome": { bind: "nome" },
            "#sigla": { bind: "sigla" },
            "#adm_direta": { bind: "adm_direta" },
            "#secretario": { bind: "secretario" }
        }
    };

    let dtAdministrativeUnits = $('#tblAdministrativeUnits').DataTable({
        dom: "<'row'<'col-xs-12'f<'col-xs-12'>>r>" +
        "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        responsive: true,
        columnDefs: [
            {
                targets: 2,
                className: "text-center",
            }
        ],
        ajax: '/api/audits/tables/administrative-units',
        columns: [
            {data: 'nome', name: 'nome'},
            {data: 'sigla', name: 'sigla', width:'150px'},
            {data: 'action', orderable: false, searchable: false, width: '100px'}
        ],
        createdRow: function (nRow, aData, iDisplayIndex) {
            $(nRow).attr('data-id', aData.id)
        }
    });
    
    //Validação do Formulário
    let frmInstance = frmAdministrativeUnits.validate({
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        }
    });

    //Configuração da Caixa de Diálogo
    dlgAdministrativeUnits.dialog({
        closed: true,
        modal: true,
        width: 500,
        buttons: [{
            text:'Gravar',
            handler:function(){
                if(frmAdministrativeUnits.valid()){
                    if(txtAction.val() === 'insert'){
                        $.ajax({
                            type: "POST",
                            url: '/api/audits/tables/administrative-units',
                            data: frmAdministrativeUnits.my("data")
                        }).done(function (response) {
                            dlgAdministrativeUnits.dialog('close');
                            frmAdministrativeUnits.my("remove");
                            dtAdministrativeUnits.ajax.reload( null, false );
                            alert_success(response)
                        }).fail(function (response) {
                            dlgAdministrativeUnits.dialog('close');
                            frmAdministrativeUnits.my("remove");
                            if(Object.isObject(response.responseJSON)){
                                alert_error(objToString(response.responseJSON));
                            }else{
                                alert_error(response.responseJSON);
                            }
                        });
                    }else if(txtAction.val() === 'update'){
                        $.ajax({
                            type: "PATCH",
                            url: '/api/audits/tables/administrative-units/'+ txtId.val(),
                            data: frmAdministrativeUnits.my("data")
                        }).done(function (response) {
                            dlgAdministrativeUnits.dialog('close');
                            frmAdministrativeUnits.my('remove');
                            txtId.val('');
                            alert_success(response);
                            dtAdministrativeUnits.ajax.reload( null, false );
                        }).fail(function (response) {
                            dlgAdministrativeUnits.dialog('close');
                            frmAdministrativeUnits.my('remove');
                            if(Object.isObject(response.responseJSON)){
                                alert_error(objToString(response.responseJSON));
                            }else{
                                alert_error(response.responseJSON);
                            }
                        });
                    }
                }
            }
        }, {
            text:'Cancelar',
            handler:function(){
                frmAdministrativeUnits.my('remove');
                frmInstance.resetForm();
                dlgAdministrativeUnits.dialog('close');
            }
        }]
    });

    //Ações dos Botões
    btnNewAdministrativeUnits.on('click', function() {
        $('div.panel-title').text('Formulário de Cadastro');
        frmAdministrativeUnits.my(manifestAdministrativeUnits);
        frmAdministrativeUnits.my("data",{id: "", nome: "", sigla: "", adm_direta: "", secretario: ""});
        dlgAdministrativeUnits.dialog('open');
        txtAction.val('insert');
    });

    //Abre o Dialog de Edição
    dtAdministrativeUnits.on('click','#editReg', function (e) {
        txtAction.val('update');
        let id = $(this).closest('tr').attr('data-id');
        $.ajax({
            type: "GET",
            url: '/api/audits/tables/administrative-units/'+ id +'/edit'
        }).done(function (response) {
            dlgAdministrativeUnits.dialog('open');
            $('div.panel-title').text('Formulário de Alteração');
            frmAdministrativeUnits.my(manifestAdministrativeUnits, response);
        });
        return false;
    });

    //Abre o Dialog de Confirmação de Remoção
    dtAdministrativeUnits.on('click','#deleteReg', function (e) {
        let id = $(this).closest('tr').attr('data-id');
        $.messager.confirm('Confirmação','Tem certeza que quer remover este registro?',function(r){
            if (r){
                $.ajax({
                    type: "DELETE",
                    url: '/api/audits/tables/administrative-units/'+ id
                }).done(function (response) {
                    alert_success(response);
                    dtAdministrativeUnits.ajax.reload( null, false );
                });
            }
        });
    });
});