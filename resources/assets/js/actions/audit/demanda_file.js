$(function () {

    let dvDemandaFile = $("#dvDemandaFile");
    let dvDemandaFileConteudo = $('#dvDemandaTexto');
    let selFormatoFile = $('select[name="tipo_file"]');
    let txtDemandaFileConteudo = $('textarea[name="conteudo"]');

    let optionsDemandaCreate = {
        toolbar: [
            { name: 'document', items: [ 'Print' ] },
            { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
            { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
            { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting' ] },
            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
            { name: 'align', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
            { name: 'links', items: [ 'Link', 'Unlink' ] },
            { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
            { name: 'insert', items: [ 'Image', 'Table' ] },
            { name: 'tools', items: [ 'Maximize' ] },
            { name: 'editing', items: [ 'Scayt','Templates' ] }
        ],
        height: 400,
        disallowedContent: 'img{width,height,float}',
        extraAllowedContent: 'img[width,height,align]',
        removePlugins: 'links,tools,about,document',
        removeButtons: 'Cut,Copy,Paste,Anchor,Source,Subscript,Superscript,Link,Unlink,Anchor,SpecialChar',
        enterMode: CKEDITOR.ENTER_P,
        shiftEnterMode: CKEDITOR.ENTER_BR,
        extraPlugins: 'print,justify,font,copyformatting,tableresize,uploadimage,templates',
        bodyClass: 'document-editor',
        contentsCss: [ '/css/custom_ckeditor.css' ]
    };

    let baseTemplate = '<table cellpadding="15" cellspacing="0" style="width:100%">' +
        '<tbody><tr>' +
        '<td style="white-space:nowrap; width:106px"><img data-cke-saved-src="/images/logo_pref.jpg" src="/images/logo_pref.jpg"></td>' +
        '<td style="text-align:center"><strong>PREFEITURA DO MUNICÍPIO DE PORTO VELHO<br>CONTROLADORIA GERAL DO MUNICÍPIO</strong></td>' +
        '</tr></tbody></table>' +
        '<p></p>';

    selFormatoFile.change(function () {
        //if(this.val === "")
        console.log();
        if($(this).val() === 'A'){
            dvDemandaFile.collapse('show');
            dvDemandaFileConteudo.collapse('hide');
            if(CKEDITOR.instances['DemandaFileTexto']){
                CKEDITOR.instances['DemandaFileTexto'].destroy();
            }
            selFormatoFile.prop('required',true);
        }else if($(this).val() === 'T'){
            dvDemandaFile.collapse('hide');
            dvDemandaFileConteudo.collapse('show');
            CKEDITOR.replace('DemandaFileTexto', optionsDemandaCreate);
            CKEDITOR.instances['DemandaFileTexto'].setData(baseTemplate);
            selFormatoFile.prop('required',false);
        }
    });

});