document.addEventListener('DOMContentLoaded', function() {
    //DIVS E FORMS DO FORMULÁRIO

    //FIELDS
    let selAuditoriaProcessoCreate = $('#selAuditoriaProcesso');
    let txtAuditoriaDescricaoCreate = $('#auditoriaDescricaoCreate');
    let txtAuditoriaObjetivoCreate = $('#auditoriaObjetivoCreate');
    let txtAuditoriaEscopoCreate = $('#auditoriaEscopoCreate');
    let selAuditoriaDepartamentoCreate = $('#auditoriaDepartamentoCreate');

    //DIVERSOS
    //let dlgAdministrativeUnitsProcess = $('#dlgAdministrativeUnitsProcess');
    //let frmAdministrativeUnitsProcessCreate = $('#frmAdministrativeUnits');
   

    let optionsAuditoriaCreate = {
        height: 170,
        removePlugins: 'links,tools,about,document',
        removeButtons: 'ImageButton',
        enterMode: CKEDITOR.ENTER_P,
        shiftEnterMode: CKEDITOR.ENTER_BR
    };

    let dtAuditing = $('#tblAcompanhamento').DataTable({
        dom: "<'row'<'col-xs-12'f<'col-xs-12'>>r>" +
        "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        responsive: true
    });
});