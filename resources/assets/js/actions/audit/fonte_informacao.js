document.addEventListener('DOMContentLoaded', function() {
//Caixas de Diálogo e Forms
    let dlgSourceInformation = $('#dlgSourceInformation');
    let frmSourceInformation = $('#frmSourceInformation');

//Botoes
    let btnNewSourceInformation = $('#newSourceInformation');

    let manifestSourceInformation = {
        data: {
            id: "",
            nome: "",
            descricao: "",
            status: "",
        },
        ui:{
            "#id": { bind: "id" },
            "#nome": { bind: "nome" },
            "#descricao": { bind: "descricao" },
            "#status": { bind: "status" },
        }
    };

    //Campos
    let txtAction = $('input:hidden[name="action"]');
    let txtId = $('input:hidden[name="item"]');

    let dtSourceInformation = $('#tblSourceInformation').DataTable({
        dom: "<'row'<'col-xs-12'f<'col-xs-12'>>r>" +
        "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        responsive: true,
        columnDefs: [
            {
                targets: 3,
                className: "text-center",
            }
        ],
        ajax: '/api/audits/tables/source-information',
        columns: [
            {data: 'nome', name: 'nome'},
            {data: 'descricao', name: 'descricao'},
            {data: 'status', name: 'status', width: '110px',searchable: false},
            {data: 'action', orderable: false, searchable: false, width: '100px'}
        ],
        createdRow: function (nRow, aData, iDisplayIndex) {
            $(nRow).attr('data-id', aData.id)
        }
    });

    //Validação do Formulário
    let frmInstaceSourceInformation = frmSourceInformation.validate({
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        }
    });


    //Configuração da Caixa de Diálogo
    dlgSourceInformation.dialog({
        closed: true,
        modal: true,
        width: 400,
        buttons: [{
            text:'Gravar',
            handler:function(){
                if(frmSourceInformation.valid()){
                    if(txtAction.val() === 'insert'){
                        $.ajax({
                            type: "POST",
                            url: '/api/audits/tables/source-information',
                            data: frmSourceInformation.my("data")
                        }).done(function (response) {
                            dlgSourceInformation.dialog('close');
                            frmSourceInformation.my("remove");
                            dtSourceInformation.ajax.reload( null, false );
                            alert_success(response);
                        }).fail(function (response) {
                            dlgSourceInformation.dialog('close');
                            frmSourceInformation.my("remove");
                            if(Object.isObject(response.responseJSON)){
                                alert_error(objToString(response.responseJSON));
                            }else{
                                alert_error(response.responseJSON);
                            }
                        });
                    }else if(txtAction.val() === 'update'){
                        $.ajax({
                            type: "PATCH",
                            url: '/api/audits/tables/source-information/'+ txtId.val(),
                            data: frmSourceInformation.my("data")
                        }).done(function (response) {
                            dlgSourceInformation.dialog('close');
                            frmSourceInformation.my("remove");
                            txtId.val('');
                            alert_success(response);
                            dtSourceInformation.ajax.reload( null, false );
                        }).fail(function (response) {
                            dlgSourceInformation.dialog('close');
                            frmSourceInformation.my("remove");
                            if(Object.isObject(response.responseJSON)){
                                alert_error(objToString(response.responseJSON));
                            }else{
                                alert_error(response.responseJSON);
                            }
                        });
                    }
                }
            }
        }, {
            text:'Cancelar',
            handler:function(){
                frmSourceInformation.my("remove");
                frmInstaceSourceInformation.resetForm();
                dlgSourceInformation.dialog('close');
            }
        }]
    });

    //Ações dos Botões
    btnNewSourceInformation.on('click', function() {
        $('div.panel-title').text('Formulário de Cadastro');
        frmSourceInformation.my(manifestSourceInformation);
        frmSourceInformation.my("data",{id: "", nome: "", descricao: "", status: ""});
        dlgSourceInformation.dialog('open');
        $('#nome').focus();
        txtAction.val('insert');
    });

    //Abre o Dialog de Edição
    dtSourceInformation.on('click','#editReg', function (e) {
        txtAction.val('update');
        let id = $(this).closest('tr').attr('data-id');
        $.ajax({
            type: "GET",
            url: '/api/audits/tables/source-information/'+ id +'/edit'
        }).done(function (response) {
            $('div.panel-title').text('Formulário de Alteração');
            dlgSourceInformation.dialog('open');
            $('#nome').focus();
            frmSourceInformation.my(manifestSourceInformation, response);
        });
        return false;
    });

    //Abre o Dialog de Confirmação de Remoção
    dtSourceInformation.on('click','#deleteReg', function (e) {
        let id = $(this).closest('tr').attr('data-id');
        $.messager.confirm('Confirmação','Tem certeza que quer remover este registro?',function(r){
            if (r){
                $.ajax({
                    type: "DELETE",
                    url: '/api/audits/tables/source-information/'+ id
                }).done(function (response) {
                    alert_success(response);
                    dtSourceInformation.ajax.reload( null, false );
                });
            }
        });
    });
});