document.addEventListener('DOMContentLoaded', function() {
//Caixas de Diálogo e Forms
    let dlgRecomendationCategory = $('#dlgRecomendationCategory');
    let frmRecomendationCategory = $('#frmRecomendationCategory');

//Botoes
    let btnNewRecomendationCategory = $('#newRecomendationCategory');

    let manifestRecomendationCategory = {
        data: {
            id: "",
            descricao: "",
        },
        ui:{
            "#id": { bind: "id" },
            "#descricao": { bind: "descricao" }
        }
    };

//Campos
    let txtAction = $('input:hidden[name="action"]');
    let txtId = $('input:hidden[name="item"]');

    let dtRecomendacaoCategory = $('#tblRecomendationCategory').DataTable({
        dom: "<'row'<'col-xs-12'f<'col-xs-12'>>r>" +
        "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        responsive: true,
        columnDefs: [
            {
                targets: 1,
                className: "text-center",
            }
        ],
        ajax: '/api/audits/tables/recomendation-categories',
        columns: [
            {data: 'descricao', name: 'descricao'},
            {data: 'action', orderable: false, searchable: false, width: '100px'}
        ],
        createdRow: function (nRow, aData, iDisplayIndex) {
            $(nRow).attr('data-id', aData.id)
        }
    });

    //Validação do Formulário
    let frmInstaceRecomendationCategory = frmRecomendationCategory.validate({
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        }
    });

    //Configuração da Caixa de Diálogo
    dlgRecomendationCategory.dialog({
        closed: true,
        modal: true,
        width: 400,
        buttons: [{
            text:'Gravar',
            handler:function(){
                if(frmRecomendationCategory.valid()){
                    if(txtAction.val() === 'insert'){
                        $.ajax({
                            type: "POST",
                            url: '/api/audits/tables/recomendation-categories',
                            data: frmRecomendationCategory.my("data")
                        }).done(function (response) {
                            dlgRecomendationCategory.dialog('close');
                            frmRecomendationCategory.my("remove");
                            alert_success(response);
                            dtRecomendacaoCategory.ajax.reload( null, false );
                        }).fail(function (response) {
                            dlgRecomendationCategory.dialog('close');
                            frmRecomendationCategory.my("remove");
                            if(Object.isObject(response.responseJSON)){
                                alert_error(objToString(response.responseJSON));
                            }else{
                                alert_error(response.responseJSON);
                            }
                        });
                    }else if(txtAction.val() === 'update'){
                        $.ajax({
                            type: "PATCH",
                            url: '/api/audits/tables/recomendation-categories/'+ txtId.val(),
                            data: frmRecomendationCategory.my("data")
                        }).done(function (response) {
                            dlgRecomendationCategory.dialog('close');
                            frmRecomendationCategory.my("remove");
                            txtId.val('');
                            dtRecomendacaoCategory.ajax.reload( null, false );
                            alert_success(response);
                        }).fail(function (response) {
                            dlgRecomendationCategory.dialog('close');
                            frmRecomendationCategory.my("remove");
                            txtId.val('');
                            if(Object.isObject(response.responseJSON)){
                                alert_error(objToString(response.responseJSON));
                            }else{
                                alert_error(response.responseJSON);
                            }
                        });
                    }
                }
            }
        }, {
            text:'Cancelar',
            handler:function(){
                frmRecomendationCategory.my("remove");
                frmInstaceRecomendationCategory.resetForm();
                dlgRecomendationCategory.dialog('close');
            }
        }]
    });

    //Ações dos Botões
    btnNewRecomendationCategory.on('click', function() {
        $('div.panel-title').text('Formulário de Cadastro');
        frmRecomendationCategory.my(manifestRecomendationCategory);
        frmRecomendationCategory.my("data",{id:"", descricao:""});
        dlgRecomendationCategory.dialog('open');
        txtAction.val('insert');
        $('#descricao').focus();
    });

    //Abre o Dialog de Edição
    dtRecomendacaoCategory.on('click','#editReg', function (e) {
        txtAction.val('update');
        let id = $(this).closest('tr').attr('data-id');
        $.ajax({
            type: "GET",
            url: '/api/audits/tables/recomendation-categories/'+ id +'/edit'
        }).done(function (response) {
            txtId.val(response.id);
            $('div.panel-title').text('Formulário de Alteração');
            frmRecomendationCategory.my(manifestRecomendationCategory,response);
            dlgRecomendationCategory.dialog('open');
            $('#descricao').focus();
        });
        return false;
    });

    //Abre o Dialog de Confirmação de Remoção
    dtRecomendacaoCategory.on('click','#deleteReg', function (e) {
        let id = $(this).closest('tr').attr('data-id');
        $.messager.confirm('Confirmação','Tem certeza que quer remover este registro?',function(r){
            if (r){
                $.ajax({
                    type: "DELETE",
                    url: '/api/audits/tables/recomendation-categories/'+ id
                }).done(function (response) {
                    alert_success(response);
                    dtRecomendacaoCategory.ajax.reload( null, false );
                });
            }
        });
    });
});