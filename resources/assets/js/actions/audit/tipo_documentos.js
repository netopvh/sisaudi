document.addEventListener('DOMContentLoaded', function() {
//Caixas de Diálogo e Forms
    let dlgDocTypes = $('#dlgDocTypes');
    let frmDocTypes = $('#frmDocTypes');

//Botoes
    let btnNewDocTypes = $('#newDocType');

    let manifestDocTypes = {
        data: {
            id: "",
            descricao: "",
            prazo: ""
        },
        ui:{
            "#id": { bind: "id" },
            "#descricao": { bind: "descricao" },
            "#prazo": { bind: "prazo" }
        }
    };

    //Campos
    let txtAction = $('input:hidden[name="action"]');
    let txtId = $('input:hidden[name="item"]');

    let dtDocTypes = $('#tblDocTypes').DataTable({
        dom: "<'row'<'col-xs-12'f<'col-xs-12'>>r>" +
        "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        responsive: true,
        columnDefs: [
            {
                targets: 2,
                className: "text-center",
            }
        ],
        ajax: '/api/audits/tables/doc-types',
        columns: [
            {data: 'descricao', name: 'descricao'},
            {data: 'prazo', name: 'prazo', width: '100px'},
            {data: 'action', orderable: false, searchable: false, width: '100px'}
        ],
        createdRow: function (nRow, aData, iDisplayIndex) {
            $(nRow).attr('data-id', aData.id)
        }
    });

    //Validação do Formulário
    let frmInstaceDocTypes = frmDocTypes.validate({
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        }
    });


    //Configuração da Caixa de Diálogo
    dlgDocTypes.dialog({
        closed: true,
        modal: true,
        width: 400,
        buttons: [{
            text:'Gravar',
            handler:function(){
                if(frmDocTypes.valid()){
                    if(txtAction.val() === 'insert'){
                        $.ajax({
                            type: "POST",
                            url: '/api/audits/tables/doc-types',
                            data: frmDocTypes.my("data")
                        }).done(function (response) {
                            dlgDocTypes.dialog('close');
                            frmDocTypes.my("remove");
                            dtDocTypes.ajax.reload( null, false );
                            alert_success(response);
                        }).fail(function (response) {
                            dlgDocTypes.dialog('close');
                            frmDocTypes.my("remove");
                            if(Object.isObject(response.responseJSON)){
                                alert_error(objToString(response.responseJSON));
                            }else{
                                alert_error(response.responseJSON);
                            }
                        });
                    }else if(txtAction.val() === 'update'){
                        $.ajax({
                            type: "PATCH",
                            url: '/api/audits/tables/doc-types/'+ txtId.val(),
                            data: frmDocTypes.my("data")
                        }).done(function (response) {
                            dlgDocTypes.dialog('close');
                            frmDocTypes.my("remove");
                            txtId.val('');
                            alert_success(response);
                            dtDocTypes.ajax.reload( null, false );
                        }).fail(function (response) {
                            dlgDocTypes.dialog('close');
                            frmDocTypes.my("remove");
                            if(Object.isObject(response.responseJSON)){
                                alert_error(objToString(response.responseJSON));
                            }else{
                                alert_error(response.responseJSON);
                            }
                        });
                    }
                }
            }
        }, {
            text:'Cancelar',
            handler:function(){
                frmDocTypes.my("remove");
                frmInstaceDocTypes.resetForm();
                dlgDocTypes.dialog('close');
            }
        }]
    });

    //Ações dos Botões
    btnNewDocTypes.on('click', function() {
        $('div.panel-title').text('Formulário de Cadastro');
        frmDocTypes.my(manifestDocTypes);
        frmDocTypes.my("data",{id:"", descricao:"", prazo:""});
        dlgDocTypes.dialog('open');
        txtAction.val('insert');
    });

    //Abre o Dialog de Edição
    dtDocTypes.on('click','#editReg', function (e) {
        txtAction.val('update');
        let id = $(this).closest('tr').attr('data-id');
        $.ajax({
            type: "GET",
            url: '/api/audits/tables/doc-types/'+ id +'/edit'
        }).done(function (response) {
            $('div.panel-title').text('Formulário de Alteração');
            dlgDocTypes.dialog('open');
            frmDocTypes.my(manifestDocTypes, response);
        });
    });

    //Abre o Dialog de Confirmação de Remoção
    dtDocTypes.on('click','#deleteReg', function (e) {
        let id = $(this).closest('tr').attr('data-id');
        $.messager.confirm('Confirmação','Tem certeza que quer remover este registro?',function(r){
            if (r){
                $.ajax({
                    type: "DELETE",
                    url: '/api/audits/tables/doc-types/'+ id
                }).done(function (response) {
                    alert_success(response);
                    dtDocTypes.ajax.reload( null, false );
                })
            }
        });
    });
});