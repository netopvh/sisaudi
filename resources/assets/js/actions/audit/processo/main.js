$(function () {
        let optionsProcessoCreate = {
            height: 250,
            removePlugins: 'links,tools,about,document',
            removeButtons: 'Cut,Copy,Paste,Undo,Redo,Anchor,Underline,Scayt,Source,Strike,Subscript,Superscript,Image,Link,Unlink,Anchor,Maximize,SpecialChar',
            enterMode: CKEDITOR.ENTER_P,
            shiftEnterMode: CKEDITOR.ENTER_BR
        };
//DIVS E FORMS DO FORMULÁRIO
        let dvProcessoCreate = $('#dvProcessoCreate');
        let dvProcessoProtocoloCreate = $('#dvProcessoProtocoloCreate');
        let tbProcessoDescSum = $('#processoTabDescSum');
        let tbProcessoExercAnt = $('#processoTabExercAnt');
        let tbProcessoExercAtual = $('#processoTabExercAtual');
        let tbProcesso = $('#tabProcesso');
        let tbEscopo = $('#processoTabEscopo');

//FIELDS
        let selProcessoTipo = $('#selProcessoTipo');
        let rbProcessoProtocoloCreate = $('input[type="radio"][name="protocolo"]');
        let txtProcessoNumeroCreate = $('input[name="numero"]');
        let txtProcessoAutuacaoCreate = $('input[name="autuacao"]');
        let selProcessoUniAdm = $('#selProcessoUniAdm');
        let selProcessoAuditores = $('#processoAuditores');
        let selProcessoDepartamento = $('#processoDepartamento');
        let selProcessoGestorAud = $('#processoGestorAud');

//DATATABLE DA PÁGINA INICIAL - INDEX
        $('#tblProcess').DataTable({
            dom: "<'row'<'col-xs-12'f<'col-xs-12'>>r>" +
                "<'row'<'col-xs-12't>>" +
                "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
            processing: true,
            serverSide: true,
            responsive: true,
            columnDefs: [
                {
                    targets: 4,
                    className: "text-center",
                }
            ],
            ajax: '/api/audits/process',
            columns: [
                {data: 'numero_processo', name: 'processos.numero_processo', width: '140px'},
                {data: 'assunto', name: 'processos.assunto', width: '350px'},
                {data: 'created_at', name: 'processos.created_at', width: '120px'},
                {data: 'auditados', name: 'auditados.nome', width: '200px'},
                {data: 'action', orderable: false, searchable: false, width: '15px'}
            ],
            createdRow: function (nRow, aData, iDisplayIndex) {
                $(nRow).attr('data-id', aData.id)
            }
        });

        $('#toolbar').html('<button id="newProcessCriteriaCreate" class="btn btn-sm btn-primary"><i class="icon-add"></i></button>');

        tbProcesso.tabs({
            tabWidth: 220,
            onSelect: function (event, ui) {
                $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
            }
        });

        //Exibe ou Oculta Formulario de acordo com ação
        selProcessoTipo.change(function () {
            if (this.value !== '') {
                if (this.value === 'APG') {
                    $.ajax({
                        type: "GET",
                        url: '/api/audits/process/type/' + this.value
                    }).done(function (response) {
                        $('#processoDescSum').val('');
                        $('#processoExercicioAnt').val('');
                        $('#processoExercicioAtual').val('');
                        CKEDITOR.instances['processoDescSum'].setData(response.desc_sum);
                        CKEDITOR.instances['processoExercicioAnt'].setData(response.exercicio_ante);
                        CKEDITOR.instances['processoExercicioAtual'].setData(response.exercicio_atual);
                        tbProcessoDescSum.collapse('show');
                        tbProcessoExercAnt.collapse('show');
                        tbProcessoExercAtual.collapse('show');
                        dvProcessoCreate.collapse('show');
                    });
                } else {
                    $('#processoDescSum').val('');
                    $('#processoExercicioAnt').val('');
                    $('#processoExercicioAtual').val('');
                    CKEDITOR.instances['processoDescSum'].setData('');
                    CKEDITOR.instances['processoExercicioAnt'].setData('');
                    CKEDITOR.instances['processoExercicioAtual'].setData('');
                    tbProcessoDescSum.collapse('hide');
                    tbProcessoExercAnt.collapse('hide');
                    tbProcessoExercAtual.collapse('hide');
                    dvProcessoCreate.collapse('show');
                }
            } else {
                dvProcessoCreate.collapse('hide');
            }
        });

        if (selProcessoTipo.length) {
            if (selProcessoTipo.val() !== '') {
                if (selProcessoTipo.val() === 'APG') {
                    $.ajax({
                        type: "GET",
                        url: '/api/audits/process/type/' + this.value
                    }).done(function (response) {
                        if(CKEDITOR.instances['processoDescSum'].getData()){
                            CKEDITOR.instances['processoDescSum'].setData(response.desc_sum);
                        }
                        if(CKEDITOR.instances['processoExercicioAnt'].getData()){
                            CKEDITOR.instances['processoExercicioAnt'].setData(response.exercicio_ante);
                        }
                        if(CKEDITOR.instances['processoExercicioAtual'].getData()){
                            CKEDITOR.instances['processoExercicioAtual'].setData(response.exercicio_atual);
                        }
                        tbProcessoDescSum.collapse('show');
                        tbProcessoExercAnt.collapse('show');
                        tbProcessoExercAtual.collapse('show');
                        dvProcessoCreate.collapse('show');
                    });
                } else {
                    $('#processoDescSum').val('');
                    $('#processoExercicioAnt').val('');
                    $('#processoExercicioAtual').val('');
                    CKEDITOR.instances['processoDescSum'].setData('');
                    CKEDITOR.instances['processoExercicioAnt'].setData('');
                    CKEDITOR.instances['processoExercicioAtual'].setData('');
                    tbProcessoDescSum.collapse('hide');
                    tbProcessoExercAnt.collapse('hide');
                    tbProcessoExercAtual.collapse('hide');
                    dvProcessoCreate.collapse('show');
                }
            } else {
                dvProcessoCreate.collapse('hide');
            }
        }

        selProcessoDepartamento.on('selectmenuchange', function () {
            let vm = this;
            if (vm.value !== '') {
                $.ajax({
                    type: "GET",
                    url: '/api/access/departaments/' + this.value
                }).done(function (response) {
                    selProcessoGestorAud.find('option').remove();
                    $('<option>').appendTo(selProcessoGestorAud);
                    $.each(response, (item, value) => {
                        $('<option>').val(item).text(value).appendTo(selProcessoGestorAud);
                    });
                    selProcessoGestorAud.selectmenu("refresh");
                }).fail(function (response) {
                    console.log(response)
                });
            }
        });

        if (selProcessoDepartamento.length) {
            if (selProcessoDepartamento.val() !== '') {
                $.get('/api/audits/process/' + $('#processoId').val(), (response) => {
                    $.get('/api/access/departaments/' + selProcessoDepartamento.val(), (busca) => {
                        selProcessoGestorAud.find('option').remove();
                        selProcessoGestorAud.append('<option value=""></option>');
                        $.each(busca, (key,  value) => {
                            if(key == response.gestor_id){
                                selProcessoGestorAud.append('<option value="'+ key +'" selected>' + value + '</option>');
                            }else{
                                selProcessoGestorAud.append('<option value="'+ key +'">' + value + '</option>');
                            }
                        });
                        selProcessoGestorAud.selectmenu('refresh');
                    });
                });
            }

        }

        $('#processoInicioProposto').datepicker().inputmask('dd/mm/yyyy');

        if ($('#processoEscopo').length) {
            CKEDITOR.replace('processoEscopo', optionsProcessoCreate);
        }
        if ($('#processoDescSum').length) {
            CKEDITOR.replace('processoDescSum', optionsProcessoCreate);
        }
        if ($('#processoExercicioAnt').length) {
            CKEDITOR.replace('processoExercicioAnt', optionsProcessoCreate);
        }
        if ($('#processoExercicioAtual').length) {
            CKEDITOR.replace('processoExercicioAtual', optionsProcessoCreate);
        }

        rbProcessoProtocoloCreate.change(function () {
            if (this.value === 'A') {
                clearChildren(dvProcessoProtocoloCreate);
                dvProcessoProtocoloCreate.collapse('hide');
                txtProcessoNumeroCreate.attr('required', false);
                txtProcessoAutuacaoCreate.attr('required', false);
            } else if (this.value === 'M') {
                clearChildren(dvProcessoProtocoloCreate);
                dvProcessoProtocoloCreate.collapse('show');
                txtProcessoNumeroCreate.attr('required', true);
                txtProcessoAutuacaoCreate.attr('required', true);
            }
        });

        selProcessoUniAdm.multiselect().multiselectfilter({
            label: 'Filtro',
            placeholder: 'Item à Pesquisar'
        });

        selProcessoAuditores.multiselect().multiselectfilter({
            label: 'Filtro',
            placeholder: 'Item à Pesquisar'
        });

        $('.ui-multiselect').css('width', '100%');

        /**
         //Configuração da Caixa de Diálogo
         dlgPosFindings.dialog({
        closed: true,
        modal: true,
        width: 400,
        buttons: [{
            text:'Gravar',
            handler:function(){
                if(frmAdministrativeUnitsProcessCreate.valid()){
                    $.ajax({
                        type: "PATCH",
                        url: '/api/audits/tables/possible-findings/'+ txtId.val(),
                        data: frmPosFindings.my("data")
                    }).done(function (response) {
                        dlgPosFindings.dialog('close');
                        frmPosFindings.my("remove");
                        txtId.val('');
                        alert_success(response);
                        dtPosFindings.ajax.reload( null, false );
                    }).fail(function (response) {
                        dlgPosFindings.dialog('close');
                        frmPosFindings.my("remove");
                        if(Object.isObject(response.responseJSON)){
                            alert_error(objToString(response.responseJSON));
                        }else{
                            alert_error(response.responseJSON);
                        }
                    });
                }
            }
        }, {
            text:'Cancelar',
            handler:function(){
                frmPosFindings.my("remove");
                frmInstacePosFindings.resetForm();
                dlgPosFindings.dialog('close');
            }
        }]
    });

         //Ações dos Botões
         btnNewPosFindings.on('click', function() {
        $('div.panel-title').text('Formulário de Cadastro');
        frmPosFindings.my(manifestPosFindings);
        frmPosFindings.my("data",{id: "", nome: "", status: ""});
        dlgPosFindings.dialog('open');
        $('#nome').focus();
        txtAction.val('insert');
    }); */
    }
);