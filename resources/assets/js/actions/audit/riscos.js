document.addEventListener('DOMContentLoaded', function() {
//Caixas de Diálogo e Forms
    let dlgRisk = $('#dlgRisk');
    let frmRisk = $('#frmRisk');

//Botoes
    let btnNewRisk = $('#newRisk');

    let manifestRisk = {
        data: {
            id: "",
            descricao: "",
            impacto: "",
            mitigacao: ""
        },
        ui:{
            "#id": { bind: "id" },
            "#descricao": { bind: "descricao" },
            "#impacto": { bind: "impacto" },
            "#mitigacao": { bind: "mitigacao" }
        }
    };

//Campos
    let txtAction = $('input:hidden[name="action"]');
    let txtId = $('input:hidden[name="item"]');

    let dtRisk = $('#tblRisk').DataTable({
        dom: "<'row'<'col-xs-12'f<'col-xs-12'>>r>" +
        "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        responsive: true,
        columnDefs: [
            {
                targets: 3,
                className: "text-center",
            }
        ],
        ajax: '/api/audits/tables/risks',
        columns: [
            {data: 'descricao', name: 'descricao'},
            {data: 'impacto', name: 'impacto'},
            {data: 'mitigacao', name: 'mitigacao'},
            {data: 'action', orderable: false, searchable: false, width: '100px'}
        ],
        createdRow: function (nRow, aData, iDisplayIndex) {
            $(nRow).attr('data-id', aData.id)
        }
    });

    //Validação do Formulário
    let frmInstaceRisk = frmRisk.validate({
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        }
    });

    //Configuração da Caixa de Diálogo
    dlgRisk.dialog({
        closed: true,
        modal: true,
        width: 400,
        buttons: [{
            text:'Gravar',
            handler:function(){
                if(frmRisk.valid()){
                    if(txtAction.val() === 'insert'){
                        $.ajax({
                            type: "POST",
                            url: '/api/audits/tables/risks',
                            data: frmRisk.my("data")
                        }).done(function (response) {
                            dlgRisk.dialog('close');
                            frmRisk.my("remove");
                            alert_success(response);
                            dtRisk.ajax.reload( null, false );
                        }).fail(function (response) {
                            dlgRisk.dialog('close');
                            frmRisk.my("remove");
                            if(Object.isObject(response.responseJSON)){
                                alert_error(objToString(response.responseJSON));
                            }else{
                                alert_error(response.responseJSON);
                            }
                        });
                    }else if(txtAction.val() === 'update'){
                        $.ajax({
                            type: "PATCH",
                            url: '/api/audits/tables/risk/'+ txtId.val(),
                            data: frmRisk.my("data")
                        }).done(function (response) {
                            dlgRisk.dialog('close');
                            frmRisk.my("remove");
                            txtId.val('');
                            alert_success(response);
                            dtRisk.ajax.reload( null, false );
                        }).fail(function (response) {
                            dlgRisk.dialog('close');
                            frmRisk.my("remove");
                            txtId.val('');
                            if(Object.isObject(response.responseJSON)){
                                alert_error(objToString(response.responseJSON));
                            }else{
                                alert_error(response.responseJSON);
                            }
                        });
                    }
                }
            }
        }, {
            text:'Cancelar',
            handler:function(){
                frmRisk.my("remove");
                frmInstaceRisk.resetForm();
                dlgRisk.dialog('close');
            }
        }]
    });

    //Ações dos Botões
    btnNewRisk.on('click', function() {
        $('div.panel-title').text('Formulário de Cadastro');
        frmRisk.my(manifestRisk);
        frmRisk.my("data",{id:"", descricao:"",impacto:"",mitigacao:""});
        dlgRisk.dialog('open');
        txtAction.val('insert');
        $('#descricao').focus();
    });

    //Abre o Dialog de Edição
    dtRisk.on('click','#editReg', function (e) {
        txtAction.val('update');
        let id = $(this).closest('tr').attr('data-id');
        $.ajax({
            type: "GET",
            url: '/api/audits/tables/risks/'+ id +'/edit'
        }).done(function (response) {
            txtId.val(response.id);
            $('div.panel-title').text('Formulário de Alteração');
            frmRisk.my(manifestRisk, response);
            dlgRisk.dialog('open');
            $('#descricao').focus();
        });
        return false;
    });

    //Abre o Dialog de Confirmação de Remoção
    dtRisk.on('click','#deleteReg', function (e) {
        let id = $(this).closest('tr').attr('data-id');
        $.messager.confirm('Confirmação','Tem certeza que quer remover este registro?',function(r){
            if (r){
                $.ajax({
                    type: "DELETE",
                    url: '/api/audits/tables/risks/'+ id
                }).done(function (response) {
                    alert_success(response);
                    dtRisk.ajax.reload( null, false );
                });
            }
        });
    });
});