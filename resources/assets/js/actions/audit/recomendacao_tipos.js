document.addEventListener('DOMContentLoaded', function() {
//Caixas de Diálogo e Forms
    let dlgRecomendationType = $('#dlgRecomendationType');
    let formRecomendationType = $('#frmRecomendationType');

//Botoes
    let btnNewRecomendacaoTipo = $('#newRecomendacaoTipo');

    let manifestRecomendationType = {
        data: {
            id: "",
            descricao: "",
        },
        ui:{
            "#id": { bind: "id" },
            "#descricao": { bind: "descricao" }
        }
    };

//Campos
    let txtAction = $('input:hidden[name="action"]');
    let txtId = $('input:hidden[name="item"]');

    let dtRecomendacaoTipo = $('#tblRecomendacaoTipo').DataTable({
        dom: "<'row'<'col-xs-12'f<'col-xs-12'>>r>" +
        "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        responsive: true,
        columnDefs: [
            {
                targets: 1,
                className: "text-center",
            }
        ],
        ajax: '/api/audits/tables/recomendation-types',
        columns: [
            {data: 'descricao', name: 'descricao'},
            {data: 'action', orderable: false, searchable: false, width: '100px'}
        ],
        createdRow: function (nRow, aData, iDisplayIndex) {
            $(nRow).attr('data-id', aData.id)
        }
    });

    //Validação do Formulário
    let frmInstaceRecomendationType = formRecomendationType.validate({
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        }
    });

    //Configuração da Caixa de Diálogo
    dlgRecomendationType.dialog({
        closed: true,
        modal: true,
        width: 400,
        buttons: [{
            text:'Gravar',
            handler:function(){
                if(formRecomendationType.valid()){
                    if(txtAction.val() === 'insert'){
                        $.ajax({
                            type: "POST",
                            url: '/api/audits/tables/recomendation-types',
                            data: formRecomendationType.my("data")
                        }).done(function (response) {
                            dlgRecomendationType.dialog('close');
                            formRecomendationType.my("remove");
                            alert_success(response);
                            dtRecomendacaoTipo.ajax.reload( null, false );
                        }).fail(function (response) {
                            dlgRecomendationType.dialog('close');
                            formRecomendationType.my("remove");
                            if(Object.isObject(response.responseJSON)){
                                alert_error(objToString(response.responseJSON));
                            }else{
                                alert_error(response.responseJSON);
                            }
                        });
                    }else if(txtAction.val() === 'update'){
                        $.ajax({
                            type: "PATCH",
                            url: '/api/audits/tables/recomendation-types/'+ txtId.val(),
                            data: formRecomendationType.my("data")
                        }).done(function (response) {
                            dlgRecomendationType.dialog('close');
                            formRecomendationType.my("remove");
                            txtId.val('');
                            alert_success(response);
                            dtRecomendacaoTipo.ajax.reload( null, false );
                        }).fail(function (response) {
                            dlgRecomendationType.dialog('close');
                            formRecomendationType.my("remove");
                            txtId.val('');
                            if(Object.isObject(response.responseJSON)){
                                alert_error(objToString(response.responseJSON));
                            }else{
                                alert_error(response.responseJSON);
                            }
                        });
                    }
                }
            }
        }, {
            text:'Cancelar',
            handler:function(){
                formRecomendationType.my("remove");
                frmInstaceRecomendationType.resetForm();
                dlgRecomendationType.dialog('close');
            }
        }]
    });

    //Ações dos Botões
    btnNewRecomendacaoTipo.on('click', function() {
        $('div.panel-title').text('Formulário de Cadastro');
        formRecomendationType.my(manifestRecomendationType);
        formRecomendationType.my("data",{id:"", descricao:""});
        dlgRecomendationType.dialog('open');
        txtAction.val('insert');
        $('#descricao').focus();
    });

    //Abre o Dialog de Edição
    dtRecomendacaoTipo.on('click','#editReg', function (e) {
        txtAction.val('update');
        let id = $(this).closest('tr').attr('data-id');
        $.ajax({
            type: "GET",
            url: '/api/audits/tables/recomendation-types/'+ id +'/edit'
        }).done(function (response) {
            txtId.val(response.id);
            $('div.panel-title').text('Formulário de Alteração');
            formRecomendationType.my(manifestRecomendationType, response);
            dlgRecomendationType.dialog('open');
            $('#descricao').focus();
        });
        return false;
    });

    //Abre o Dialog de Confirmação de Remoção
    dtRecomendacaoTipo.on('click','#deleteReg', function (e) {
        let id = $(this).closest('tr').attr('data-id');
        $.messager.confirm('Confirmação','Tem certeza que quer remover este registro?',function(r){
            if (r){
                $.ajax({
                    type: "DELETE",
                    url: '/api/audits/tables/recomendation-types/'+ id
                }).done(function (response) {
                    alert_success(response);
                    dtRecomendacaoTipo.ajax.reload( null, false );
                });
            }
        });
    });
});