document.addEventListener('DOMContentLoaded', function() {
//Caixas de Diálogo e Forms
    let dlgFollowUpStatus = $('#dlgFollowUpStatus');
    let frmFollowUpStatus = $('#frmFollowUpStatus');

//Botoes
    let btnNewFollowUpStatus = $('#newFollowUpStatus');

    let manifestFollowUpStatus = {
        data: {
            id: "",
            descricao: "",
        },
        ui:{
            "#id": { bind: "id" },
            "#descricao": { bind: "descricao" }
        }
    };

//Campos
    let txtAction = $('input:hidden[name="action"]');
    let txtId = $('input:hidden[name="item"]');

    let dtFollowUpStatus = $('#tblFollowUpStatus').DataTable({
        dom: "<'row'<'col-xs-12'f<'col-xs-12'>>r>" +
        "<'row'<'col-xs-12't>>" +
        "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        responsive: true,
        columnDefs: [
            {
                targets: 1,
                className: "text-center",
            }
        ],
        ajax: '/api/audits/tables/follow-status',
        columns: [
            {data: 'descricao', name: 'descricao'},
            {data: 'action', orderable: false, searchable: false, width: '100px'}
        ],
        createdRow: function (nRow, aData, iDisplayIndex) {
            $(nRow).attr('data-id', aData.id)
        }
    });

    //Validação do Formulário
    let frmInstaceFollowUpStatus = frmFollowUpStatus.validate({
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        }
    });

    //Configuração da Caixa de Diálogo
    dlgFollowUpStatus.dialog({
        closed: true,
        modal: true,
        width: 400,
        buttons: [{
            text:'Gravar',
            handler:function(){
                if(frmFollowUpStatus.valid()){
                    if(txtAction.val() === 'insert'){
                        $.ajax({
                            type: "POST",
                            url: '/api/audits/tables/follow-status',
                            data: frmFollowUpStatus.my("data")
                        }).done(function (response) {
                            dlgFollowUpStatus.dialog('close');
                            frmFollowUpStatus.my("remove");
                            dtFollowUpStatus.ajax.reload( null, false );
                            alert_success(response);
                        }).fail(function (response) {
                            dlgFollowUpStatus.dialog('close');
                            frmFollowUpStatus.my("remove");
                            if(Object.isObject(response.responseJSON)){
                                alert_error(objToString(response.responseJSON));
                            }else{
                                alert_error(response.responseJSON);
                            }
                        });
                    }else if(txtAction.val() === 'update'){
                        $.ajax({
                            type: "PATCH",
                            url: '/api/audits/tables/follow-status/'+ txtId.val(),
                            data: frmFollowUpStatus.my("data")
                        }).done(function (response) {
                            dlgFollowUpStatus.dialog('close');
                            frmFollowUpStatus.my("remove");
                            txtId.val('');
                            alert_success(response);
                            dtFollowUpStatus.ajax.reload( null, false );
                        }).fail(function (response) {
                            dlgFollowUpStatus.dialog('close');
                            frmFollowUpStatus.my("remove");
                            txtId.val('');
                            if(Object.isObject(response.responseJSON)){
                                alert_error(objToString(response.responseJSON));
                            }else{
                                alert_error(response.responseJSON);
                            }
                        });
                    }
                }
            }
        }, {
            text:'Cancelar',
            handler:function(){
                frmFollowUpStatus.my("remove");
                frmInstaceFollowUpStatus.resetForm();
                dlgFollowUpStatus.dialog('close');
            }
        }]
    });

    //Ações dos Botões
    btnNewFollowUpStatus.on('click', function() {
        $('div.panel-title').text('Formulário de Cadastro');
        frmFollowUpStatus.my(manifestFollowUpStatus);
        frmFollowUpStatus.my("data",{id:"", descricao:""});
        dlgFollowUpStatus.dialog('open');
        txtAction.val('insert');
        $('#descricao').focus();
    });

    //Abre o Dialog de Edição
    dtFollowUpStatus.on('click','#editReg', function (e) {
        txtAction.val('update');
        let id = $(this).closest('tr').attr('data-id');
        $.ajax({
            type: "GET",
            url: '/api/audits/tables/follow-status/'+ id +'/edit'
        }).done(function (response) {
            txtId.val(response.id);
            $('div.panel-title').text('Formulário de Alteração');
            frmFollowUpStatus.my(manifestFollowUpStatus, response);
            dlgFollowUpStatus.dialog('open');
            $('#descricao').focus();
        });
        return false;
    });

    //Abre o Dialog de Confirmação de Remoção
    dtFollowUpStatus.on('click','#deleteReg', function (e) {
        let id = $(this).closest('tr').attr('data-id');
        $.messager.confirm('Confirmação','Tem certeza que quer remover este registro?',function(r){
            if (r){
                $.ajax({
                    type: "DELETE",
                    url: '/api/audits/tables/follow-status/'+ id
                }).done(function (response) {
                    alert_success(response);
                    dtFollowUpStatus.ajax.reload( null, false );
                });
            }
        });
    });
});