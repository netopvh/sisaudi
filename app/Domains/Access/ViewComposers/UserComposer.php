<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 29/08/2018
 * Time: 10:36
 */

namespace App\Domains\Access\ViewComposers;

use Illuminate\View\View;
use App\Domains\Access\Repositories\Contracts\UserRepository;

class UserComposer
{
    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * UserComposer constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function compose(View $view)
    {
        $view->with('usuarios', $this->repository->with('departamento')->all());
    }
}