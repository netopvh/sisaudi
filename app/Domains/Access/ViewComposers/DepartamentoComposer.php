<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 29/08/2018
 * Time: 10:36
 */

namespace App\Domains\Access\ViewComposers;

use Illuminate\View\View;
use App\Domains\Access\Repositories\Contracts\DepartamentoRepository;

class DepartamentoComposer
{
    /**
     * @var DepartamentoRepository
     */
    protected $repository;

    /**
     * DepartamentoComposer constructor.
     * @param DepartamentoRepository $repository
     */
    public function __construct(DepartamentoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function compose(View $view)
    {
        $view->with('departamentos', $this->repository->all());
    }
}