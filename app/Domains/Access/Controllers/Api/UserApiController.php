<?php

namespace App\Domains\Access\Controllers\Api;

use App\Core\Exceptions\GeneralException;
use Illuminate\Http\Request;
use App\Core\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use App\Domains\Access\Repositories\Contracts\UserRepository;
use Illuminate\Support\Str;
use App\Domains\Access\Models\User;

class UserApiController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->middleware('auth:api');
        $this->userRepository = $userRepository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index(DataTables $dataTables)
    {
        $model = $query = User::with('roles')->select('users.*');
        return $dataTables->eloquent($model)
            ->addColumn('active', function ($user){
                return $user->active? '<span class="label label-success">Ativo</span>':'<span class="label label-warning">Inativo</span>';
            })
            ->addColumn('display_name',function (User $user){
                return $user->roles->first()->display_name;
            })
            ->addColumn('action', function ($user){
                return view('users.actions', compact('user'));
            })
            ->rawColumns(['action','active'])
            ->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        try{
            if($request->json()){
                $user = $this->userRepository->findExists('id', $id);
                return response()->json($user);
            }
        }catch (GeneralException $e){
            return response()->json($e->getMessage(),400);
        }
    }

    public function update($id)
    {
        $user = $this->userRepository->find($id);
        if($user->active == 1){
            $this->userRepository->update(['active' => 0], $id);
        }else{
            $this->userRepository->update(['active' => 1], $id);
        }
    }
}
