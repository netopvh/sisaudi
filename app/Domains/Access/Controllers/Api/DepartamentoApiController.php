<?php

namespace App\Domains\Access\Controllers\Api;

use App\Core\Exceptions\GeneralException;
use App\Core\Http\Controllers\Controller;
use Prettus\Validator\Exceptions\ValidatorException;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Domains\Access\Repositories\Contracts\DepartamentoRepository;

class DepartamentoApiController extends Controller
{
    /**
     * @var DepartamentoRepository
     */
    protected $departamentoRepository;

    /**
     * DepartamentoApiController constructor.
     * @param DepartamentoRepository $departamentoRepository
     */
    public function __construct(DepartamentoRepository $departamentoRepository)
    {
        $this->middleware('auth:api');
        $this->departamentoRepository = $departamentoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index(DataTables $dataTables)
    {
        $model = $this->departamentoRepository->select(['id','descricao']);

        return $dataTables->eloquent($model)
            ->addColumn('action', function ($departamento){
                return view('departamentos.actions', compact('departamento'));
            })
            ->rawColumns(['action'])
            ->toJson();
    }

    /**
     * Grava Registros no Banco de Dados
     *
     * @param Request $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try{
            if ($request->json()){
                $this->departamentoRepository->create($request->all());
                return response()->json(config('messages.create'),200);
            }
            return false;
        }catch (ValidatorException $e){
            return response()->json($e->getMessageBag(),400);
        }catch (GeneralException $e){
            return response()->json($e->getMessage(),400);
        }
    }

    public function show(Request$request, $id)
    {
        try{
            if($request->json()){
                $departamento = $this->departamentoRepository->with('users')->findExists('id', $id);
                return response()->json($departamento->users()->pluck('users.nome','users.id'));
            }
        }catch (GeneralException $e){
            return response()->json($e->getMessage(),400);
        }
    }

    /**
     * Localiza registro recebido em JSON
     *
     * @param Request $request
     * @param $id
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function edit(Request $request, $id)
    {
        try{
            if ($request->json()){
                $result = $this->departamentoRepository->findExists('id',$id);
                return response()->json($result,200);
            }
            return false;
        }catch (GeneralException $e){
            return response()->json($e->getMessage(),400);
        }
    }

    /**
     * Atualiza registro via JSON
     *
     * @param Request $request
     * @param $id
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try{
            if($request->json()){
                $this->departamentoRepository->update($request->all(), $id);
                return response()->json(config('messages.update'),200);
            }
            return false;
        }catch (ValidatorException $e){
            return response()->json($e->getMessageBag(),400);
        }
        catch (GeneralException $e){
            return response()->json($e->getMessage(),400);
        }
    }

    /**
     * Remove registro do banco de dados
     *
     * @param Request $request
     * @param $id
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        try{
            if ($request->json()){
                $this->departamentoRepository->findExists('id', $id);
                $this->departamentoRepository->delete($id);
                return response()->json(config('messages.delete'),200);
            }
            return false;
        }catch (GeneralException $e){
            return response()->json($e->getMessage(),400);
        }
    }
}
