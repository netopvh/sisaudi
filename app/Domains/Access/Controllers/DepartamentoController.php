<?php

namespace App\Domains\Access\Controllers;

use App\Core\Exceptions\GeneralException;
use App\Core\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Domains\Access\Repositories\Contracts\UserRepository;
use App\Domains\Access\Repositories\Contracts\DepartamentoRepository;

class DepartamentoController extends Controller
{

    /**
     * @var DepartamentoRepository
     */
    protected $departamentoRepository;

    /**
     * @var UserRepository
     */
    protected $userRepository;
    /**
     * DepartamentoController constructor.
     * @param DepartamentoRepository $departamentoRepository
     */
    public function __construct(
        DepartamentoRepository $departamentoRepository,
        UserRepository $userRepository
    )
    {
        $this->departamentoRepository = $departamentoRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Item inicial do Controller
     *
     * @param Request $request
     * @return
     */
    public function index()
    {
        return view('departamentos.index');
    }

    /**
     * Exibe formulário de cadastro
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('departamentos.create');
    }

    /**
     * Armazena registro no banco de dados
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $this->departamentoRepository->create($request->all());
            return redirect()->route('departamentos.index')
                ->withSuccess(config('messages.create'));
        } catch (ValidatorException $e) {
            return redirect()->route('departamentos.create')
                ->withErrors($e->getMessageBag());
        }
    }

    /**
     * Localiza registro no banco de dados para edição.
     *
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        try {
            $departamento = $this->departamentoRepository->findExists('id', $id);
            return view('departamentos.edit')
                ->withDepartamento($departamento);
        } catch (GeneralException $e) {
            return redirect()->route('departamentos.index')
                ->withErrors($e->getMessage());
        }
    }

    /**
     * Atualiza informações no banco de dados
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        try {
            $this->departamentoRepository->update($request->all(), $id);
            return redirect()->route('departamentos.index')
                ->withSuccess(config('messages.update'));
        } catch (ValidatorException $e) {
            return redirect()->back()
                ->withErrors($e->getMessageBag());
        }
    }

    /**
     * Remove registro do banco de dados
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $this->departamentoRepository->findExists('id', $id);
            $this->departamentoRepository->delete($id);
            return redirect('/processo/departamento/')
                ->withSuccess(config('messages.delete'));
        } catch (GeneralException $e) {
            return redirect('/processo/departamento/')
                ->withErrors($e->getMessage());
        }
    }

}