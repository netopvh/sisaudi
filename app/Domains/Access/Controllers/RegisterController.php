<?php

namespace App\Domains\Access\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Domains\Access\Repositories\Contracts\UserRepository;
use App\Domains\Access\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Domains\Access\Models\User;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * @var UserRepository
     */
    protected $repository;

    protected $service;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $repository, UserService $service)
    {
        $this->middleware('guest');
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $request = $this->service->store($request->all());
        //dd($request['data']->matriculas->matricula);

        if ($request['success'] && isset($request['step'])){
            return redirect()->route('login')->withSuccess($request['messages']);
        }elseif ($request['success']){
            return redirect()->route('register',['step'=>'next'])->with('data', $request['data']);
        }else{
            return redirect()->route('register')
                ->withErrors($request['messages'])
                ->withInput();
        }
    }


}
