<?php

namespace App\Domains\Access\Repositories;

use App\Core\Repositories\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Domains\Access\Repositories\Contracts\DepartamentoRepository;
use App\Domains\Access\Models\Departamento;
use App\Domains\Access\Validators\DepartamentoValidator;

/**
 * Class DepartamentoRepositoryEloquent.
 *
 * @package namespace App\Domains\Processo\Repositories;
 */
class DepartamentoRepositoryEloquent extends BaseRepository implements DepartamentoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Departamento::class;
    }

    public function validator()
    {
        return DepartamentoValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
