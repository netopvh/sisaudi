<?php

namespace App\Domains\Access\Repositories;

use App\Core\Exceptions\GeneralException;
use App\Core\Repositories\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Domains\Access\Repositories\Contracts\UserRepository;
use App\Domains\Access\Models\User;
use App\Domains\Access\Validators\UserValidator;
use Prettus\Repository\Events\RepositoryEntityCreated;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Prettus\Repository\Traits\CacheableRepository;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class UserRepositoryEloquent.
 *
 * @package namespace App\Domains\Access\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository, CacheableInterface
{
    use CacheableRepository;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function findUser($id)
    {
        $result = $this->model->newQuery()->where('id', $id)->get()->first();
        if($result->roles()->count() <= 0){
            $result->roles()->attach(3);
        }
        if (is_null($result)) {
            throw new GeneralException("Não foi localizado nenhum registro no banco de dados");
        }
        return $result;
    }

    public function setUserStatus(array $attributes, $id)
    {
        $user = $this->model->find($id);
        $user->fill($attributes);
        if(!$user->save()){
            throw new GeneralException('Erro ao Definir Status do Usuário');
        }
    }
    
}
