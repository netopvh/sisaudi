<?php

namespace App\Domains\Access\Repositories\Contracts;

use App\Core\Repositories\BaseRepositoryContract;

/**
 * Interface DepartamentoRepository.
 *
 * @package namespace App\Domains\Processo\Repositories\Contracts;
 */
interface DepartamentoRepository extends BaseRepositoryContract
{
    //
}
