@permission('editar-departamentos')
<button id="editReg" data-toggle="tooltip" title="Editar" class="btn btn-xs btn-primary">
    <i class="icon-pencil7"></i>
</button>
@endpermission
@permission('remover-departamentos')
<button id="deleteReg" title="Deletar" class="btn btn-xs btn-danger">
    <i class="icon-trash"></i>
</button>
@endpermission