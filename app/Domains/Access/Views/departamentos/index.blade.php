@extends('layouts.app')

@section('page-header')
    <div class="page-header page-header-default page-header-sm" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Departamentos</span></h5>
            </div>
        </div>
        {{ Breadcrumbs::render('departamentos.index') }}
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default border-grey">
                <div class="panel-heading">
                    @permission('criar-departamentos')
                    <button id="newDepartament" class="btn btn-sm btn-primary legitRipple">
                        <i class="icon-database-add"></i>
                        <span>Novo Registro</span>
                    </button>
                    @endpermission
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse" class=""></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <table class="table table-bordered table-condensed table-hover" id="tblDepartaments">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Descrição</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div id="dlgDepartaments">
        <form method="post" id="frmDepartaments">
            {{ csrf_field() }}
            <input type="hidden" name="item">
            <input type="hidden" name="action">
            <div class="form-group">
                <label>Descrição:</label>
                <input type="text" name="descricao" class="form-control text-uppercase" required>
            </div>
        </form>
    </div>
@stop