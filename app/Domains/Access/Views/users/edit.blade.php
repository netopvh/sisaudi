@extends('layouts.app')

@section('page-header')
    <div class="page-header page-header-default page-header-sm"
         style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Usuários</span></h5>
            </div>
        </div>
        {{ Breadcrumbs::render('users.edit') }}
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default border-grey">
                <div class="panel-heading">
                    <i class="icon-table"></i> <span class="text-bold">Formulário de Alteração</span>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse" class=""></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <form action="{{ route('users.update',['id' => $user->id]) }}" class="form-validate"
                      method="post" autocomplete="off">
                    <div class="panel-body">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label class="text-bold">ID</label>
                                    <input type="text" class="form-control"
                                           value="{{ str_pad($user->id, 5, "0", STR_PAD_LEFT) }}" readonly="">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="text-bold">Login:</label>
                                    <input type="text" class="form-control" value="{{ $user->username }}" readonly
                                           required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="text-bold">Perfil:</label>
                                    <select name="role_id" class="select">
                                        <option value="">Selecione</option>
                                        @foreach($roles as $role)
                                            <option {{ isset($user) ? ($role->id === $user->roles()->first()->id ? 'selected': '') : '' }} value="{{ $role->id }}">{{ $role->display_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="text-bold">Nome:</label>
                                    <input type="text" name="nome" class="form-control text-uppercase"
                                           value="{{ $user->nome }}" required>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="text-bold">Email:</label>
                                    <input type="email" name="email" class="form-control" value="{{ $user->email }}"
                                           required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="text-bold">Departamento: </label>
                                    <select name="departamento_id" class="select" required>
                                        <option value=""></option>
                                        @foreach($departamentos as $departamento)
                                            <option {{ isset($departamento) ? ($departamento->id === $user->departamento_id ? 'selected': '') : '' }} value="{{ $departamento->id }}">{{ $departamento->descricao }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="display-block text-bold">Função:</label>
                                    <select name="funcao" class="select" required>
                                        <option value=""></option>
                                        <option value="1" {{ $user->funcao == 1 ? 'selected': '' }}>Administrador</option>
                                        <option value="2" {{ $user->funcao == 2 ? 'selected': '' }}>Auditor</option>
                                        <option value="3" {{ $user->funcao == 3 ? 'selected': '' }}>Técnico</option>
                                        <option value="4" {{ $user->funcao == 4 ? 'selected': '' }}>Controlador</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-primary legitRipple" type="submit" id="btnUpdateUser"><i
                                    class="icon-database-insert"></i>
                            Gravar
                        </button>
                        <a class="btn btn-info legitRipple" href="{{ route('users.index') }}"><i
                                    class="icon-database-arrow"></i> Retornar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
