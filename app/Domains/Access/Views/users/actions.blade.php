@permission('editar-usuario')
<a href="{{ route('users.edit',['id' => $user->id]) }}" class="btn btn-xs bg-teal-400 legitRipple">
    <i class="icon-pencil7"></i>
</a>
@endpermission
@permission('desativar-usuario')
<input type="hidden" id="inUpdateUserStatus" name="_method" value="patch">
@if($user->active)
    <a href="#" class="btn btn-xs bg-danger-300 desativa" data-id="{{ $user->id }}" data-value="0">
        <i class="icon-user-block"></i>
    </a>
@else
    <a href="#" class="btn btn-xs bg-green-400 ativa" data-id="{{ $user->id }}" data-value="1">
        <i class="icon-user-check"></i>
    </a>
@endif
@endpermission