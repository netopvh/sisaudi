@permission('editar-perfil')
<a href="{{ route('roles.edit',['id' => $role->id]) }}" title="Editar" class="btn btn-xs btn-primary easyui-tooltip">
    <i class="icon-pencil7"></i>
</a>
@endpermission