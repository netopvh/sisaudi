@extends('layouts.app')

@push('styles')
    <style type="text/css">
        body {
            background: url('images/bg/{{ rand(0,15) }}.jpg') no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>
@endpush


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-primary">
                    <div class="panel-heading">Cadastrar Acesso ao Sistema SISAUDI</div>

                    <div class="panel-body">
                        <form class="form-validate" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}
                            <fieldset class="step" id="validation-step1">
                                <h6 class="form-wizard-title text-semibold">
                                    <span class="form-wizard-count">1</span>
                                    Informações
                                    <small class="display-block">Digite nos campos abaixo</small>
                                </h6>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-bold">Matrícula: <span
                                                        class="text-danger">*</span></label>
                                            <input type="text" name="username" id="registerMat"
                                                   class="form-control" required
                                                   value="{{ session()->has('data')? session()->get('data')->matriculas->matricula:'' }}">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>CPF: <span class="text-danger">*</span></label>
                                            <input type="text" name="cpf" id="registerCpf" required
                                                   class="form-control"
                                                   value="{{ session()->has('data')? clean_cpf(session()->get('data')->cpf):'' }}">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            @if(Request::get('step') == 'next')
                                <input type="hidden" name="step" value="last">
                                <fieldset class="step" id="validation-step2">
                                    <h6 class="form-wizard-title text-semibold">
                                        <span class="form-wizard-count">2</span>
                                        Informações Adicionais
                                    </h6>


                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text-bold">Nome: <span
                                                            class="text-danger">*</span></label>
                                                <input type="text" name="nome"
                                                       class="form-control text-uppercase" required
                                                       value="{{ session()->has('data')? session()->get('data')->nome:'' }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text-bold">Departamento:</label>
                                                <select name="departamento_id" class="form-control" required>
                                                    <option value=""></option>
                                                    @foreach($departamentos as $departamento)
                                                        <option value="{{ $departamento->id }}">{{ $departamento->descricao }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text-bold">Senha:</label>
                                                <input type="password" name="password" id="password" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text-bold">Repita Senha:</label>
                                                <input type="password" name="repeat_password" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            @endif

                            <div class="form-wizard-actions">
                                <a href="{{ route('login') }}" class="btn btn-info">Voltar</a>
                                <button class="btn btn-primary" id="validation-next" type="submit">Próximo</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
