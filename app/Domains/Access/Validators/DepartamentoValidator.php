<?php

namespace App\Domains\Access\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class DepartamentoValidator.
 *
 * @package namespace App\Domains\Processo\Validators;
 */
class DepartamentoValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'descricao' => 'required|unique:departamentos'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'descricao' => 'required|unique:departamentos'
        ],
    ];

    protected $messages = [
        'required' => '* O :attribute é obrigatório',
        'unique' => '* Já possui este :attribute no banco de dados',
    ];

    protected $attributes = [
        'descricao' => 'descrição',
    ];
}
