<?php

if (!function_exists("remove_bars")) {
    function remove_bars(string $attribute)
    {
        if($attribute === "[]"){
            return "";
        }else{
            $resultOne = str_replace('["','',$attribute);
            $resultTwo = str_replace('"]','',$resultOne);
            return $resultTwo;
        }
    }
}

if (!function_exists('clean_cpf')){
    function clean_cpf($cpf)
    {
        $cpf = preg_replace( '#[^0-9]#', '',$cpf);
        if(strlen($cpf) == 9){
            return '00'.$cpf;
        }elseif (strlen($cpf) == 10){
            return '0'.$cpf;
        }else{
            return $cpf;
        }
    }
}
