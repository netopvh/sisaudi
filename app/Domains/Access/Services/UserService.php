<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 04/09/2018
 * Time: 15:56
 */

namespace App\Domains\Access\Services;


use App\Core\Exceptions\GeneralException;
use App\Domains\Access\Repositories\Contracts\UserRepository;
use App\Domains\Access\Validators\UserValidator;
use GuzzleHttp\Exception\ClientException;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

class UserService
{

    /**
     * @var UserRepository 
     */
    private $repository;

    /**
     * @var UserValidator
     */
    private $validator;

    /**
     * UserService constructor.
     * @param UserRepository $repository
     * @param UserValidator $validator
     */
    public function __construct(
        UserRepository $repository,
        UserValidator $validator
    )
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    public function store(array $data)
    {
        try{
            //$this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
            $content = $this->getApi($data['username']);

            if ($this->validateCpf($data['cpf'],$content->cpf)){
                if(!$this->verifyUserExists($data['username'])){
                    if(isset($data['step'])){
                        $data['password'] = bcrypt($data['password']);
                        $result = $this->repository->create($data);
                        if($result){
                            $result->roles()->attach(3);
                            return ['success' => true, 'step' => 'last','messages' => 'Usuário cadastrado com sucesso'];
                        }else{
                            return ['success' => false,'messages' => 'Erro ao Cadastrar usuário, por favor entre em contato com o DATI'];
                        }
                    }else{
                        return ['success' => true, 'data' => $content];
                    }
                }else{
                    return ['success' => false, 'messages' => 'Usuário já cadastrado no sistema.'];
                }
            }else{
                return ['success' => false, 'messages' => 'O CPF informado não corresponde a matrícula'];
            }
        }catch (\Exception $e){
            switch (get_class($e)) {
                case ValidatorException::class:
                    return ['success' => false, 'messages' => $e->getMessageBag()];
                    break;
                case GeneralException::class:
                    return ['success' => false, 'messages' => $e->getMessage()];
                    break;
                case ClientException::class:
                    return ['success' => false, 'messages' => 'Não foi localizada nenhuma matrícula correspondente em nossa base de dados.'];
                    break;
                default:
                    return ['success' => false, 'messages' => $e->getMessage()];
                    break;
            }
        }
    }

    private function getApi($field){
        $client = new \GuzzleHttp\Client(['base_uri' => 'https://sistemas.portovelho.ro.gov.br/']);
        $respose = $client->request('GET','rest/Servidor/ConsultarMatricula?matricula='.$field);
        $body = $respose->getBody();
        $content = $body->getContents();
        return json_decode($content);
    }

    private function validateCpf($field, $compare){
        if(clean_cpf($field) == clean_cpf($compare)){
            return true;
        }else{
            return false;
        }
    }

    private function verifyUserExists($attribute){
        $count = $this->repository->query()->where('username','=',$attribute)->get()->count();
        if ($count >= 1){
            return true;
        }else{
            return false;
        }
    }

}