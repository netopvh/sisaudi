<?php

namespace App\Domains\Access\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;

/**
 * Class Departamento.
 *
 * @package namespace App\Domains\Processo\Models;
 */
class Departamento extends Model implements Transformable, AuditableContract
{
    use TransformableTrait, Auditable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['descricao'];

    /**
     * Deixa registro em Maiúsculo
     *
     * @param $value
     */
    public function setDescricaoAttribute($value)
    {
        $this->attributes['descricao'] = mb_strtoupper($value, "UTF-8");
    }

    /**
     * Relacionamento Departamento -> Usuário
     *
     */
    public function users()
    {
        return $this->hasMany(User::class,'departamento_id');
    }
}
