<?php

namespace App\Domains\Auditoria\Controllers;

use App\Core\Exceptions\GeneralException;
use App\Core\Http\Controllers\Controller;
use App\Domains\Access\Repositories\Contracts\DepartamentoRepository;
use App\Domains\Auditoria\Repositories\Contracts\FundamentacaoLegalRepository;
use App\Domains\Auditoria\Repositories\Contracts\ProcessoRepository;
use App\Domains\Auditoria\Repositories\Contracts\RiscoRepository;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Domains\Auditoria\Repositories\Contracts\UnidadeAdministrativaRepository;
use App\Domains\Access\Repositories\Contracts\UserRepository;

class AuditoriaController extends Controller
{
    /**
     * @var ProcessoRepository
     */
    protected $processoRepository;

    /**
     * ProcessoController constructor.
     * @param ProcessoRepository $processoRepository
     */
    public function __construct(
        ProcessoRepository $processoRepository
    )
    {
        $this->processoRepository = $processoRepository;
    }

    /**
     * Item inicial do Controller
     *
     * @param Request $request
     * @return
     */
    public function index()
    {
        return view('auditoria.index');
    }


    public function create(Request $request)
    {
        return view('auditoria.create')
            ->withProcessos($this->processoRepository->all());
    }

    public function store(Request $request)
    {
        try{
            $processo = $this->processoRepository->save($request->all());
            return redirect()->route('processo.show',['id' => $processo])
                ->withSuccess(config('messages.create'));
        }catch (ValidatorException $e){
            return redirect()->route('processo.create',['action'=>'new'])
                ->withErrors($e->getMessageBag())
                ->withInput();
        }catch (GeneralException $e){
            return redirect()->route('processo.create',['action'=>'new'])
                ->withErrors($e->getMessage())
                ->withInput();
        }
    }

    public function show($id)
    {
        try{
            $processo = $this->processoRepository->findExists('id', $id);
            return view('processo.show')
                ->withProcesso($processo);
        }catch (GeneralException $e){
            return redirect()->route('processo.index')
                ->withErrors($e->getMessage());
        }
    }

    public function edit($id)
    {
        try{
            $processo = $this->processoRepository->with(['base_legais','riscos','auditados'])->findExists('id', $id);
            return view('processo.edit')
                ->withProcessoTipos(DB::table('processo_tipo')->get())
                ->withBasesLegais($this->fundamentacaoLegalRepository->all())
                ->withRiscos($this->riscoRepository->all())
                ->withDepartamentos($this->departamentoRepository->all())
                ->withUsuarios($this->userRepository->with('departamento')->all())
                ->withUnidadesAdministrativas($this->unidadeAdministrativaRepository->all())
                ->withProcesso($processo);
        }catch (GeneralException $e){
            return redirect()->route('processo.index')
                ->withErrors($e->getMessage());
        }
    }

}