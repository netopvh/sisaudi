<?php
namespace App\Domains\Auditoria\Controllers;

use App\Core\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PossivelAchadoController extends Controller
{
    /**
     * Item inicial do Controller
     *
     * @param Request $request
     * @return
     */
    public function index()
    {
        return view('possivel_achado.index');
    }

}