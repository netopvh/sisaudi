<?php

namespace App\Domains\Auditoria\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Domains\Auditoria\Jobs\CheckDeadlineDocs;
use App\Domains\Auditoria\Repositories\Contracts\DemandaRepository;
use App\Domains\Auditoria\Repositories\Contracts\ProcessoRepository;
use App\Domains\Auditoria\Services\DemandaService;
use Illuminate\Http\Request;

class DemandaController extends Controller
{
    /**
     * @var ProcessoRepository
     */
    protected $repository;

    protected $service;

    /**
     * ProcessoController constructor.
     * @param DemandaRepository $repository
     * @param DemandaService $service
     */
    public function __construct(
        DemandaRepository $repository,
        DemandaService $service
    )
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Item inicial do Controller
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('demanda.index');
    }


    public function create()
    {
        return view('demanda.create');
    }

    public function store(Request $request)
    {
        $request = $this->service->store($request->all());

        if ($request['success']) {
            return redirect()->route('demanda.show', ['id' => $request['data']])->withSuccess($request['messages']);
        } else {
            return redirect()->route('demanda.create')
                ->withErrors($request['messages'])
                ->withInput();
        }
    }

    public function show($id)
    {
        $request = $this->service->show($id);

        if ($request['success']) {
            return view('demanda.show')
                ->withDemanda($request['data']);
        } else {
            return redirect()->route('demanda.index')
                ->withErrors($request['messages']);
        }
    }

    public function edit($id)
    {
        $request = $this->service->edit($id);

        if ($request['success']) {
            return view('processo.auditoria.edit')
                ->withProcesso($request['data']);
        } else {
            return redirect()->back()
                ->withErrors($request['messages']);

        }
    }

    public function update(Request $request, $id)
    {
        $request = $this->service->update($request->all(), $id);

        if ($request['success']) {
            return redirect()->route('processo.show', ['id' => $request['data']])->withSuccess($request['messages']);
        } else {
            return redirect()->route('processo.index')
                ->withErrors($request['messages'])
                ->withInput();
        }
    }

}