<?php

namespace App\Domains\Auditoria\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Domains\Auditoria\Repositories\Contracts\DemandaRepository;
use App\Domains\Auditoria\Repositories\Contracts\ProcessoRepository;
use App\Domains\Auditoria\Services\DemandaService;
use Illuminate\Http\Request;

class DemandaFileController extends Controller
{
    /**
     * @var ProcessoRepository
     */
    protected $repository;

    protected $service;

    /**
     * ProcessoController constructor.
     * @param DemandaRepository $repository
     * @param DemandaService $service
     */
    public function __construct(
        DemandaRepository $repository,
        DemandaService $service
    )
    {
        $this->repository = $repository;
        $this->service = $service;
    }


    public function create($id)
    {
        return view('demanda.files.create')
            ->withDemanda($this->repository->find($id));
    }

    public function store($id, Request $request)
    {
        $request = $this->service->storeFiles($id, $request);

        if ($request['success']) {
            return redirect()->route('demanda.show', ['id' => $request['data']])->withSuccess($request['messages']);
        } else {
            return redirect()->route('demanda.files.create',['id' => $id])
                ->withErrors($request['messages'])
                ->withInput();
        }
    }

    public function show($id, $file)
    {
        $request = $this->service->showFile($id, $file);

        if ($request['success']) {
            return view('demanda.files.show')
                ->withArquivo($request['data']);
        } else {
            return redirect()->route('demanda.index')
                ->withErrors($request['messages']);
        }
    }

    public function edit($id)
    {
        $request = $this->service->edit($id);

        if ($request['success']) {
            return view('processo.auditoria.edit')
                ->withProcesso($request['data']);
        } else {
            return redirect()->back()
                ->withErrors($request['messages']);

        }
    }

    public function update(Request $request, $id)
    {
        $request = $this->service->update($request->all(), $id);

        if ($request['success']) {
            return redirect()->route('processo.show', ['id' => $request['data']])->withSuccess($request['messages']);
        } else {
            return redirect()->route('processo.index')
                ->withErrors($request['messages'])
                ->withInput();
        }
    }

}