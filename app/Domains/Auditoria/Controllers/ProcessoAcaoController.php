<?php

namespace App\Domains\Auditoria\Controllers;

use App\Core\Exceptions\GeneralException;
use App\Core\Http\Controllers\Controller;
use App\Domains\Access\Repositories\Contracts\DepartamentoRepository;
use App\Domains\Auditoria\Enum\ModalidadeAuditoriaEnum;
use App\Domains\Auditoria\Enum\ProcessoTipoEnum;
use App\Domains\Auditoria\Repositories\Contracts\CriterioRepository;
use App\Domains\Auditoria\Repositories\Contracts\ProcessoRepository;
use App\Domains\Auditoria\Repositories\Contracts\RiscoRepository;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Domains\Auditoria\Repositories\Contracts\UnidadeAdministrativaRepository;
use App\Domains\Access\Repositories\Contracts\UserRepository;
use DOMWrap\Document;

class ProcessoAcaoController extends Controller
{
    /**
     * @var ProcessoRepository
     */
    protected $processoRepository;

    /**
     * ProcessoController constructor.
     * @param ProcessoRepository $processoRepository
     */
    public function __construct(
        ProcessoRepository $processoRepository
    )
    {
        $this->processoRepository = $processoRepository;
    }

    public function listFontes($id)
    {
        $processo = $this->processoRepository->find($id);
    }

}