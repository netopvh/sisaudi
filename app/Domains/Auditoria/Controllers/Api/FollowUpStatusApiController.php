<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 02/08/2018
 * Time: 13:04
 */

namespace App\Domains\Auditoria\Controllers\Api;

use App\Core\Exceptions\GeneralException;
use App\Core\Http\Controllers\Controller;
use Prettus\Validator\Exceptions\ValidatorException;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Domains\Auditoria\Repositories\Contracts\FollowUpStatusRepository;

class FollowUpStatusApiController extends Controller
{
    /**
     * @var FollowUpStatusRepository
     */
    protected $followUpStatusRepository;

    /**
     * DepartamentoApiController constructor.
     * @param FollowUpStatusRepository $followUpStatusRepository
     */
    public function __construct(FollowUpStatusRepository $followUpStatusRepository)
    {
        $this->followUpStatusRepository = $followUpStatusRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index(DataTables $dataTables)
    {
        $model = $this->followUpStatusRepository->select(['id','descricao']);

        return $dataTables->eloquent($model)
            ->addColumn('action', function (){
                return view('follow_up.status.actions');
            })
            ->rawColumns(['action'])
            ->toJson();
    }

    /**
     * Grava Registros no Banco de Dados
     *
     * @param Request $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try{
            if ($request->json()){
                $this->followUpStatusRepository->create($request->all());
                return response()->json(config('messages.create'),200);
            }
            return false;
        }catch (ValidatorException $e){
            return response()->json($e->getMessageBag(),400);
        }catch (GeneralException $e){
            return response()->json($e->getMessage(),400);
        }
    }

    /**
     * Localiza registro recebido em JSON
     *
     * @param Request $request
     * @param $id
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function edit(Request $request, $id)
    {
        try{
            if ($request->json()){
                $result = $this->followUpStatusRepository->findExists('id',$id);
                return response()->json($result,200);
            }
            return false;
        }catch (GeneralException $e){
            return response()->json($e->getMessage(),400);
        }
    }

    /**
     * Atualiza registro via JSON
     *
     * @param Request $request
     * @param $id
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try{
            if($request->json()){
                $this->followUpStatusRepository->update($request->all(), $id);
                return response()->json(config('messages.update'),200);
            }
            return false;
        }catch (ValidatorException $e){
            return response()->json($e->getMessageBag(),400);
        }
        catch (GeneralException $e){
            return response()->json($e->getMessage(),400);
        }
    }

    /**
     * Remove registro do banco de dados
     *
     * @param Request $request
     * @param $id
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        try{
            if ($request->json()){
                $this->followUpStatusRepository->findExists('id', $id);
                $this->followUpStatusRepository->delete($id);
                return response()->json(config('messages.delete'),200);
            }
            return false;
        }catch (GeneralException $e){
            return response()->json($e->getMessage(),400);
        }
    }
}