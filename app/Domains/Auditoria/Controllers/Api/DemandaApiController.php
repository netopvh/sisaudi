<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 02/08/2018
 * Time: 13:04
 */

namespace App\Domains\Auditoria\Controllers\Api;

use App\Core\Http\Controllers\Controller;
use App\Domains\Auditoria\Models\Demanda;
use App\Domains\Auditoria\Repositories\Contracts\DemandaRepository;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class DemandaApiController extends Controller
{
    /**
     * @var ProcessoRepository
     */
    protected $demandaRepository;

    /**
     * ProcessoApiController constructor.
     * @param DemandaRepository $demandaRepository
     */
    public function __construct(DemandaRepository $demandaRepository)
    {
        $this->demandaRepository = $demandaRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index(DataTables $dataTables, Request $request)
    {
        $model = $this->demandaRepository->with(['interessados','criador'])
        ->query()
        ->departamento();

        return $dataTables->eloquent($model)
            ->addColumn('interessados', function (Demanda $demanda) {
                return $demanda->interessados->map(function ($interessado) {
                    return $interessado->nome;
                })->implode('<br>');
            })
            ->addColumn('criador',function ($demanda){
                return $demanda->criador->nome;
            })
            ->editColumn('autuacao', function ($demanda){
                return date_to_format($demanda->autuacao);
            })
            ->addColumn('action', function ($demanda) {
                return view('demanda.actions', compact('demanda'));
            })
            ->filter(function ($demanda) use ($request){
                if($request->has('assunto') && $request->get('assunto') != ''){
                    $demanda->where('assunto','like', "%" . $request->get('assunto') . "%");
                }
                if($request->has('autuacao') && $request->get('autuacao') != ''){
                    $demanda->whereDate('autuacao', Carbon::createFromFormat('d/m/Y',$request->get('autuacao'))->format('Y-m-d'));
                }
                if($request->has('criador') && $request->get('criador') != ''){
                    $demanda->where('criador_id', $request->get('criador'));
                }
            })
            ->rawColumns(['action', 'interessados'])
            ->toJson();
    }
}