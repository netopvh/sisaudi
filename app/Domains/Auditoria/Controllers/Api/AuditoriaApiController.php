<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 02/08/2018
 * Time: 13:04
 */

namespace App\Domains\Auditoria\Controllers\Api;

use App\Core\Exceptions\GeneralException;
use App\Core\Http\Controllers\Controller;
use App\Domains\Auditoria\Models\Processo;
use App\Domains\Auditoria\Repositories\Contracts\AuditoriaRepository;
use Prettus\Validator\Exceptions\ValidatorException;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Domains\Auditoria\Repositories\Contracts\ProcessoRepository;

class AuditoriaApiController extends Controller
{
    /**
     * @var AuditoriaRepository
     */
    protected $auditoriaRepository;

    /**
     * AuditoriaApiController constructor.
     * @param AuditoriaRepository $auditoriaRepository
     */
    public function __construct(AuditoriaRepository $auditoriaRepository)
    {
        $this->auditoriaRepository = $auditoriaRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index(DataTables $dataTables)
    {
        $model = $this->auditoriaRepository->with(['processo'])->select(['id','status','inicio_proposto','fim_proposto']);

        return $dataTables->eloquent($model)
            ->addColumn('action', function ($processo){
                return view('auditoria.actions', compact('processo'));
            })
            ->rawColumns(['action','auditores'])
            ->toJson();
    }
}