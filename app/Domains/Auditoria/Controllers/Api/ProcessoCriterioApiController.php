<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 02/08/2018
 * Time: 13:04
 */

namespace App\Domains\Auditoria\Controllers\Api;

use App\Core\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Domains\Auditoria\Repositories\Contracts\CriterioRepository;

class ProcessoCriterioApiController extends Controller
{

    /**
     * @var CriterioRepository
     */
    protected $criterioRepository;

    /**
     * ProcessoCriterioApiController constructor.
     * @param CriterioRepository $criterioRepository
     */
    public function __construct(CriterioRepository $criterioRepository)
    {
        $this->criterioRepository = $criterioRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index(Request $request)
    {
        if ($request->json()){
            if ($request->has('search')){
                $result = $this->criterioRepository->findWhere([
                    'nome','like',$request->get('search')
                ]);
                $response = [];
                foreach ($result as $res){
                    $response[] = ['ck' => false, 'id' => $res->id, 'nome' => $res->nome];
                }
                return response()->json($response);
            }else{
                $result = $this->criterioRepository->all();
                $response = [];
                foreach ($result as $res){
                    $response[] = ['ck' => false, 'id' => $res->id, 'nome' => $res->nome];
                }
                return response()->json($response);
            }
        }
    }
}