<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 02/08/2018
 * Time: 13:04
 */

namespace App\Domains\Auditoria\Controllers\Api;

use App\Core\Exceptions\GeneralException;
use App\Core\Http\Controllers\Controller;
use App\Domains\Auditoria\Repositories\Contracts\ProcessoMatrizRepository;
use App\Domains\Auditoria\Services\ProcessoMatrizService;
use Illuminate\Http\Request;
use App\Domains\Auditoria\Repositories\Contracts\RiscoRepository;

class ProcessoMatrizApiController extends Controller
{

    protected $repository;

    protected $service;


    public function __construct(
        ProcessoMatrizRepository $repository,
        ProcessoMatrizService $service
    )
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTotalSub($id)
    {
        $questao = $this->repository->with('subquestoes')->find($id);
        $total = $questao->subquestoes->count();
        return response()->json([
            'total' => $total
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $result = $this->repository->delete($id);
        return response()->json('success',200);
    }

}