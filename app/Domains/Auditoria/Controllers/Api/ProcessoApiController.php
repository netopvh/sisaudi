<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 02/08/2018
 * Time: 13:04
 */

namespace App\Domains\Auditoria\Controllers\Api;

use App\Core\Exceptions\GeneralException;
use App\Core\Http\Controllers\Controller;
use App\Domains\Auditoria\Models\Processo;
use Prettus\Validator\Exceptions\ValidatorException;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Domains\Auditoria\Repositories\Contracts\ProcessoRepository;

class ProcessoApiController extends Controller
{
    /**
     * @var ProcessoRepository
     */
    protected $processoRepository;

    /**
     * ProcessoApiController constructor.
     * @param ProcessoRepository $processoRepository
     */
    public function __construct(ProcessoRepository $processoRepository)
    {
        $this->processoRepository = $processoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return mixed
     */
    public function index(DataTables $dataTables)
    {
        $model = $this->processoRepository->with(['auditados'])
        ->query()
        ->departamento();

        return $dataTables->eloquent($model)
            ->addColumn('auditados', function (Processo $processo) {
                return $processo->auditados->map(function ($auditor) {
                    return $auditor->nome;
                })->implode('<br>');
            })
            ->addColumn('action', function ($processo) {
                return view('processo.auditoria.actions', compact('processo'));
            })
            ->rawColumns(['action', 'auditados'])
            ->toJson();
    }

    public function type(Request $request, $type)
    {
        if ($request->json()) {
            switch ($type) {
                case 'APG':
                    return response()->json([
                        'desc_sum' => view('processo.auditoria._contents.desc_sum')->render(),
                        'exercicio_ante' => view('processo.auditoria._contents.exercicio_ante')->render(),
                        'exercicio_atual' => view('processo.auditoria._contents.exercicio_atual')->render()
                    ]);
                    break;
                default:
                    return response()->json("");
            }
        }
    }

    public function getProcesso(Request $request, $id)
    {
        try{
            if($request->json()){
                $processoGestor = $this->processoRepository->findExists('id',$id);
                return response()->json($processoGestor,200);
            }
        }catch (GeneralException $e){
            return response()->json($e->getMessage(),400);
        }
    }
}