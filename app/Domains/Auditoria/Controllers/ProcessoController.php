<?php

namespace App\Domains\Auditoria\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Domains\Auditoria\Repositories\Contracts\ProcessoRepository;
use App\Domains\Auditoria\Services\ProcessoService;
use Illuminate\Http\Request;

class ProcessoController extends Controller
{
    /**
     * @var ProcessoRepository
     */
    protected $repository;

    protected $service;

    /**
     * ProcessoController constructor.
     * @param ProcessoRepository $repository
     */
    public function __construct(
        ProcessoRepository $repository,
        ProcessoService $service
    )
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Item inicial do Controller
     *
     * @param Request $request
     * @return
     */
    public function index()
    {
        return view('processo.auditoria.index');
    }


    public function create()
    {
        return view('processo.auditoria.create');
    }

    public function store(Request $request)
    {
        $request = $this->service->store($request->all());

        if ($request['success']) {
            return redirect()->route('processo.show', ['id' => $request['data']])->withSuccess($request['messages']);
        } else {
            return redirect()->route('processo.create', ['action' => 'new'])
                ->withErrors($request['messages'])
                ->withInput();
        }
    }

    public function show($id)
    {
        $request = $this->service->show($id);

        if ($request['success']) {
            return view('processo.auditoria.show')
                ->withProcesso($request['data']);
        } else {
            return redirect()->route('processo.index')
                ->withErrors($request['messages']);
        }
    }

    public function edit($id)
    {
        $request = $this->service->edit($id);

        if ($request['success']) {
            return view('processo.auditoria.edit')
                ->withProcesso($request['data']);
        } else {
            return redirect()->back()
                ->withErrors($request['messages']);

        }
    }

    public function update(Request $request, $id)
    {
        $request = $this->service->update($request->all(), $id);

        if ($request['success']) {
            return redirect()->route('processo.show', ['id' => $request['data']])->withSuccess($request['messages']);
        } else {
            return redirect()->route('processo.index')
                ->withErrors($request['messages'])
                ->withInput();
        }
    }

}