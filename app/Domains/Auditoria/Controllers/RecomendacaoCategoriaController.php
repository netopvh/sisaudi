<?php
namespace App\Domains\Auditoria\Controllers;

use App\Core\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RecomendacaoCategoriaController extends Controller
{

    /**
     * Item inicial do Controller
     *
     * @param Request $request
     * @return
     */
    public function index()
    {
        return view('recomendacao.categorias.index');
    }

}