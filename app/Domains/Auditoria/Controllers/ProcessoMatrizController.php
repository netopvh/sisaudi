<?php

namespace App\Domains\Auditoria\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Domains\Auditoria\Repositories\Contracts\ProcessoRepository;
use App\Domains\Auditoria\Services\ProcessoMatrizService;
use Illuminate\Http\Request;

class ProcessoMatrizController extends Controller
{
    protected $repository;

    protected $service;

    public function __construct(ProcessoRepository $repository, ProcessoMatrizService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($processo)
    {
        $dtProcesso = $this->repository->findExists('id', $processo);
        return view('processo.matriz.create')
            ->withProcesso($dtProcesso);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createSub($processo, $questao)
    {
        $dtProcesso = $this->repository->findExists('id', $processo);
        $dtQuestao = $dtProcesso->questoes()->find($questao);

        return view('processo.matriz.create_sub')
            ->withQuestion($dtQuestao)
            ->withProcesso($dtProcesso);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $processo)
    {
        $processo = $this->repository->find($processo);
        $result = $processo->questoes()->create($request->all());
        if ($result) {
            return redirect()->route('processo.show', ['id' => $processo->id, 'action' => 'matriz'])
                ->withSuccess('Questão cadastrada com sucesso!');
        } else {
            return redirect()->back()
                ->withErrors('Erro ao cadastrar questão, tente novamente.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($processo, $questao)
    {
        $request = $this->service->edit($processo, $questao);

        if ($request['success']) {
            if($request['type'] == 'questao'){
                return view('processo.matriz.edit')
                    ->withMatriz($request['data']);
            }elseif($request['type'] == 'sub'){
                return view('processo.edit_sub')
                    ->withMatriz($request['data']);
            }
        } else {
            return redirect()->back()
                ->withErrors($request['messages']);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
}
