<?php

namespace App\Domains\Auditoria\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;

/**
 * Class FollowUpStatus.
 *
 * @package namespace App\Domains\Auditoria\Models;
 */
class FollowUpStatus extends Model implements Transformable, AuditableContract
{
    use TransformableTrait, Auditable, SoftDeletes;

    protected $table = 'follow_up_status';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['descricao'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Deixa registro em Maiúsculo
     *
     * @param $value
     */
    public function setDescricaoAttribute($value)
    {
        $this->attributes['descricao'] = mb_strtoupper($value, "UTF-8");
    }

}
