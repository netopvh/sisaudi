<?php

namespace App\Domains\Auditoria\Models;

use App\Domains\Access\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Demanda.
 *
 * @package namespace App\Domains\Auditoria\Models;
 */
class Demanda extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['autuacao','assunto','tipo_demanda','procedencia','departamento_id','orgao_id','criador_id'];

    public function interessados()
    {
        return $this->belongsToMany(UnidadeAdministrativa::class,'demandas_unidades','demanda_id','unidade_administrativa_id');
    }

    public function tramitacoes()
    {
        return $this->hasMany(Tramitacao::class,'demanda_id');
    }

    public function files()
    {
        return $this->hasMany(DemandaFile::class,'demanda_id');
    }

    public function criador()
    {
        return $this->belongsTo(User::class,'criador_id');
    }

    public function setAssuntoAttribute($value)
    {
        $this->attributes['assunto'] = mb_strtoupper($value,'UTF-8');
    }

    public function setAutuacaoAttribute($value)
    {
        $this->attributes['autuacao'] = Carbon::createFromFormat('d/m/Y',$value)->format('Y-m-d');
    }

    public function scopeDepartamento($query)
    {
        return $query->where('departamento_id', auth()->user()->departamento_id);
    }

}
