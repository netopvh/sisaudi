<?php

namespace App\Domains\Auditoria\Models;

use App\Domains\Access\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Tramitacao.
 *
 * @package namespace App\Domains\Auditoria\Models;
 */
class Tramitacao extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['processo_id','descricao','data','tipo','user_id','origem_id','destino_id'];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

}
