<?php

namespace App\Domains\Auditoria\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;

/**
 * Class TipoDocumento.
 *
 * @package namespace App\Domains\Auditoria\Models;
 */
class TipoDocumento extends Model implements Transformable, AuditableContract
{
    use TransformableTrait, Auditable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['descricao','prazo'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Deixa registro em Maiúsculo
     *
     * @param $value
     */
    public function setDescricaoAttribute($value)
    {
        $this->attributes['descricao'] = mb_strtoupper($value, "UTF-8");
    }

    public function getPrazoAttribute($value)
    {
        return $value ? 'SIM': 'NÃO';
    }
}
