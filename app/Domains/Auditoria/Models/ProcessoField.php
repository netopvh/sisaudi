<?php

namespace App\Domains\Auditoria\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;

class ProcessoField extends Model implements Transformable, AuditableContract
{
    use TransformableTrait, Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['processo_id','name','content'];//

    public $timestamps = false;
}
