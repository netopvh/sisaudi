<?php

namespace App\Domains\Auditoria\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;

/**
 * Class Risco.
 *
 * @package namespace App\Domains\Auditoria\Models;
 */
class Risco extends Model implements Transformable, AuditableContract
{
    use TransformableTrait, Auditable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['descricao','impacto','mitigacao'];

    public function setDescricaoAttribute($value)
    {
        $this->attributes['descricao'] = mb_strtoupper($value, "UTF-8");
    }

    public function setImpactoAttribute($value)
    {
        $this->attributes['impacto'] = mb_strtoupper($value, "UTF-8");
    }

    public function setMitigacaoAttribute($value)
    {
        $this->attributes['mitigacao'] = mb_strtoupper($value, "UTF-8");
    }

}
