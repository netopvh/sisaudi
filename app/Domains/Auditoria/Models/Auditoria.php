<?php

namespace App\Domains\Auditoria\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;

/**
 * Class Auditoria.
 *
 * @package namespace App\Domains\Auditoria\Models;
 */
class Auditoria extends Model implements Transformable, AuditableContract
{
    use TransformableTrait, Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['processo_id','status','restrito','inicio_proposto','fim_proposto','portaria_num','portaria_data'];

    public function processo()
    {
        return $this->belongsTo(Processo::class,'processo_id');
    }

}
