<?php

namespace App\Domains\Auditoria\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;

/**
 * Class UnidadeAdministrativa.
 *
 * @package namespace App\Domains\Auditoria\Models;
 */
class UnidadeAdministrativa extends Model implements Transformable, AuditableContract
{
    use TransformableTrait, Auditable, SoftDeletes;

    protected $table = 'unidades_administrativas';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nome', 'sigla','adm_direta','secretario'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Deixa Nome em Maiúsculo
     *
     * @param $value
     */
    public function setNomeAttribute($value)
    {
        $this->attributes['nome'] = mb_strtoupper($value, "UTF-8");
    }

    /**
     * Deixa Sigla em Maiúsculo
     *
     * @param $value
     */
    public function setSiglaAttribute($value)
    {
        $this->attributes['sigla'] = mb_strtoupper($value, "UTF-8");
    }

    public function setSecretarioAttribute($value)
    {
        $this->attributes['secretario'] = mb_strtoupper($value, "UTF-8");
    }
}
