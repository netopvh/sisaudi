<?php

namespace App\Domains\Auditoria\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class DemandaFile.
 *
 * @package namespace App\Domains\Auditoria\Models;
 */
class DemandaFile extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['demanda_id','tipo_doc_id','tipo_file','descricao','path','conteudo','prazo'];

    public function tipo()
    {
        return $this->belongsTo(TipoDocumento::class,'tipo_doc_id');
    }

    public function setDescricaoAttribute($value)
    {
        $this->attributes['descricao'] = mb_strtoupper($value,'UTF-8');
    }

    public function setPrazoAttribute($value)
    {
        $this->attributes['prazo'] = is_null($value) ? 15:$value;
    }
}
