<?php

namespace App\Domains\Auditoria\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;

/**
 * Class ProcessoMatriz.
 *
 * @package namespace App\Domains\Auditoria\Models;
 */
class ProcessoMatriz extends Model implements Transformable, AuditableContract
{
    use TransformableTrait, Auditable;

    protected $table = 'processo_matriz';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['processo_id','questao_id','subquestao','questao','risco','procedimento','criterio','informacoes_requeridas',
        'fonte_informacao','limitacoes','possiveis_achados'];

    public function subquestoes()
    {
        return $this->hasMany(ProcessoMatriz::class, 'questao_id');
    }

}
