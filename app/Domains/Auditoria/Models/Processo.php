<?php

namespace App\Domains\Auditoria\Models;

use App\Domains\Access\Models\User;
use App\Domains\Auditoria\Models\ProcessoField;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;

/**
 * Class Processo.
 *
 * @package namespace App\Domains\Auditoria\Models;
 */
class Processo extends Model implements Transformable, AuditableContract
{
    use TransformableTrait, Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'numero_processo','autuacao','tipo_processo','assunto','auditoria_obrigatoria','inicio_proposto',
        'fim_proposto','num_portaria_inicial','data_portaria_inicial','status','gestor_id','departamento_id','criador_id','tipo']
    ;

    protected $dates = ['autuacao','inicio_proposto','fim_proposto','data_portaria_inicial'];

    public function auditados()
    {
        return $this->belongsToMany(UnidadeAdministrativa::class,'processo_unidades','processo_id','unidade_administrativa_id');
    }

    public function auditores()
    {
        return $this->belongsToMany(User::class,'processo_auditores','processo_id','user_id');
    }

    public function criador()
    {
        return $this->belongsTo(User::class, 'criador_id');
    }

    public function gestor()
    {
        return $this->belongsTo(User::class, 'gestor_id');
    }

    public function fields()
    {
        return $this->hasMany(ProcessoField::class,'processo_id');
    }

    public function questoes()
    {
        return $this->hasMany(ProcessoMatriz::class, 'processo_id')->where('questao_id','=',null);
    }

    public function setAssuntoAttribute($value)
    {
        $this->attributes['assunto'] = mb_strtoupper($value, "UTF-8");
    }

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$value)->format('d/m/Y');
    }

    public function scopeDepartamento($query)
    {
        return $query->where('departamento_id', auth()->user()->departamento_id)
            ->where('status','E')
            ->orWhere('status','H');
    }
    public function scopeInterno($query)
    {
        return $query->where('tipo', 'I');
    }
    public function scopeAuditoria($query)
    {
        return $query->where('tipo', 'A');
    }

}
