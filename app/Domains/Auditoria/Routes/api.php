<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('audits')->middleware('auth:api')->group(function (){
    Route::prefix('/tables')->group(function (){
        Route::resource('recomendation-types', 'Api\RecomendacaoTipoApiController');
        Route::resource('recomendation-categories', 'Api\RecomendacaoCategoriaApiController');
        Route::resource('recomendation-gravity', 'Api\RecomendacaoGravidadeApiController');
        Route::resource('follow-status', 'Api\FollowUpStatusApiController');
        Route::resource('administrative-units', 'Api\UnidadeAdministrativaApiController');
        Route::resource('doc-types', 'Api\TipoDocumentoApiController');
        Route::resource('possible-findings', 'Api\PossivelAchadoApiController');
        Route::resource('risks', 'Api\RiscoApiController');
    });
    Route::prefix('/process')->group(function (){
        //Processos de Auditoria
        Route::get('/','Api\ProcessoApiController@index');
        Route::get('/type/{type}','Api\ProcessoApiController@type');
        Route::prefix('/matriz')->group(function (){
            Route::get('/total-subquestions/{id}','Api\ProcessoMatrizApiController@getTotalSub');
            Route::delete('/destroy/{id}','Api\ProcessoMatrizApiController@destroy');
        });
        Route::get('/{id}','Api\ProcessoApiController@getProcesso');
    });
    Route::prefix('/demands')->group(function (){
        Route::get('/','Api\DemandaApiController@index');
    });
    Route::resource('process-criteria','Api\ProcessoCriterioApiController');
    Route::resource('auditing', 'Api\AuditoriaApiController');
});

