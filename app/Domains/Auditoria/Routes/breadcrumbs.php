<?php
/**
 * TIPO RECOMENDACAO
 */
Breadcrumbs::for('recomendacao_tipos.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Tipos de Recomendação', route('recomendacao_tipos.index'));
});

/**
 * CATEGORIA RECOMENDACAO
 */
Breadcrumbs::for('recomendacao_categorias.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Categorias de Recomendação', route('recomendacao_categorias.index'));
});

/**
 * GRAVIDADE RECOMENDACAO
 */
Breadcrumbs::for('recomendacao_gravidades.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Categorias de Recomendação', route('recomendacao_gravidades.index'));
});

/**
 * STATUS DO FOLLOW UP
 */
Breadcrumbs::for('follow_up_status.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Status de Follow Up', route('follow_up_status.index'));
});

/**
 * Unidades Administrativas
 */
Breadcrumbs::for('administrative_units.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Unidades Administrativas', route('administrative_units.index'));
});

/**
 * TIPO DE DOCUMENTOS
 */
Breadcrumbs::for('tipo_documentos.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Tipo de Documentos', route('tipo_documentos.index'));
});

Breadcrumbs::for('possivel_achado.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Possível Achado', route('possivel_achado.index'));
});

Breadcrumbs::for('risco.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Riscos', route('risco.index'));
});

Breadcrumbs::for('processo.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Processo de Auditoria', route('processo.index'));
});

Breadcrumbs::for('processo.create', function ($trail) {
    $trail->parent('processo.index');
    $trail->push('Novo', route('processo.create'));
});

Breadcrumbs::for('processo.show', function ($trail) {
    $trail->parent('processo.index');
    $trail->push('Exibir', '');
});

Breadcrumbs::for('processo.edit', function ($trail) {
    $trail->parent('processo.index');
    $trail->push('Editar', '');
});

Breadcrumbs::for('processo.questao.create', function ($trail) {
    $trail->parent('processo.index');
    $trail->push('Questão de Auditoria', '');
});

//Processos Internos

Breadcrumbs::for('demanda.index', function ($trail) {
    $trail->parent('home');
    $trail->push('Minhas Demandas', route('demanda.index'));
});

Breadcrumbs::for('demanda.create', function ($trail) {
    $trail->parent('demanda.index');
    $trail->push('Cadastro de Demandas', route('demanda.create'));
});

Breadcrumbs::for('demanda.show', function ($trail) {
    $trail->parent('demanda.index');
    $trail->push('Exibir Demanda', '');
});

Breadcrumbs::for('demanda.files.create', function ($trail) {
    $trail->parent('demanda.index');
    $trail->push('Inserir Documento', '');
});