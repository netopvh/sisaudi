<?php

use Illuminate\Support\Facades\Route;

Route::prefix('audits')->middleware('auth')->group(function (){
    Route::prefix('tables')->group(function (){
        Route::prefix('recomendation-types')->group(function (){
            Route::get('/','RecomendacaoTipoController@index')->middleware('permission:ver-tipo-recomendacao')->name('recomendacao_tipos.index');
        });
        Route::prefix('recomendation-categories')->group(function (){
            Route::get('/','RecomendacaoCategoriaController@index')->middleware('permission:ver-categoria-recomendacao')->name('recomendacao_categorias.index');
        });
        Route::prefix('recomendation-gravity')->group(function (){
            Route::get('/','RecomendacaoGravidadeController@index')->middleware('permission:ver-gravidade-recomendacao')->name('recomendacao_gravidades.index');
        });
        Route::prefix('follow-status')->group(function (){
            Route::get('/','FollowUpStatusController@index')->middleware('permission:ver-status-acompanhamento')->name('follow_up_status.index');
        });
        Route::prefix('administrative-units')->group(function (){
            Route::get('/','UnidadeAdministrativaController@index')->middleware('permission:ver-unidades')->name('administrative_units.index');
        });
        Route::prefix('doc-types')->group(function (){
            Route::get('/','TipoDocumentoController@index')->middleware('permission:ver-tipo-documentos')->name('tipo_documentos.index');
        });
        Route::prefix('possible-findings')->group(function (){
            Route::get('/','PossivelAchadoController@index')->middleware('permission:ver-possiveis-achados')->name('possivel_achado.index');
        });

        Route::prefix('risks')->group(function (){
            Route::get('/','RiscoController@index')->middleware('permission:ver-usuario')->name('risco.index');
        });

    });
    Route::prefix('process')->group(function (){
        // Processos de Auditoria
        Route::get('/','ProcessoController@index')->middleware('permission:ver-processos-auditaveis')->name('processo.index');
        Route::get('/create','ProcessoController@create')->middleware('permission:criar-processos-auditaveis')->name('processo.create');
        Route::post('/store','ProcessoController@store')->middleware('permission:criar-processos-auditaveis')->name('processo.store');
        Route::get('/{id}/show','ProcessoController@show')->middleware('permission:ver-processos-auditaveis')->name('processo.show');
        Route::get('/{id}/edit','ProcessoController@edit')->middleware('permission:editar-processos-auditaveis')->name('processo.edit');
        Route::patch('/{id}','ProcessoController@update')->middleware('permission:editar-processos-auditaveis')->name('processo.update');
        Route::prefix('matriz')->group(function (){
            Route::get('/{processo}/question','ProcessoMatrizController@create')->middleware('permission:criar-matriz')->name('processo.matriz.create');
            Route::post('/{processo}/question','ProcessoMatrizController@store')->middleware('permission:criar-matriz')->name('processo.matriz.store');
            Route::get('/{processo}/question/{questao}','ProcessoMatrizController@createSub')->middleware('permission:criar-matriz')->name('processo.matriz.create.sub');
            Route::get('/{processo}/question/{questao}/edit','ProcessoMatrizController@edit')->middleware('permission:editar-matriz')->name('processo.matriz.edit');
        });
    });

    //Processos Internos
    Route::prefix('demands')->group(function (){
        Route::get('/','DemandaController@index')->middleware('permission:ver-processos-auditaveis')->name('demanda.index');
        Route::get('/create','DemandaController@create')->middleware('permission:ver-processos-auditaveis')->name('demanda.create');
        Route::post('/create','DemandaController@store')->middleware('permission:ver-processos-auditaveis')->name('demanda.store');
        Route::get('/{id}/show','DemandaController@show')->middleware('permission:ver-processos-auditaveis')->name('demanda.show');
        Route::get('{id}/files/create','DemandaFileController@create')->middleware('permission:ver-processos-auditaveis')->name('demanda.files.create');
        Route::post('{id}/files/create','DemandaFileController@store')->middleware('permission:ver-processos-auditaveis')->name('demanda.files.store');
        Route::get('{id}/files/{file}/show','DemandaFileController@show')->middleware('permission:ver-processos-auditaveis')->name('demanda.files.show');
    });

    Route::prefix('auditing')->group(function (){
        Route::get('/','AuditoriaController@index')->middleware('permission:ver-usuario')->name('auditoria.index');
        Route::get('/create','AuditoriaController@create')->middleware('permission:ver-usuario')->name('auditoria.create');
        Route::post('/store','AuditoriaController@store')->middleware('permission:ver-usuario')->name('auditoria.store');
        Route::get('/{id}/show','AuditoriaController@show')->middleware('permission:ver-usuario')->name('auditoria.show');
        Route::get('/{id}/edit','AuditoriaController@edit')->middleware('permission:ver-usuario')->name('auditoria.edit');
    });

    Route::prefix('monitoring')->group(function (){
        Route::get('/','AcompanhamentoController@index')->middleware('permission:ver-usuario')->name('acompanhamento.index');
        Route::get('/create','AcompanhamentoController@create')->middleware('permission:ver-usuario')->name('acompanhamento.create');
        Route::post('/store','AcompanhamentoController@store')->middleware('permission:ver-usuario')->name('acompanhamento.store');
        Route::get('/{id}/show','AcompanhamentoController@show')->middleware('permission:ver-usuario')->name('acompanhamento.show');
        Route::get('/{id}/edit','AcompanhamentoController@edit')->middleware('permission:ver-usuario')->name('acompanhamento.edit');
    });
});