<?php

namespace App\Domains\Auditoria\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ProcessoValidator.
 *
 * @package namespace App\Domains\Auditoria\Validators;
 */
class ProcessoValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'assunto' => 'required',
            'auditados' => 'required',
            'desc_sumaria' => 'required_if:tipo_processo,==,APG',
            'exercicio_anterior' => 'required_if:tipo_processo,==,APG',
            'desc_sumaria' => 'required_if:exercicio_atual,==,APG',
            'departamento_id' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'assunto' => 'required',
            'auditados' => 'required',
            'desc_sumaria' => 'required_if:tipo_processo,==,APG',
            'exercicio_anterior' => 'required_if:tipo_processo,==,APG',
            'desc_sumaria' => 'required_if:exercicio_atual,==,APG',
            'departamento_id' => 'required'
        ],
    ];

    protected $messages = [
        'required' => '* O :attribute é obrigatório',
        'required_if' => '* O :attribute não pode ser vazio',
        'unique' => '* Já possui esta :attribute no banco de dados',
    ];

    protected $attributes = [
        'numero_processo' => 'número do processo',
        'assunto' => 'assunto',
        'auditados' => 'unidades auditadas',
        'inicio_atividades' => 'início das atividades',
        'sigiloso' => 'sigiloso',
        'escopo' => 'escopo',
        'departamento_id' => 'departamento',
    ];
}
