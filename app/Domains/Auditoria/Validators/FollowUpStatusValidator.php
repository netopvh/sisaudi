<?php

namespace App\Domains\Auditoria\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class FollowUpStatusValidator.
 *
 * @package namespace App\Domains\Auditoria\Validators;
 */
class FollowUpStatusValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'descricao' => 'required|unique:follow_up_status'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'descricao' => 'required|unique:follow_up_status',
        ],
    ];

    protected $messages = [
        'required' => '* O :attribute é obrigatório',
        'unique' => '* Já possui esta :attribute no banco de dados',
    ];

    protected $attributes = [
        'descricao' => 'descrição',
    ];
}
