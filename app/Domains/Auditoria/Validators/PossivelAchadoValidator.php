<?php

namespace App\Domains\Auditoria\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class PossivelAchadoValidator.
 *
 * @package namespace App\Domains\Auditoria\Validators;
 */
class PossivelAchadoValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'nome' => 'required|unique:possiveis_achados',
            'status' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'nome' => 'required|unique:possiveis_achados',
            'status' => 'required'
        ],
    ];

    protected $messages = [
        'required' => '* O :attribute é obrigatório',
        'unique' => '* Já possui esta :attribute no banco de dados',
    ];

    protected $attributes = [
        'nome' => 'nome',
        'status' => 'status',
    ];
}
