<?php

namespace App\Domains\Auditoria\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class RecomendacaoCategoriaValidator.
 *
 * @package namespace App\Domains\Auditoria\Validators;
 */
class RecomendacaoCategoriaValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'descricao' => 'required|unique:recomendacao_categorias'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'descricao' => 'required|unique:recomendacao_categorias',
        ],
    ];

    protected $messages = [
        'required' => '* O :attribute é obrigatório',
        'unique' => '* Já possui esta :attribute no banco de dados',
    ];

    protected $attributes = [
        'descricao' => 'descrição',
    ];
}
