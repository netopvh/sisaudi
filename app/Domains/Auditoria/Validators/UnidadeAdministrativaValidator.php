<?php

namespace App\Domains\Auditoria\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class UnidadeAdministrativaValidator.
 *
 * @package namespace App\Domains\Auditoria\Validators;
 */
class UnidadeAdministrativaValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'nome' => 'required|unique:unidades_administrativas',
            'sigla' => 'required|unique:unidades_administrativas',
            'adm_direta' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'nome' => 'required|unique:unidades_administrativas',
            'sigla' => 'required|unique:unidades_administrativas',
            'adm_direta' => 'required'
        ],
    ];

    protected $messages = [
        'required' => '* O :attribute é obrigatório',
        'unique' => '* Já possui esta :attribute no banco de dados',
    ];

    protected $attributes = [
        'nome' => 'nome',
        'sigla' => 'sigla',
        'adm_direta' => 'administração direta',
    ];
}
