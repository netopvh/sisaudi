<?php

namespace App\Domains\Auditoria\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class TipoDocumentoValidator.
 *
 * @package namespace App\Domains\Auditoria\Validators;
 */
class TipoDocumentoValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'descricao' => 'required|unique:tipo_documentos'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'descricao' => 'required|unique:tipo_documentos',
        ],
    ];

    protected $messages = [
        'required' => '* O :attribute é obrigatório',
        'unique' => '* Já possui este :attribute no banco de dados',
    ];

    protected $attributes = [
        'descricao' => 'descrição',
    ];
}
