<?php

namespace App\Domains\Auditoria\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class RecomendacaoTipoValidator.
 *
 * @package namespace App\Domains\Auditoria\Validators;
 */
class RecomendacaoTipoValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'descricao' => 'required|unique:recomendacao_tipos'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'descricao' => 'required|unique:recomendacao_tipos',
        ],
    ];

    protected $messages = [
        'required' => '* O :attribute é obrigatório',
        'unique' => '* Já possui este :attribute no banco de dados',
    ];

    protected $attributes = [
        'descricao' => 'descrição',
    ];
}
