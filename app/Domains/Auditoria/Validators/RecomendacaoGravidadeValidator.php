<?php

namespace App\Domains\Auditoria\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class RecomendacaoGravidadeValidator.
 *
 * @package namespace App\Domains\Auditoria\Validators;
 */
class RecomendacaoGravidadeValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'descricao' => 'required|unique:recomendacao_gravidades'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'descricao' => 'required|unique:recomendacao_gravidades',
        ],
    ];

    protected $messages = [
        'required' => '* O :attribute é obrigatório',
        'unique' => '* Já possui esta :attribute no banco de dados',
    ];

    protected $attributes = [
        'descricao' => 'descrição',
    ];
}
