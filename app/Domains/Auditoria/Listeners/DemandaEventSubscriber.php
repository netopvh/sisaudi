<?php

namespace App\Domains\Auditoria\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DemandaEventSubscriber
{

    public function onDemandaCreated($event)
    {
        $event->demanda->tramitacoes()->create([
            'data' => \Carbon\Carbon::now(),
            'descricao' => 'Cadastrou uma nova demanda',
            'tipo' => 'N',
            'user_id' => auth()->user()->id
        ]);
    }

    public function onDemandaFileInserted($event)
    {
        $lastFileName = $event->demanda->files()->get()->last()->descricao;
        $event->demanda->tramitacoes()->create([
            'data' => \Carbon\Carbon::now(),
            'descricao' => 'Inseriu o documento '. $lastFileName,
            'tipo' => 'I',
            'user_id' => auth()->user()->id
        ]);
    }

    public function subscribe($events)
    {
        $events->listen(
            'App\Domains\Auditoria\Events\DemandaCreated',
            'App\Domains\Auditoria\Listeners\DemandaEventSubscriber@onDemandaCreated'
        );

        $events->listen(
            'App\Domains\Auditoria\Events\DemandaFileInserted',
            'App\Domains\Auditoria\Listeners\DemandaEventSubscriber@onDemandaFileInserted'
        );
    }
}
