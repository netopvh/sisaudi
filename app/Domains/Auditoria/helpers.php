<?php

if (!function_exists("especial_ucwords")) {
    function especial_ucwords(string $attribute)
    {
        if($attribute=="")
            return $attribute;
        $string = strtolower($attribute);
        $string[0] = strtoupper($attribute[0]);
        for($atual = 1;$atual <= strlen($string);$atual++)
        {
            if($string[$atual]==" ")
                if($string[$atual+1]!="d")
                    $string[$atual+1]=strtoupper($string[$atual+1]);
                else
                    if(!($string[$atual+3]==" " || $string[$atual+4]==" "))
                        $string[$atual+1]=strtoupper($string[$atual+1]);
        }
        return $string;
    }
}

if(! function_exists('status_processo')){
    function status_processo($attribute)
    {
        $status = \App\Domains\Auditoria\Enum\ProcessoStatusEnum::toArray();
        return $status[$attribute];
    }
}

if(! function_exists('text_to_array_fields')){
    function text_to_array_fields($text)
    {
        $tags = "div|b";
        $string = $text;
        $string = preg_replace( "#<\s*\/?(".$tags.")\s*[^>]*?>#im" , '|' , $string );
        $string = preg_replace('/\|+/', '|', $string);
        $string = array_filter( explode( '|' , $string ) );
        $newString = [];
        foreach ($string as $item) {
            $itemAtual = trim(preg_replace('/\s+/', ' ', $item));
            if($itemAtual != "" && !str_contains($itemAtual,'&gt; 1.') && !str_contains($itemAtual,'&gt; 2.') && !str_contains($itemAtual,'&gt; 3.')){
                $newString[] = $itemAtual;
            }
        }
        return explode("<hr />",implode('',$newString));
    }
}

if(! function_exists('str_to_array')){
    function str_to_array($text, $element)
    {
        $items = [];
        $doc = new \DOMWrap\Document();
        $doc->html($text);
        $nodes = $doc->find($element);
        $contents =  $nodes->contents();
        foreach ($contents as $content) {
            $items[] = $content->getOuterHtml();
        }
        return $items;
    }
}

if(! function_exists('last_num_processo')){
    function last_num_processo($last)
    {
        $primeiraAcao = explode('.',$last);
        $segundaAcao = explode('/',$primeiraAcao[1]);

        return (int) $segundaAcao[0];
    }
}

if(! function_exists('date_to_database')){
    function date_to_database($date)
    {
        return \Carbon\Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }
}

if(! function_exists('date_to_format')){
    function date_to_format($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }
}

if(! function_exists('substracao')){
    function substracao($first, $last)
    {
        return $last - $first;
    }
}
