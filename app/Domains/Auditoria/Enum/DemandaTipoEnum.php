<?php

namespace App\Domains\Auditoria\Enum;

use MyCLabs\Enum\Enum;

class DemandaTipoEnum extends Enum
{
    private const A = "ANÁLISE DE PROCESSOS";
    private const B = "ACOMPANHAMENTO DE RECOMENDAÇÕES/DECISÕES/SOLICITAÇÕES DE CONTROLE EXTERNO";
    private const C = "VISTORIAS E INSPEÇÕES";
    private const D = "OUTROS ATIVIDADES";
}