<?php

namespace App\Domains\Auditoria\Enum;

use MyCLabs\Enum\Enum;

class ProcessoTipoEnum extends Enum
{
    private const TCN = "TOMADA DE CONTAS";
    private const TCE = "TOMADA DE CONTAS ESPECIAL";
    private const APG = "AVALIAÇÃO DE PROGRAMA DE GOVERNO";
    private const AOP = "AUDITORIA OPERACIONAL";
    private const OUA = "OUTRAS AUDITORIAS";
}