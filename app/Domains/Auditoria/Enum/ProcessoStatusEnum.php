<?php

namespace App\Domains\Auditoria\Enum;

use MyCLabs\Enum\Enum;

class ProcessoStatusEnum extends Enum
{
    private const E = "EM ELABORAÇÃO";
    private const H = "EM HOMOLOGAÇÃO";
    private const A = "HOMOLOGADO";
    private const P = "EM EXECUÇÃO";
    private const F = "FINALIZADO";
}