<?php

namespace App\Domains\Auditoria\Enum;

use MyCLabs\Enum\Enum;

class ModalidadeAuditoriaEnum extends Enum
{
    private const AF = "AUDITORIA FISCAL";
    private const AO = "AUDITORIA OPERACIONAL";
    private const AC = "AUDITORIA CONTÁBIL";
    private const AG = "AUDITORIA DE AVALIAÇÃO DA GESTÃO";
    private const AS = "AUDITORIA DE SISTEMAS INFORMATIZADOS";
    private const AQ = "AUDITORIA DE QUALIDADE";
    private const AA = "AUDITORIA AMBIENTAL";
    private const OA = "OUTRAS AUDITORIAS";
}