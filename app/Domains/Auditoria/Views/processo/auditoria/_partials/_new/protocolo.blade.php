<fieldset>
    <legend>Protocolo</legend>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="radio">
                            <label>
                                <input type="radio" name="protocolo"
                                       class="styled"
                                       value="A"
                                       checked="checked">
                                Automático
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="protocolo" value="M"
                                       class="styled">
                                Informado
                            </label>
                        </div>
                    </div>
                </div>
                <div id="dvProcessoProtocoloCreate" class="collapse">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="text-bold">Número do Processo: <span
                                        class="text-danger"> *</span></label>
                            <input type="text" class="form-control"
                                   name="numero_processo"
                                   value="{{ old('numero_processo') }}">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="text-bold">Data de Autuação: <span
                                        class="text-danger"> *</span></label>
                            <input type="text" class="form-control datepicker"
                                   name="autuacao"
                                   value="{{ old('autuacao') }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</fieldset>
<br>