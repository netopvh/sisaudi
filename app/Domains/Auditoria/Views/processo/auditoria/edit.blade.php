@extends('layouts.app')

@section('page-header')
    <div class="page-header page-header-default page-header-sm"
         style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Processo de Auditoria</span></h5>
            </div>
        </div>
        {{ Breadcrumbs::render('processo.edit') }}
    </div>
@stop

@section('content')
    <div class="row">
        <div class=" col-md-offset-1 col-md-10">
            <div class="panel panel-default border-grey">
                <div class="panel-heading">
                    <i class="icon-table"></i> <span class="text-bold">Formulário de Alteração</span>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <form action="{{ route('processo.update',['id' => $processo->id]) }}" method="post" class="form-validate" autocomplete="off">
                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}
                        <input type="hidden" id="processoId" name="processo_id" value="{{ $processo->id }}">
                        <div id="tabProcesso" class="" style="width:100%;height:auto">
                            <!--  PRIMEIRO PANEL -->
                            <div title="Informações para o Planejamento" id="tbGerais" style="padding:10px">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="text-bold">Número do processo:</label>
                                            <input type="text" class="form-control"
                                                   value="{{ $processo->numero_processo }}" disabled>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="text-bold">Data da Autuação:</label>
                                            <input type="text" class="form-control"
                                                   value="{{ $processo->autuacao->format('d/m/Y') }}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="text-bold">Tipo de Procedimento:</label>
                                            <select id="selProcessoTipo" name="tipo_processo"
                                                    class="select-search" required>
                                                <option value="">Selecione o Tipo de Processo</option>
                                                @foreach($processoTipos as $key => $value)
                                                    <option value="{{ $key }}" {{ $processo->tipo_processo == $key?'selected':'' }}>{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div id="dvProcessoCreate" class="collapse">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="text-bold">Assunto: <span
                                                            class="text-danger"> *</span></label>
                                                <input type="text" name="assunto" class="form-control text-uppercase"
                                                       value="{{ $processo->assunto }}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-group panel-group-control content-group-lg"
                                         id="accordion-control">
                                        <div class="panel panel-white collapse" id="processoTabEscopo">
                                            <div class="panel-heading">
                                                <h6 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion-control"
                                                       href="#accordion-control-group1" aria-expanded="true" class="">
                                                        Escopo
                                                    </a>
                                                </h6>
                                            </div>
                                            <div id="accordion-control-group1" class="panel-collapse collapse in"
                                                 aria-expanded="true" style="">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="label-full">
                                                    <textarea name="escopo" id="processoEscopo"
                                                              rows="5" required>{{ $processo->fields->escopo }}</textarea>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-white">
                                            <div class="panel-heading">
                                                <h6 class="panel-title">
                                                    <a class="collapsed" data-toggle="collapse"
                                                       data-parent="#accordion-control" href="#accordion-control-group2"
                                                       aria-expanded="false">Descrição Sumária</a>
                                                </h6>
                                            </div>
                                            <div id="accordion-control-group2" class="panel-collapse collapse"
                                                 aria-expanded="false" style="height: 0px;">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="label-full">
                                                        <textarea name="desc_sumaria" id="processoDescSum" rows="5"
                                                                  class="" required>
                                                            @if($processo->tipo_processo == 'APG')
                                                                @include('processo.auditoria._contents.desc_sum', $processo)
                                                            @endif
                                                        </textarea>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-white">
                                            <div class="panel-heading">
                                                <h6 class="panel-title">
                                                    <a class="collapsed" data-toggle="collapse"
                                                       data-parent="#accordion-control" href="#accordion-control-group3"
                                                       aria-expanded="false">Exercício Anterior</a>
                                                </h6>
                                            </div>
                                            <div id="accordion-control-group3" class="panel-collapse collapse"
                                                 aria-expanded="false" style="height: 0px;">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="label-full">
                                                        <textarea name="exercicio_anterior" id="processoExercicioAnt" rows="5"
                                                                  class="" required>
                                                            @if($processo->tipo_processo == 'APG')
                                                                @include('processo.auditoria._contents.exercicio_ante', $processo)
                                                            @endif
                                                        </textarea>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-white">
                                            <div class="panel-heading">
                                                <h6 class="panel-title">
                                                    <a class="collapsed" data-toggle="collapse"
                                                       data-parent="#accordion-control" href="#accordion-control-group4"
                                                       aria-expanded="false">Exercício Atual</a>
                                                </h6>
                                            </div>
                                            <div id="accordion-control-group4" class="panel-collapse collapse"
                                                 aria-expanded="false" style="height: 0px;">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="label-full">
                                                        <textarea name="exercicio_atual" id="processoExercicioAtual" rows="5"
                                                                  class="" required>
                                                            @if($processo->tipo_processo == 'APG')
                                                                @include('processo.auditoria._contents.exercicio_atual', $processo)
                                                            @endif
                                                        </textarea>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- SEGUNDO PANEL -->
                            <div title="Dados Gerais" id="tbPlanejamento" style="padding:10px">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="text-bold display-block">Auditoria
                                                Obrigatória:</label>
                                            <label class="radio-inline">
                                                <input type="radio" name="auditoria_obrigatoria" value="S"
                                                       class="styled" {{ $processo->auditoria_obrigatoria == 'S'?'checked':'' }}>
                                                Sim
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="auditoria_obrigatoria" value="N" checked
                                                       class="styled" {{ $processo->auditoria_obrigatoria == 'N'?'checked':'' }}>
                                                Não
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="text-bold">Início Proposto:</label>
                                            <input type="text" id="" name="inicio_proposto"
                                                   class="form-control datepicker"
                                                   value="{{ $processo->inicio_proposto->format('d/m/Y') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="text-bold">Fim Proposto:</label>
                                            <input type="text" name="fim_proposto" class="form-control datepicker"
                                                   value="{{ $processo->fim_proposto->format('d/m/Y') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="text-bold">Nº Portaria:</label>
                                            <input type="text" name="num_portaria_inicial" class="form-control"
                                                   value="{{ $processo->num_portaria_inicial }}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="text-bold">Data:</label>
                                            <input type="text" name="data_portaria_inicial"
                                                   class="form-control datepicker"
                                                   value="{{ $processo->data_portaria_inicial->format('d/m/Y') }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-bold">Unidades Auditadas: <span
                                                        class="text-danger"> *</span></label>
                                            <label class="label-full">
                                                <select name="auditados[]" id="selProcessoUniAdm" multiple required>
                                                    @foreach($unidadesAdministrativas as $unidadeAdministrativa)
                                                        <option value="{{ $unidadeAdministrativa->id }}" {{ in_array($unidadeAdministrativa->id,$processo->auditados->pluck('id')->toArray()) ? 'selected':'' }}>{{ $unidadeAdministrativa->nome }}
                                                            - {{ $unidadeAdministrativa->sigla }}</option>
                                                    @endforeach
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-bold">Departamento Executor:</label>
                                            <label class="label-full">
                                                <select name="departamento_id" id="processoDepartamento" class="select-ui" style="width: 100%" required>
                                                    <option></option>
                                                    @foreach($departamentos as $departamento)
                                                        <option value="{{ $departamento->id }}" {{ $processo->departamento_id == $departamento->id?'selected':'' }}>{{ $departamento->descricao }}</option>
                                                    @endforeach
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-bold">Servidores Portariados: <span class="text-danger">*</span></label>
                                            <label class="label-full">
                                                <select name="auditores[]" multiple id="processoAuditores" style="width: 100%" required>
                                                    @foreach($usuarios as $usuario)
                                                        <option value="{{ $usuario->id }}" {{ in_array($usuario->id,$processo->auditores->pluck('id')->toArray()) ? 'selected':'' }}>{{ $usuario->nome }}</option>
                                                    @endforeach
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-bold">Gestor da Auditoria: <span class="text-danger">*</span></label>
                                            <label class="label-full">
                                                <select name="gestor_id" id="processoGestorAud" class="select-ui" style="width: 100%" required>
                                                    <option></option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="{{ route('processo.show',['id' => $processo->id]) }}"
                                           class="btn btn-info legitRipple"><i
                                                    class="icon-database-arrow"></i> Retornar</a>
                                        <button type="submit" id="btnProcesso" class="btn btn-primary legitRipple"><i
                                                    class="icon-database-insert"></i> Gravar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--
    <div id="dlgAdministrativeUnitsProcess">
        <form method="post" id="frmAdministrativeUnits">
            {{ csrf_field() }}
            <input type="hidden" name="item" id="id">
            <input type="hidden" name="action">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label>Nome: *</label>
                        <input type="text" name="nome" id="nome" class="form-control text-uppercase" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Sigla: *</label>
                        <input type="text" name="sigla" id="sigla" class="form-control text-uppercase" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="display-block">Administração Direta: *</label>
                        <select name="adm_direta" id="adm_direta" class="form-control" required="">
                            <option value=""></option>
                            <option value="1">SIM</option>
                            <option value="0">NÃO</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-7">
                    <label>Secretário / Responsável</label>
                    <input type="text" class="form-control text-uppercase" name="secretario" id="secretario">
                </div>
            </div>
        </form>
    </div> -->
@stop
