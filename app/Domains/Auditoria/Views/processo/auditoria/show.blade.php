@extends('layouts.app')

@section('page-header')
    <div class="page-header page-header-default page-header-sm"
         style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Processo: {{ $processo->numero_processo }}</span></h5>
            </div>
        </div>
        {{ Breadcrumbs::render('processo.show') }}
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-grey">
                <div class="panel-heading">
                    <h6 class="panel-title">Processo Nº {{ $processo->numero_processo }}</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="text-bold">Ações do Processo:</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ route('processo.show',['id' => $processo->id]) }}"
                                       class="btn btn-primary btn-raised legitRipple"><i
                                                class="icon-checkmark-circle" data-popup="tooltip"
                                                title="Dados do Processo"></i>
                                    </a>
                                    @permission('editar-processos-auditaveis')
                                    <a href="{{ route('processo.edit',['id' => $processo->id]) }}"
                                       class="btn btn-primary btn-raised legitRipple"><i
                                                class="icon-pencil7" data-popup="tooltip"
                                                title="Modificar Processo"></i>
                                    </a>
                                    @endpermission
                                    <a href="{{ route('processo.show',['id' => $processo->id, 'action' => 'criterios']) }}"
                                       class="btn btn-primary btn-raised legitRipple"><i
                                                class="icon-folder-open" data-popup="tooltip"
                                                title="Critérios"></i>
                                    </a>
                                    @permission('ver-matriz')
                                    <a href="{{ route('processo.show',['id' => $processo->id, 'action' => 'matriz']) }}"
                                       class="btn btn-primary btn-raised legitRipple"><i
                                                class="icon-tree7" data-popup="tooltip"
                                                title="Matriz de Planejamento"></i>
                                    </a>
                                    @endpermission
                                    <a href="{{ route('processo.index') }}"
                                       class="btn btn-primary btn-raised legitRipple"><i
                                                class="icon-reply-all" data-popup="tooltip"
                                                title="Voltar Tela Anterior"></i>
                                    </a>
                                </div>
                            </div>
                            <br>
                            @switch(Request::get('action'))
                                @case('criterios')
                                @include('processo.auditoria._contents.criterios',$processo)
                                @break
                                @case('matriz')
                                @include('processo.auditoria._contents.matriz',$processo)
                                @break
                                @default
                                @include('processo.auditoria._contents.dados',$processo)
                                @break
                            @endswitch
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop