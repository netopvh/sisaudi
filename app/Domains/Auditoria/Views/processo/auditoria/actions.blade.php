@permission('ver-processos-auditaveis')
<a href="{{ route('processo.show',['id' => $processo->id]) }}" title="Ver Processo" class="btn btn-xs btn-primary">
    <i class="icon-folder-search"></i>
</a>
@endpermission