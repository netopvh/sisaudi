@extends('layouts.app')

@section('page-header')
    <div class="page-header page-header-default page-header-sm"
         style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Questões de Auditoria</span></h5>
            </div>
        </div>
        {{ Breadcrumbs::render('processo.questao.create') }}
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default border-grey">
                <div class="panel-heading">
                    <i class="icon-table"></i> <span class="text-bold">Formulário de Alteração</span>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse" class=""></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="processoQuestaoDv">
                        <form action="{{ route('processo.matriz.store',['processo' => $matriz->id]) }}" method="POST"
                              id="processoMatrizQuestao" class="form-validate">
                            {{ csrf_field() }}
                            <fieldset>
                                <legend>Itens iniciais</legend>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-bold display-block">Possui Subquestão?</label>
                                            <label class="checkbox-inline">
                                                Sim <input type="radio" class="styled" name="subquestao" value="S" {{ $matriz->subquestao=='S'?'checked':'' }} required>
                                            </label>
                                            <label class="checkbox-inline">
                                                Não <input type="radio" class="styled" name="subquestao" value="N" {{ $matriz->subquestao=='N'?'checked':'' }} required>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-bold">Questão / Pergunta:</label>
                                            <textarea name="questao" id="procMatrizQuestao" class="form-control" rows="3"
                                                      required>{{ $matriz->questao }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <div id="matrizItens" class="collapse">
                                <fieldset>
                                    <legend>Informações</legend>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text-bold">Risco:</label>
                                                <textarea name="risco" class="form-control" rows="3" required>{{ $matriz->risco }}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text-bold">Procedimento:</label>
                                                <textarea name="procedimento" class="form-control" rows="3"
                                                          required>{{ $matriz->procedimento }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text-bold">Critério:</label>
                                                <textarea name="criterio" class="form-control" rows="3" required>{{ $matriz->criterio }}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text-bold">Informações Requeridas:</label>
                                                <textarea name="informacoes_requeridas" class="form-control" rows="3"
                                                          required>{{ $matriz->informacoes_requeridas }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text-bold">Fonte de Informação:</label>
                                                <textarea name="fonte_informacao" class="form-control" rows="3"
                                                          required>{{ $matriz->fonte_informacao }}</textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="text-bold">Limitações:</label>
                                                <textarea name="limitacoes" class="form-control" rows="3"
                                                          required>{{ $matriz->limitacoes }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="text-bold">Possíveis Achados:</label>
                                                <textarea name="possiveis_achados" class="form-control" rows="3"
                                                          required>{{ $matriz->possiveis_achados }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ route('processo.show',['id' => $matriz->processo_id, 'action' => 'matriz']) }}" class="btn btn-info">
                                        <i class="icon-reply"></i>
                                        Voltar
                                    </a>
                                    <button type="submit" class="btn btn-primary">
                                        <i class="icon-database-insert"></i>
                                        Gravar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop