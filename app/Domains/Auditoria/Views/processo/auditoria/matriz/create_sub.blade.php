@extends('layouts.app')

@section('page-header')
    <div class="page-header page-header-default page-header-sm"
         style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Questões de Auditoria</span></h5>
            </div>
        </div>
        {{ Breadcrumbs::render('processo.questao.create') }}
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default border-grey">
                <div class="panel-heading">
                    <i class="icon-table"></i> <span class="text-bold">Formulário de Cadastro</span>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse" class=""></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <div id="processoQuestaoDv">
                        <form action="{{ route('processo.matriz.store',['processo' => $processo->id]) }}" method="POST"
                              id="processoMatrizQuestao" class="form-validate">
                            {{ csrf_field() }}
                            <input type="hidden" name="processo_id" value="{{ $processo->id }}">
                            <input type="hidden" name="questao_id" value="{{ $question->id }}">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="text-bold display-block">Questão / Pergunta Principal:</label>
                                        <input type="text" value="{{ $question->questao }}" class="form-control" disabled>
                                    </div>
                                </div>
                            </div>
                            <fieldset>
                                <legend>Informações</legend>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-bold">Subquestao / Pergunta:</label>
                                            <textarea name="questao" id="procMatrizQuestao" class="form-control" rows="3"
                                                      required autofocus></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-bold">Risco:</label>
                                            <textarea name="risco" class="form-control" rows="3" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-bold">Procedimento:</label>
                                            <textarea name="procedimento" class="form-control" rows="3"
                                                      required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-bold">Critério:</label>
                                            <textarea name="criterio" class="form-control" rows="3" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-bold">Fonte de Informação:</label>
                                            <textarea name="fonte_informacao" class="form-control" rows="3"
                                                      required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-bold">Limitações:</label>
                                            <textarea name="limitacoes" class="form-control" rows="3"
                                                      required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-bold">Possíveis Achados:</label>
                                            <textarea name="possiveis_achados" class="form-control" rows="3"
                                                      required></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-bold">Informações Requeridas:</label>
                                            <textarea name="informacoes_requeridas" class="form-control" rows="3"
                                                      required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ route('processo.show',['id' => $processo->id, 'action' => 'matriz']) }}" class="btn btn-info">
                                        <i class="icon-reply"></i>
                                        Voltar
                                    </a>
                                    <button type="submit" class="btn btn-primary"><i class="icon-database-insert"></i>
                                        Gravar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop