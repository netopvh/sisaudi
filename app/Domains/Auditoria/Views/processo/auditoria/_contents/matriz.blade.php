<fieldset>
    <legend>Dados Cadastrais</legend>
    <div class="row">
        <div class="col-md-2">
            <label class="text-bold display-block">Número do Processo:</label>
            <span>{{ $processo->numero_processo }}</span>
        </div>
        <div class="col-md-10">
            <label class="text-bold display-block">Assunto/Objeto:</label>
            <span>{{ $processo->assunto }}</span>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-2">
            <label class="text-bold display-block">Status:</label>
            <span>{{ $status[$processo->status] }}</span>
        </div>
        <div class="col-md-4">
            <label class="text-bold display-block">Elaborado por:</label>
            <span>{{ $processo->criador->nome }}</span>
        </div>
    </div>
    <br><br>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group pull-right">
                <a href="{{ route('processo.matriz.create',['processo' => $processo->id]) }}"
                   class="btn-sm btn-primary"><i class="icon-database-add"></i> Cadastrar Questão</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-condensed table-bordered table-striped" id="tblMatrizPlan" data-table="tblMatriz">
                <thead>
                <tr>
                    <th width="80">#</th>
                    <th width="13%">Subquestões</th>
                    <th width="17%">Procedimentos</th>
                    <th width="12%">Critério</th>
                    <th width="12%">Informações Requeridas</th>
                    <th>Fontes de Informação</th>
                    <th width="10%">Possíveis Limitações</th>
                    <th>Possíveis Achados</th>
                    <th width="126">Ações</th>
                </tr>
                </thead>
                <tbody>
                @foreach($processo->questoes as $questao)
                    <tr data-id="{{ $questao->id }}" {{ $questao->subquestao == 'N'?'data-type=q':'data-type=s' }}>
                        <td colspan="2" class="text-bold bg-grey-300">Questão de Auditoria:</td>
                        <td colspan="6" class="text-bold bg-grey-300">{{ $questao->questao }}</td>
                        <td class="bg-grey-300">
                            @if($questao->subquestao == 'S')
                                <a href="{{ route('processo.matriz.create.sub',['processo'=>$processo->id, 'questao'=>$questao->id]) }}"
                                   class="btn-xs btn-link" data-popup="tooltip" title="Cadastrar Subquestão"><i
                                            class="icon-plus-circle2"></i></a>
                            @endif
                            @permission('editar-matriz')
                            <a href="{{ route('processo.matriz.edit',['processo'=>$processo->id,'questao' => $questao->id]) }}" class="btn-xs btn-link" data-popup="tooltip" title="Editar"><i
                                        class="icon-pencil"></i></a>
                            @endpermission
                            @permission('remover-matriz')
                            <button class="btn-xs btn-link" id="deleteMatrizQuestao" data-popup="tooltip" title="Remover"><i
                                        class="icon-trash"></i></button>
                            @endpermission
                        </td>
                    </tr>
                    @if($questao->subquestao == 'S')
                        @foreach($questao->subquestoes as $subquestao)
                            @if(empty($subquestao))
                                <tr>
                                    <td colspan="9" class="text-center">Sem Subquestões Cadastradas</td>
                                </tr>
                            @else
                                <tr data-id="{{ $subquestao->id }}">
                                    @if($loop->first == 1)
                                        <td width="40" rowspan="{{ $loop->count }}"> QA001</td>
                                    @endif
                                    <td style="vertical-align: top"
                                        class="text-justify">{{ $subquestao->questao }}</td>
                                    <td style="vertical-align: top"
                                        class="text-justify">{{ $subquestao->procedimento }}</td>
                                    <td style="vertical-align: top">{{ $subquestao->criterio }}</td>
                                    <td style="vertical-align: top">{{ $subquestao->informacoes_requeridas }}</td>
                                    <td style="vertical-align: top">{{ $subquestao->fonte_informacao }}</td>
                                    <td style="vertical-align: top">{{ $subquestao->limitacoes }}</td>
                                    <td style="vertical-align: top">{{ $subquestao->informacoes_requeridas }}</td>
                                    <td>
                                        @permission('editar-matriz')
                                        <a href="{{ route('processo.matriz.edit',['processo'=>$processo->id,'questao' => $subquestao->id]) }}" class="btn-xs btn-link" data-popup="tooltip" title="Editar"><i
                                                    class="icon-pencil"></i></a>
                                        @endpermission
                                        @permission('remover-matriz')
                                        <button class="btn-xs btn-link" id="deleteMatrizSubQuestao" data-popup="tooltip" title="Remover"><i
                                                    class="icon-trash"></i></button>
                                        @endpermission
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    @elseif($questao->subquestao == 'N')
                        <tr>
                            <td width="40" colspan="2"> QA</td>
                            <td style="vertical-align: top">{{ $questao->procedimento }}</td>
                            <td style="vertical-align: top">{{ $questao->criterio }}</td>
                            <td style="vertical-align: top">{{ $questao->informacoes_requeridas }}</td>
                            <td style="vertical-align: top">{{ $questao->fonte_informacao }}</td>
                            <td style="vertical-align: top">{{ $questao->limitacoes }}</td>
                            <td colspan="2">{{ $questao->informacoes_requeridas }}</td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</fieldset>