<div class="row">
    <div class="col-md-12">
        <ul class="easyui-datalist" title="Critérios" lines="true"
            style="width:100%;height:300px">
            @foreach(str_to_array($processo->fields->where('name','lista_docs_obtida')->first()->content,'li') as $fonte)
                <li>{{ $fonte }}</li>
            @endforeach
        </ul>
    </div>
</div>