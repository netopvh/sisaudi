<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;" id="or"><b>> 2.1 Realização Informada Comparada com Metas</b></div>
@if(isset($processo) && !is_null($processo))
    {{ $processo->fields->compara_metas }}
@else
    <p></p>
@endif
<hr/>
<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;" id="obj"><b>> 2.2 Conclusões Sobre a Execução Informada Pelo Gestor</b></div>
@if(isset($processo) && !is_null($processo))
    {{ $processo->fields->conc_exec_gestor }}
@else
    <p></p>
@endif
<hr/>
<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;" id="msl"><b>> 2.3 Resumo dos Principais Problemas/Desvios</b></div>
@if(isset($processo) && !is_null($processo))
    {{ $processo->fields->resumo_prin_problemas }}
@else
    <p></p>
@endif
<hr/>
<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;" id="msg"><b>> 2.4 Providências do Controle – Repercussões</b></div>
@if(isset($processo) && !is_null($processo))
    {{ $processo->fields->prov_controle }}
@else
    <p></p>
@endif
<hr/>
<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;" id="ldo"><b>> 2.5 Outras Questões Relevantes</b>
</div>
@if(isset($processo) && !is_null($processo))
    {{ $processo->fields->questoes_relevantes }}
@else
    <p></p>
@endif