<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;" id="or"><b>> 1.1 Órgãos Responsáveis</b></div>
@if(isset($processo) && !is_null($processo))
    {{ $processo->fields->org_resp }}
@else
    <p></p>
@endif
<hr/>
<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;" id="obj"><b>> 1.2 Objetivos</b></div>
@if(isset($processo) && !is_null($processo))
    {{ $processo->fields->objetivos }}
@else
    <p></p>
@endif
<hr/>
<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;" id="msl"><b>> 1.3 Metas Segundo a LOA</b></div>
@if(isset($processo) && !is_null($processo))
    {{ $processo->fields->metas_loa }}
@else
    <p></p>
@endif
<hr/>
<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;" id="msg"><b>> 1.4 Metas Segundo o Gestor</b></div>
@if(isset($processo) && !is_null($processo))
    {{ $processo->fields->metas_gestor }}
@else
    <p></p>
@endif
<hr/>
<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;" id="ldo"><b>> 1.5 Listagem da Documentação Obtida</b>
</div>
@if(isset($processo) && !is_null($processo))
    {{ $processo->fields->lista_docs_obtida }}
@else
    <ul>
        <li></li>
    </ul>
@endif
<hr/>
<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;" id="rri"><b>> 1.6 Resumo das Razões da
        Implantação</b></div>
@if(isset($processo) && !is_null($processo))
    {{ $processo->fields->razoes_implantacao }}
@else
    <p></p>
@endif
<hr/>
<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;" id="dfnp"><b>> 1.7 Divisões, Formas, Normas e
        Procedimentos</b></div>
@if(isset($processo) && !is_null($processo))
    {{ $processo->fields->divisoes_procedimentos }}
@else
    <p></p>
@endif