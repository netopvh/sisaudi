<fieldset>
    <legend>Dados Cadastrais</legend>
    <div class="row">
        <div class="col-md-2">
            <label class="text-bold display-block">Número do Processo:</label>
            <span>{{ $processo->numero_processo }}</span>
        </div>
        <div class="col-md-10">
            <label class="text-bold display-block">Assunto/Objeto:</label>
            <span>{{ $processo->assunto }}</span>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-2">
            <label class="text-bold display-block">Status:</label>
            <span>{{ $status[$processo->status] }}</span>
        </div>
        <div class="col-md-4">
            <label class="text-bold display-block">Elaborado por:</label>
            <span>{{ $processo->criador->nome }}</span>
        </div>
    </div>
    <br>
</fieldset>