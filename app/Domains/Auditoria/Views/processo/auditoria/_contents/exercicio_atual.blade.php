<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;" id="or"><b>> 3.1 Recursos Inicialmente Solicitados Pelo Gestor</b></div>
@if(isset($processo) && !is_null($processo))
    {{ $processo->fields->recursos_ini_gestor }}
@else
    <p></p>
@endif
<hr/>
<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;" id="obj"><b>> 3.2 Execução Planejada – Físico, Financeiro e Cronograma</b></div>
@if(isset($processo) && !is_null($processo))
    {{ $processo->fields->exec_plan }}
@else
    <p></p>
@endif
<hr/>
<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;" id="msl"><b>> 3.3 Avaliação Sumária Sobre o Planejamento</b></div>
@if(isset($processo) && !is_null($processo))
    {{ $processo->fields->avaliacao_sum_plan }}
@else
    <p></p>
@endif
<hr/>
<div style="background:#eee;border:1px solid #ccc;padding:5px 10px;" id="msg"><b>> 3.4 Aspectos Operacionais Relevantes</b></div>
@if(isset($processo) && !is_null($processo))
    {{ $processo->fields->aspect_op_rel }}
@else
    <p></p>
@endif