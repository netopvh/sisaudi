@extends('layouts.app')

@section('page-header')
    <div class="page-header page-header-default page-header-sm"
         style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Riscos</span></h5>
            </div>
        </div>
        {{ Breadcrumbs::render('risco.index') }}
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default border-grey">
                <div class="panel-heading">
                    @permission('criar-usuario')
                    <button id="newRisk" class="btn btn-sm btn-primary legitRipple">
                        <i class="icon-database-add"></i>
                        <span>Novo Registro</span>
                    </button>
                    @endpermission
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse" class=""></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <table class="table table-bordered table-condensed table-hover" id="tblRisk">
                    <thead>
                    <tr>
                        <th>Descrição</th>
                        <th>Impacto</th>
                        <th>Mitigação</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div id="dlgRisk">
        <form method="post" id="frmRisk">
            {{ csrf_field() }}
            <input type="hidden" name="item" id="id">
            <input type="hidden" name="action">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Descrição:</label>
                        <label class="label-full">
                            <textarea name="descricao" id="descricao" cols="30" rows="4"
                                      class="form-control text-uppercase"></textarea>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Impacto:</label>
                        <label class="label-full">
                            <textarea name="impacto" id="impacto" cols="30" rows="4"
                                      class="form-control text-uppercase"></textarea>
                        </label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Mitigação:</label>
                        <label class="label-full">
                            <textarea name="mitigacao" id="mitigacao" cols="30" rows="4"
                                      class="form-control text-uppercase"></textarea>
                        </label>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop
