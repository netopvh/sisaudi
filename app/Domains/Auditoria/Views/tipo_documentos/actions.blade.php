@permission('editar-tipo-documentos')
<button id="editReg" title="Editar" class="btn btn-xs btn-primary">
    <i class="icon-pencil7"></i>
</button>
@endpermission
@permission('remover-tipo-documentos')
<button id="deleteReg" title="Deletar" class="btn btn-xs btn-danger">
    <i class="icon-trash"></i>
</button>
@endpermission