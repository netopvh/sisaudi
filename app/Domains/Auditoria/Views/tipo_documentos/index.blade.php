@extends('layouts.app')

@section('page-header')
    <div class="page-header page-header-default page-header-sm" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Tipos de Documentos</span></h5>
            </div>
        </div>
        {{ Breadcrumbs::render('tipo_documentos.index') }}
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-5">
            <div class="panel panel-default border-grey">
                <div class="panel-heading">
                    @permission('criar-tipo-documentos')
                    <button id="newDocType" class="btn btn-sm btn-primary legitRipple">
                        <i class="icon-database-add"></i>
                        <span>Novo Registro</span>
                    </button>
                    @endpermission
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse" class=""></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <table class="table table-bordered table-condensed table-hover" id="tblDocTypes">
                    <thead>
                    <tr>
                        <th>Descrição</th>
                        <th>Prazo</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div id="dlgDocTypes">
        <form method="post" id="frmDocTypes">
            {{ csrf_field() }}
            <input type="hidden" name="item" id="id">
            <input type="hidden" name="action">
            <div class="form-group">
                <label>Descrição:</label>
                <input type="text" name="descricao" id="descricao" class="form-control text-uppercase" required>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Prazo:</label>
                        <select name="prazo" id="prazo" class="form-control" required>
                            <option value=""></option>
                            <option value="S">SIM</option>
                            <option value="N">NAO</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>
    </div>
@stop
