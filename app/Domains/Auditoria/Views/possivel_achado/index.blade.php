@extends('layouts.app')

@section('page-header')
    <div class="page-header page-header-default page-header-sm" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Possíveis Achados</span></h5>
            </div>
        </div>
        {{ Breadcrumbs::render('possivel_achado.index') }}
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-7">
            <div class="panel panel-default border-grey">
                <div class="panel-heading">
                    @permission('criar-possiveis-achados')
                    <button id="newPosFindings" class="btn btn-sm btn-primary legitRipple">
                        <i class="icon-database-add"></i>
                        <span>Novo Registro</span>
                    </button>
                    @endpermission
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse" class=""></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <table class="table table-bordered table-condensed table-hover" id="tblPosFindings">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Status</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div id="dlgPosFindings">
        <form method="post" id="frmPosFindings">
            {{ csrf_field() }}
            <input type="hidden" name="item" id="id">
            <input type="hidden" name="action">
            <div class="form-group">
                <label>Nome:</label>
                <input type="text" name="nome" id="nome" class="form-control text-uppercase" required>
            </div>
            <div class="form-group row">
                <div class="col-md-5">
                    <label class="display-block">Status:</label>
                    <select name="status" id="status" required class="form-control">
                        <option value=""></option>
                        <option value="A">ATIVO</option>
                        <option value="I">INATIVO</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
@stop
