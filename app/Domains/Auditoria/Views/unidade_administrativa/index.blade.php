@extends('layouts.app')

@section('page-header')
    <div class="page-header page-header-default page-header-sm" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Unidades Administrativas</span></h5>
            </div>
        </div>
        {{ Breadcrumbs::render('administrative_units.index') }}
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-7">
            <div class="panel panel-default border-grey">
                <div class="panel-heading">
                    @permission('criar-unidades')
                    <button id="newAdministrativeUnits" class="btn btn-sm btn-primary legitRipple">
                        <i class="icon-database-add"></i>
                        <span>Novo Registro</span>
                    </button>
                    @endpermission
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse" class=""></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <table class="table table-bordered table-condensed table-hover" id="tblAdministrativeUnits">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Sigla</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div id="dlgAdministrativeUnits">
        <form method="post" id="frmAdministrativeUnits">
            {{ csrf_field() }}
            <input type="hidden" name="item" id="id">
            <input type="hidden" name="action">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label>Nome: *</label>
                        <input type="text" name="nome" id="nome" class="form-control text-uppercase" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Sigla: *</label>
                        <input type="text" name="sigla" id="sigla" class="form-control text-uppercase" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="display-block">Administração Direta: *</label>
                        <select name="adm_direta" id="adm_direta" class="form-control" required="">
                            <option value=""></option>
                            <option value="1">SIM</option>
                            <option value="0">NÃO</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-7">
                    <label>Secretário / Responsável</label>
                    <input type="text" class="form-control text-uppercase" name="secretario" id="secretario">
                </div>
            </div>
        </form>
    </div>
@stop
