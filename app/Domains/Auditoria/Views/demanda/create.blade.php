@extends('layouts.app')

@section('page-header')
    <div class="page-header page-header-default page-header-sm"
         style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Cadastro de Demanda</span></h5>
            </div>
        </div>
        {{ Breadcrumbs::render('demanda.create') }}
    </div>
@stop

@section('content')
    <div class="row">
        <div class=" col-md-offset-1 col-md-10">
            <div class="panel panel-default border-grey">
                <div class="panel-heading">
                    <i class="icon-table"></i> <span class="text-bold">Formulário de Cadastro</span>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <form action="{{ route('demanda.store') }}" method="post" class="form-validate" autocomplete="off">
                        {{ csrf_field() }}
                        <input type="hidden" name="departamento_id" value="{{ auth()->user()->departamento_id }}">
                        <div id="tabDemanda" class="" style="width:100%;height:auto">
                            <!--  PRIMEIRO PANEL -->
                            <div title="Informações da Demanda" id="tbGerais" style="padding:10px">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="" class="text-bold">Autuação: <span class="text-danger">*</span></label>
                                            <input type="text" name="autuacao" class="form-control datepicker" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-bold">Tipo de Demanda: <span class="text-danger">*</span></label>
                                            <select name="tipo_processo"
                                                    class="form-control" required>
                                                <option value="">Selecione o Tipo de Demanda</option>
                                                @foreach($demandaTipos as $key => $value)
                                                    <option value="{{ $key }}" {{ request()->has('type') && request()->get('type') == $key ? 'selected' : old('tipo_processo')==$key?'selected':'' }}>{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="" class="text-bold display-block">Procedência:</label>
                                            <label class="radio-inline">
                                                <input type="radio" class="styled" name="procedencia" value="I"
                                                       > Interno
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" class="styled" name="procedencia" value="E"
                                                       > Externo
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row collapse" id="DemandaOrgProc">
                                    <div class="col-md-11">
                                        <div class="form-group">
                                            <label for="" class="text-bold">Órgão de procedência:</label>
                                            <select name="orgao_id" class="select-search">
                                                <option value="">SELECIONE</option>
                                                @foreach($unidadesAdministrativas as $unidadeAdministrativa)
                                                    <option value="{{ $unidadeAdministrativa->id }}" {{ !empty(old('orgao_id')) ? in_array($unidadeAdministrativa->id,old('auditados'))? 'selected':'':'' }}>{{ $unidadeAdministrativa->nome }}
                                                        - {{ $unidadeAdministrativa->sigla }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="form-group">
                                            <label class="text-bold">Assunto:</label>
                                            <input type="text" class="form-control text-uppercase" name="assunto"
                                                   required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="form-group">
                                            <label class="text-bold">Interessado: <span
                                                        class="text-danger"> *</span></label>
                                            <label class="label-full">
                                                <select name="interessados[]" id="selProcessoUniAdm" multiple required>
                                                    @foreach($unidadesAdministrativas as $unidadeAdministrativa)
                                                        <option value="{{ $unidadeAdministrativa->id }}" {{ !empty(old('interessados')) ? in_array($unidadeAdministrativa->id,old('interessados'))? 'selected':'':'' }}>{{ $unidadeAdministrativa->nome }}
                                                            - {{ $unidadeAdministrativa->sigla }}</option>
                                                    @endforeach
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="{{ route('processo.index') }}"
                                   class="btn btn-info legitRipple"><i
                                            class="icon-database-arrow"></i> Retornar</a>
                                <button type="submit" id="btnProcesso" class="btn btn-primary legitRipple">
                                    <i
                                            class="icon-database-insert"></i> Gravar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop