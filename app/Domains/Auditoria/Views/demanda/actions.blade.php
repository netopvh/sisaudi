@permission('ver-processos-auditaveis')
<a href="{{ route('demanda.show',['id' => $demanda->id]) }}" title="Ver Demanda" class="btn btn-xs btn-primary">
    <i class="icon-folder-search"></i>
</a>
@endpermission