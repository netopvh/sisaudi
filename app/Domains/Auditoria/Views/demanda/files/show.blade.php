@extends('layouts.app')

@section('page-header')
    <div class="page-header page-header-default page-header-sm"
         style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Cadastro de Documentos</span></h5>
            </div>
        </div>
        {{ Breadcrumbs::render('demanda.files.create') }}
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-grey">
                <div class="panel-heading">
                    <h6 class="panel-title">Visualizar Arquivo</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <a href="{{ url()->previous() }}" class="btn btn-primary btn-raised"><i
                                            class="icon-database-arrow"></i>
                                    Retornar</a>
                                @if(is_null($arquivo->path))
                                    <button id="printable" class="btn btn-primary btn-raised"><i
                                                class="icon-printer2"></i>
                                        Imprimir
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if(is_null($arquivo->path))
                                <div id="print">
                                    <div class="page">
                                        <div id="print-content">
                                            {!! $arquivo->conteudo !!}
                                        </div>
                                    </div>
                                </div>
                            @else
                                @if(config('app.env') == 'local')
                                    <embed src="/storage/{{ $arquivo->path }}" width="100%" height="600"
                                           type='application/pdf'>
                                @else
                                    <embed src="/storage/{{ $arquivo->path }}" width="100%" height="600"
                                           type='application/pdf'>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop