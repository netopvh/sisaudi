@extends('layouts.app')

@section('page-header')
    <div class="page-header page-header-default page-header-sm"
         style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Cadastro de Documentos</span></h5>
            </div>
        </div>
        {{ Breadcrumbs::render('demanda.files.create') }}
    </div>
@stop

@section('content')
    <div class="row">
        <div class=" col-md-offset-1 col-md-10">
            <div class="panel panel-default border-grey">
                <div class="panel-heading">
                    <i class="icon-table"></i> <span class="text-bold">Formulário de Cadastro</span>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <form action="{{ route('demanda.files.store',['id'=> $demanda->id]) }}" method="post"
                          class="form-validate" autocomplete="off" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="" class="text-bold">Tipo de Documento:</label>
                                    <select name="tipo_doc_id" class="select" required>
                                        <option value="">Selecione</option>
                                        @foreach($tipoDocs as $tipoDoc)
                                            <option value="{{ $tipoDoc->id }}" {{ old('tipo_doc_id')==$tipoDoc->id?'selected':'' }}>{{ $tipoDoc->descricao }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="" class="text-bold">Formato do Documento:</label>
                                    <select name="tipo_file" id="tipo_file" class="select" required>
                                        <option value="">Selecione</option>
                                        <option value="A" {{ old('tipo_file')=='A'?'selected':'' }}>Anexo</option>
                                        <option value="T" {{ old('tipo_file')=='T'?'selected':'' }}>Texto</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="" class="text-bold">Prazo:</label> <i class="icon-info22"
                                                                                      data-popup="popover"
                                                                                      data-trigger="hover"
                                                                                      data-content="Campo opcional, caso fique em branco será atribuido o prazo padrão de 15 dias"
                                                                                      data-placement="bottom"
                                                                                      data-original-title="Informação"></i>
                                    <input type="text" class="form-control" name="prazo">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-group">
                                    <label for="" class="text-bold">Descrição:</label>
                                    <input type="text" value="{{ old('descricao') }}" name="descricao"
                                           class="form-control text-uppercase" required>
                                </div>
                            </div>
                        </div>
                        <div class="row collapse" id="dvDemandaFile">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="text-bold display-block">Anexo:</label>
                                    <input type="file" name="path" class="file-styled">
                                </div>
                            </div>
                        </div>
                        <div class="row collapse" id="dvDemandaTexto">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="" class="text-bold display-block">Conteúdo:</label>
                                    <textarea name="conteudo" id="DemandaFileTexto"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="{{ route('demanda.show',['id' => $demanda->id]) }}"
                                   class="btn btn-info legitRipple"><i
                                            class="icon-database-arrow"></i> Retornar</a>
                                <button type="submit" id="btnProcesso" class="btn btn-primary legitRipple">
                                    <i
                                            class="icon-database-insert"></i> Gravar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop