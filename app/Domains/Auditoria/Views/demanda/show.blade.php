@extends('layouts.app')

@section('page-header')
    <div class="page-header page-header-default page-header-sm"
         style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Demanda: {{ $demanda->id }}</span></h5>
            </div>
        </div>
        {{ Breadcrumbs::render('demanda.show') }}
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default border-grey">
                <div class="panel-heading">
                    <h6 class="panel-title">Demanda Nº {{ $demanda->assunto }}</h6>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="col-md-12">
                                <div class="tree-files well-sm border-left-info border-left-lg">
                                    <ul>
                                        <li class="folder expanded">{{ $demanda->assunto }}
                                            <ul>
                                                @foreach($demanda->files as $file)
                                                    @if(is_null($file->path))
                                                        <li>
                                                            <a href="{{ route('demanda.files.show',['id'=>$demanda->id,'file'=>$file->id]) }}">{{ $file->tipo->descricao }}
                                                                {{ !is_null($file->descricao)?' ('. $file->descricao .')':'' }}</a>
                                                        </li>
                                                    @else
                                                        <li>
                                                            <a href="{{ route('demanda.files.show',['id'=>$demanda->id,'file'=>$file->id]) }}">{{ $file->tipo->descricao }}
                                                                {{ !is_null($file->descricao)?' ('. $file->descricao .')':'' }}</a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="text-bold">Ações:</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="{{ route('demanda.index') }}"
                                       class="btn btn-primary btn-raised legitRipple"><i
                                                class="icon-reply-all" data-popup="tooltip"
                                                title="Voltar Tela Anterior"></i>
                                    </a>
                                    <a href="#"
                                       class="btn btn-primary btn-raised legitRipple"><i
                                                class="icon-mail-read" data-popup="tooltip" title="Movimentar"></i>
                                    </a>
                                    <a href="#"
                                       class="btn btn-primary btn-raised legitRipple"><i
                                                class="icon-typewriter" data-popup="tooltip"
                                                title="Atualizar Andamento"></i>
                                    </a>
                                    <a href="{{ route('demanda.files.create',['id' => $demanda->id]) }}"
                                       class="btn btn-primary btn-raised legitRipple"><i
                                                class="icon-file-plus" data-popup="tooltip"
                                                title="Incluir Documentos"></i>
                                    </a>
                                    <a href="#"
                                       class="btn btn-primary btn-raised legitRipple"><i
                                                class="icon-cabinet" data-popup="tooltip"
                                                title="Transformar em Processo"></i>
                                    </a>
                                    <a href="{{ route('demanda.show',['id'=>$demanda->id,'view'=>'moviments']) }}"
                                       class="btn btn-primary btn-raised legitRipple"><i
                                                class="icon-move" data-popup="tooltip"
                                                title="Visualizar Tramitação"></i>
                                    </a>
                                    <a href="#"
                                       class="btn btn-primary btn-raised legitRipple"><i
                                                class="icon-folder-check" data-popup="tooltip"
                                                title="Arquivar"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(request()->has('view') && request()->get('view') == 'moviments')
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <table class="table table-condensed table-bordered">
                                    <thead>
                                    <tr>
                                        <th width="150">Data</th>
                                        <th>Ação</th>
                                        <th width="200">Usuário</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($demanda->tramitacoes as $tramite)
                                        <tr>
                                            <td>{{ date_to_format($tramite->data) }}</td>
                                            <td>{{ $tramite->descricao }}</td>
                                            <td>{{ $tramite->user->nome }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop