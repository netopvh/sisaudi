@extends('layouts.app')

@section('page-header')
    <div class="page-header page-header-default page-header-sm"
         style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Minhas Demandas</span></h5>
            </div>
        </div>
        {{ Breadcrumbs::render('demanda.index') }}
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default border-grey">
                <div class="panel-heading">
                    @permission('criar-processos-auditaveis')
                    <a href="{{ route('demanda.create') }}" class="btn btn-sm btn-primary legitRipple">
                        <i class="icon-database-add"></i>
                        <span>Novo Registro</span>
                    </a>
                    @endpermission
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse" class=""></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <form method="POST" id="demanda-search" role="form" autocomplete="off">
                    <fieldset>
                        <legend>Filtro:</legend>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="" class="text-bold">Assunto:</label>
                                    <input type="text" class="form-control upper" id="demandaAssunto">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="" class="text-bold">Data de Autuação:</label>
                                    <input type="text" class="form-control datepicker" id="demandaAutuacao">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="" class="text-bold">Criado por:</label>
                                    <select id="demandaCriador" class="select-search">
                                        <option value=""></option>
                                        @foreach($usuarios as $usuario)
                                            <option value="{{ $usuario->id }}">{{ $usuario->nome }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Pesquisar</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
                <table class="table table-bordered table-condensed table-hover" id="tblProcessInternal">
                    <thead>
                    <tr>
                        <th>Data da Autuação</th>
                        <th>Assunto</th>
                        <th>Interessados</th>
                        <th>Criado por</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop
