@extends('layouts.app')

@section('page-header')
    <div class="page-header page-header-default page-header-sm"
         style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Auditoria</span></h5>
            </div>
        </div>
        {{ Breadcrumbs::render('processo.create') }}
    </div>
@stop

@section('content')
    <div class="row">
        <div class=" col-md-offset-1 col-md-10">
            <div class="panel panel-default border-grey">
                <div class="panel-heading">
                    <i class="icon-table"></i> <span class="text-bold">Formulário de Cadastro</span>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <form action="{{ route('processo.store') }}" method="post" class="form-validate"
                          id="frmProcessoCreate"
                          autocomplete="off">
                        {{ csrf_field() }}
                        <div id="dvAuditoriaCreate">
                            <fieldset>
                                <legend>Dados Gerais</legend>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="text-bold">Exercício: <span
                                                        class="text-danger"> *</span></label>
                                            <input type="text" class="form-control" name="exercicio"
                                                   value="{{ old('exercicio') }}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-bold">Processo Auditável: <span
                                                        class="text-danger"> *</span></label>
                                            <select name="processo_id" class="select-search" id="selAuditoriaProcesso"
                                                    required>
                                                <option value=""></option>
                                                @foreach($processos as $processo)
                                                    <option value="{{ $processo->id }}">{{ $processo->numero_processo }} - {{ $processo->assunto }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-bold">Descrição:</label>
                                            <label class="label-full">
                                                <textarea name="descricao" rows="5"
                                                          id="auditoriaDescricaoCreate" class="form-control"></textarea>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-bold display-block">Acesso Restrito:</label>
                                            <label class="radio-inline">
                                                <input type="radio" name="restrito" value="S"> SIM
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="restrito" value="N"> NÃO
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Informações para o Planejamento</legend>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-bold">Objetivo: <span
                                                        class="text-danger"> *</span></label>
                                            <label class="label-full">
                                                <textarea name="objetivo" rows="5"
                                                          id="auditoriaObjetivoCreate" class="form-control" required></textarea>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-bold">Escopo: <span
                                                        class="text-danger"> *</span></label>
                                            <label class="label-full">
                                                <textarea name="escopo" rows="5" class="form-control" id="auditoriaEscopoCreate"
                                                          required></textarea>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Cronograma</legend>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="text-bold">Início Proposto: <span
                                                        class="text-danger"> *</span></label>
                                            <input type="text" name="inicio_proposto" required class="form-control datepicker">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="text-bold">Fim Proposto: <span
                                                        class="text-danger"> *</span></label>
                                            <input type="text" name="fim_proposto" required class="form-control datepicker">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Portaria</legend>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="text-bold">Número da portaria: <span
                                                        class="text-danger"> *</span></label>
                                            <input type="text" name="portaria_num" required class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label class="text-bold">Data da portaria: <span
                                                        class="text-danger"> *</span></label>
                                            <input type="text" name="portaria_data" required class="form-control datepicker">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <br>
                            <fieldset>
                                <legend>Unidades Envolvidas</legend>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-bold">Unidades a Serem Auditadas: <span
                                                        class="text-danger"> *</span></label>
                                            <label class="label-full">
                                                <select name="auditados[]" id="selProcessoUniAdm" multiple required>
                                                    @foreach($unidadesAdministrativas as $unidadeAdministrativa)
                                                        <option value="{{ $unidadeAdministrativa->id }}" {{ !empty(old('auditados')) ? in_array($unidadeAdministrativa->id,old('auditados'))? 'selected':'':'' }}>{{ $unidadeAdministrativa->nome }}
                                                            - {{ $unidadeAdministrativa->sigla }}</option>
                                                    @endforeach
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="text-bold">Departamento Executor:</label>
                                            <select name="departamento_id" id="auditoriaDepartamentoCreate" class="select-search" required>
                                                <option value=""></option>
                                                @foreach($departamentos as $departamento)
                                                    <option value="{{ $departamento->id }}">{{ $departamento->descricao }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <a href="{{ route('processo.index') }}"
                                       class="btn btn-info legitRipple"><i
                                                class="icon-database-arrow"></i> Retornar</a>
                                    <button type="submit" id="btnProcesso" class="btn btn-primary legitRipple"><i
                                                class="icon-database-insert"></i> Gravar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--
    <div id="dlgAdministrativeUnitsProcess">
        <form method="post" id="frmAdministrativeUnits">
            {{ csrf_field() }}
            <input type="hidden" name="item" id="id">
            <input type="hidden" name="action">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label>Nome: *</label>
                        <input type="text" name="nome" id="nome" class="form-control text-uppercase" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Sigla: *</label>
                        <input type="text" name="sigla" id="sigla" class="form-control text-uppercase" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="display-block">Administração Direta: *</label>
                        <select name="adm_direta" id="adm_direta" class="form-control" required="">
                            <option value=""></option>
                            <option value="1">SIM</option>
                            <option value="0">NÃO</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-7">
                    <label>Secretário / Responsável</label>
                    <input type="text" class="form-control text-uppercase" name="secretario" id="secretario">
                </div>
            </div>
        </form>
    </div> -->
@stop
