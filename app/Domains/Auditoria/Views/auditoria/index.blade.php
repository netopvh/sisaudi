@extends('layouts.app')

@section('page-header')
    <div class="page-header page-header-default page-header-sm" style="border-left: 1px solid #ddd; border-right: 1px solid #ddd;">
        <div class="page-header-content">
            <div class="page-title">
                <h5><span class="text-semibold">Auditorias</span></h5>
            </div>
        </div>
        {{ Breadcrumbs::render('processo.index') }}
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default border-grey">
                <div class="panel-heading">
                    @permission('criar-usuario')
                    <a href="{{ route('auditoria.create') }}" class="btn btn-sm btn-primary legitRipple">
                        <i class="icon-database-add"></i>
                        <span>Novo Registro</span>
                    </a>
                    @endpermission
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse" class=""></a></li>
                            <li><a data-action="reload"></a></li>
                        </ul>
                    </div>
                </div>
                <table class="table table-bordered table-condensed table-hover" id="tblAuditoria">
                    <thead>
                    <tr>
                        <th>Status</th>
                        <th>Início Proposto</th>
                        <th>Fim Proposto</th>
                        <th>Ações</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div id="confirmProcesso" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Confirmação</h5>
                </div>

                <div class="modal-body">
                    <p>Selecione a opção abaixo conforme deseja executar a ação.</p>
                </div>

                <div class="modal-footer">
                    <a href="{{ route('processo.create',['action' => 'new']) }}" type="button" class="btn btn-info">Novo Processo</a>
                    <a href="{{ route('processo.create',['action' => 'exists']) }}" type="button" class="btn btn-primary">Processo Existente</a>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
@stop
