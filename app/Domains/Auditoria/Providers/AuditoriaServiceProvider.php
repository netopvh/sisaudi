<?php
namespace App\Domains\Auditoria\Providers;

use App\Domains\Auditoria\Repositories\AuditoriaRepositoryEloquent;
use App\Domains\Auditoria\Repositories\Contracts\AuditoriaRepository;
use App\Domains\Auditoria\Repositories\Contracts\CriterioRepository;
use App\Domains\Auditoria\Repositories\Contracts\DemandaRepository;
use App\Domains\Auditoria\Repositories\Contracts\FollowUpStatusRepository;
use App\Domains\Auditoria\Repositories\Contracts\FonteInformacaoRepository;
use App\Domains\Auditoria\Repositories\Contracts\FundamentacaoLegalRepository;
use App\Domains\Auditoria\Repositories\Contracts\MetodologiaRepository;
use App\Domains\Auditoria\Repositories\Contracts\NaturezaRepository;
use App\Domains\Auditoria\Repositories\Contracts\PossivelAchadoRepository;
use App\Domains\Auditoria\Repositories\Contracts\ProcessoMatrizRepository;
use App\Domains\Auditoria\Repositories\Contracts\ProcessoRepository;
use App\Domains\Auditoria\Repositories\Contracts\ProcessoTipoRepository;
use App\Domains\Auditoria\Repositories\Contracts\RecomendacaoCategoriaRepository;
use App\Domains\Auditoria\Repositories\Contracts\RecomendacaoGravidadeRepository;
use App\Domains\Auditoria\Repositories\Contracts\RecomendacaoTipoRepository;
use App\Domains\Auditoria\Repositories\Contracts\RiscoRepository;
use App\Domains\Auditoria\Repositories\Contracts\TipoDocumentoRepository;
use App\Domains\Auditoria\Repositories\Contracts\UnidadeAdministrativaRepository;
use App\Domains\Auditoria\Repositories\CriterioRepositoryEloquent;
use App\Domains\Auditoria\Repositories\DemandaRepositoryEloquent;
use App\Domains\Auditoria\Repositories\FollowUpStatusRepositoryEloquent;
use App\Domains\Auditoria\Repositories\FonteInformacaoRepositoryEloquent;
use App\Domains\Auditoria\Repositories\FundamentacaoLegalRepositoryEloquent;
use App\Domains\Auditoria\Repositories\MetodologiaRepositoryEloquent;
use App\Domains\Auditoria\Repositories\NaturezaRepositoryEloquent;
use App\Domains\Auditoria\Repositories\PossivelAchadoRepositoryEloquent;
use App\Domains\Auditoria\Repositories\ProcessoMatrizRepositoryEloquent;
use App\Domains\Auditoria\Repositories\ProcessoRepositoryEloquent;
use App\Domains\Auditoria\Repositories\ProcessoTipoRepositoryEloquent;
use App\Domains\Auditoria\Repositories\RecomendacaoCategoriaRepositoryEloquent;
use App\Domains\Auditoria\Repositories\RecomendacaoGravidadeRepositoryEloquent;
use App\Domains\Auditoria\Repositories\RecomendacaoTipoRepositoryEloquent;
use App\Domains\Auditoria\Repositories\RiscoRepositoryEloquent;
use App\Domains\Auditoria\Repositories\TipoDocumentoRepositoryEloquent;
use App\Domains\Auditoria\Repositories\UnidadeAdministrativaRepositoryEloquent;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class AuditoriaServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Domains\Auditoria\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Register all repositories for this module
     *
     */
    public function register()
    {
        $this->app->bind(RecomendacaoTipoRepository::class,RecomendacaoTipoRepositoryEloquent::class);
        $this->app->bind(RecomendacaoCategoriaRepository::class,RecomendacaoCategoriaRepositoryEloquent::class);
        $this->app->bind(RecomendacaoGravidadeRepository::class,RecomendacaoGravidadeRepositoryEloquent::class);
        $this->app->bind(FollowUpStatusRepository::class,FollowUpStatusRepositoryEloquent::class);
        $this->app->bind(UnidadeAdministrativaRepository::class,UnidadeAdministrativaRepositoryEloquent::class);
        $this->app->bind(TipoDocumentoRepository::class,TipoDocumentoRepositoryEloquent::class);
        $this->app->bind(PossivelAchadoRepository::class,PossivelAchadoRepositoryEloquent::class);
        $this->app->bind(RiscoRepository::class,RiscoRepositoryEloquent::class);
        $this->app->bind(ProcessoRepository::class,ProcessoRepositoryEloquent::class);
        $this->app->bind(AuditoriaRepository::class,AuditoriaRepositoryEloquent::class);
        $this->app->bind(ProcessoMatrizRepository::class,ProcessoMatrizRepositoryEloquent::class);
        $this->app->bind(DemandaRepository::class,DemandaRepositoryEloquent::class);
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(app_path('Domains/Auditoria/Routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(app_path('Domains/Auditoria/Routes/api.php'));
    }
}
