<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 28/08/2018
 * Time: 19:32
 */

namespace App\Domains\Auditoria\Services;


use App\Core\Exceptions\GeneralException;
use App\Domains\Auditoria\Repositories\Contracts\ProcessoRepository;
use App\Domains\Auditoria\Validators\ProcessoValidator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use App\Domains\Auditoria\Models\ProcessoField;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Support\Facades\DB;

class ProcessoService
{

    /**
     * @var ProcessoRepository
     */
    private $repository;

    /**
     * @var ProcessoValidator
     */
    private $validator;

    /**
     * ProcessoService constructor.
     * @param ProcessoRepository $repository
     * @param ProcessoValidator $validator
     */
    public function __construct(
        ProcessoRepository $repository,
        ProcessoValidator $validator
    )
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    public function store(array $data)
    {
        try {
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
            //dd($data);
            $processo = $this->dataFields($data, 'C');
            $this->attachRelations($processo, $data);

            return [
                'success' => true,
                'messages' => config('messages.create'),
                'data' => $processo->id
            ];

        } catch (\Exception $e) {
            switch (get_class($e)) {
                case ValidatorException::class:
                    return ['success' => false, 'messages' => $e->getMessageBag()];
                    break;
                case GeneralException::class:
                    return ['success' => false, 'messages' => $e->getMessage()];
                    break;
                default:
                    return ['success' => false, 'messages' => $e->getMessage()];
                    break;
            }
        }
    }

    public function show($id)
    {
        try {

            $processo = $this->getProcessoDeptWithRelations($id, ['fields','criador', 'questoes.subquestoes'],'S');
            return [
                'success' => true,
                'data' => $processo
            ];

        } catch (\Exception $e) {
            switch (get_class($e)) {
                case ModelNotFoundException::class:
                    return ['success' => false, 'messages' => 'Nenhum Registro localizado no Sistema ou você não tem permissão.'];
                    break;
                default:
                    return ['success' => false, 'messages' => $e->getMessage()];
                    break;
            }
        }
    }

    public function edit($id)
    {
        try {

            $processo = $this->getProcessoDeptWithRelations($id, ['auditores', 'auditados']);

            $this->verifyUserIsPermission($processo);

            return [
                'success' => true,
                'data' => $processo
            ];

        } catch (\Exception $e) {
            switch (get_class($e)) {
                case ModelNotFoundException::class:
                    return ['success' => false, 'messages' => 'Nenhum Registro localizado no Sistema ou você não tem permissão.'];
                    break;
                default:
                    return ['success' => false, 'messages' => $e->getMessage()];
                    break;
            }
        }
    }
    
    public function update(array $data, $id)
    {
        try {
            $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $processo = $this->repository->update($this->dataFields($data, 'U'), $id);
            $this->updateRelations($processo, $data);

            return [
                'success' => true,
                'messages' => config('messages.update'),
                'data' => $processo->id
            ];

        } catch (\Exception $e) {
            switch (get_class($e)) {
                case ValidatorException::class:
                    return ['success' => false, 'messages' => $e->getMessageBag()];
                    break;
                case GeneralException::class:
                    return ['success' => false, 'messages' => $e->getMessage()];
                    break;
            }
        }
    }

    private function verifyUserIsPermission($attributes){
        $usersPermited = $attributes->auditores->pluck('id')->toArray();
        if(! in_array(auth()->user()->id, $usersPermited)){
            throw new GeneralException('Não possui permissão para executar esta ação');
        }
    }

    private function getProcessoDeptWithRelations($id, array $relations, $action = 'E'){
        $processo =  $this->repository->with($relations)
            ->query()
            ->departamento()
            ->findOrFail($id);

        if ($action == 'E'){
            $items = new \stdClass();
            foreach ($processo->fields->pluck('content', 'name')->toArray() as $key => $value) {
                $items->$key = $value;
            }
            unset($processo->fields);
            $processo->fields = $items;
            return $processo;
        }

        return $processo;
    }

    private function dataFields(array $data, $action)
    {
        if ($action == 'C') {
            $data['numero_processo'] = $this->getNumeroProcesso($data['numero_processo']);
            $data['autuacao'] = $this->getDateAutuacao($data['autuacao']);
            $data['criador_id'] = auth()->user()->id;
            $data['tipo'] = 'A';
        }
        $data['inicio_proposto'] = date_to_database($data['inicio_proposto']);
        $data['fim_proposto'] = date_to_database($data['fim_proposto']);
        $data['data_portaria_inicial'] = date_to_database($data['data_portaria_inicial']);
        $processo = $this->repository->create($data);

        if ($processo->tipo_processo == 'APG') {
            $descSum = text_to_array_fields($data['desc_sumaria']);
            $exercAnt = text_to_array_fields($data['exercicio_anterior']);
            $exercAtual = text_to_array_fields($data['exercicio_atual']);
            unset($data['desc_sumaria'], $data['exercicio_anterior'], $data['exercicio_atual']);
            if (count($descSum) == 7) {
                $processo->fields()->saveMany([
                    new ProcessoField(['name' => 'escopo', 'content' => $data['escopo']]),
                    new ProcessoField(['name' => 'org_resp', 'content' => $descSum[0]]),
                    new ProcessoField(['name' => 'objetivos', 'content' => $descSum[1]]),
                    new ProcessoField(['name' => 'metas_loa', 'content' => $descSum[2]]),
                    new ProcessoField(['name' => 'metas_gestor', 'content' => $descSum[3]]),
                    new ProcessoField(['name' => 'lista_docs_obtida', 'content' => $descSum[4]]),
                    new ProcessoField(['name' => 'razoes_implantacao', 'content' => $descSum[5]]),
                    new ProcessoField(['name' => 'divisoes_procedimentos', 'content' => $descSum[6]]),
                    new ProcessoField(['name' => 'compara_metas', 'content' => $exercAnt[0]]),
                    new ProcessoField(['name' => 'conc_exec_gestor', 'content' => $exercAnt[1]]),
                    new ProcessoField(['name' => 'resumo_prin_problemas', 'content' => $exercAnt[2]]),
                    new ProcessoField(['name' => 'prov_controle', 'content' => $exercAnt[3]]),
                    new ProcessoField(['name' => 'questoes_relevantes', 'content' => $exercAnt[4]]),
                    new ProcessoField(['name' => 'recursos_ini_gestor', 'content' => $exercAtual[0]]),
                    new ProcessoField(['name' => 'exec_plan', 'content' => $exercAtual[1]]),
                    new ProcessoField(['name' => 'avaliacao_sum_plan', 'content' => $exercAtual[2]]),
                    new ProcessoField(['name' => 'aspect_op_rel', 'content' => $exercAtual[3]]),
                ]);
            }
        }
        return $processo;
    }

    private function attachRelations(Model $model, array $data)
    {
        $model->auditados()->attach($data['auditados']);
        $model->auditores()->attach($data['auditores']);
    }

    private function updateRelations(Model $model, array $data)
    {
        $model->auditados()->detach();
        $model->auditores()->detach();
        $model->auditados()->attach($data['auditados']);
        $model->auditores()->attach($data['auditores']);
    }

    private function getNumeroProcesso($attribute = null)
    {
        $numProcesso = is_null($attribute) ? $this->geraNumeroProcesso() : $attribute;
        $result = $this->repository->findWhere(['numero_processo' => $numProcesso]);
        if (count($result) >= 1) {
            throw new GeneralException("Processo já cadastrado no Sistema");
        } else {
            return $numProcesso;
        }
    }

    public function getDateAutuacao($date = null)
    {
        return !is_null($date) ? date_to_database($date) : date('Y-m-d');
    }

    private function geraNumeroProcesso()
    {
        $ultimoProcesso = $this->repository->query()->get(['numero_processo'])->last()['numero_processo'];
        if (is_null($ultimoProcesso)) {
            $numAtual = DB::table('configuracoes')->where('item', '=', 'num_processo_atual')->get(['valor'])->first();
            return '03.' . $numAtual->valor . '/' . date('Y');

        } else {
            $ultimoProcesso = $this->repository->query()->get(['numero_processo'])->last();
            $this->numeroProcesso = last_num_processo($ultimoProcesso->numero_processo);
            $processo = '03.' . $this->incNumeroProcesso($this->numeroProcesso) . '/' . date('Y');
            return $processo;
        }
    }

    private function incNumeroProcesso($numero)
    {
        $valor = 1;
        return $valor = $valor + $numero;
    }
}