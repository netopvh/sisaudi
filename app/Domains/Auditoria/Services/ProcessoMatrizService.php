<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 05/09/2018
 * Time: 13:05
 */

namespace App\Domains\Auditoria\Services;


use App\Domains\Auditoria\Repositories\Contracts\ProcessoMatrizRepository;
use App\Domains\Auditoria\Repositories\Contracts\ProcessoRepository;

class ProcessoMatrizService
{
    protected $processoRepository;

    protected $processoMatrizRepository;

    public function __construct(
        ProcessoRepository $processoRepository,
        ProcessoMatrizRepository $processoMatrizRepository
    )
    {
        $this->processoRepository = $processoRepository;
        $this->processoMatrizRepository = $processoMatrizRepository;
    }

    public function edit($idProcesso, $idQuestao)
    {
        try {

            $processo = $this->getProcessoDeptWithRelations($idProcesso, ['questoes.subquestoes']);
            $questao = $processo->questoes()->find($idQuestao);
            if(is_null($questao)){
                $data = $this->processoMatrizRepository->whereHas('subquestoes', function ($q) use ($idQuestao){
                    return $q->where('id',$idQuestao);
                })->get();
                dd($data);
                $type = 'sub';
            }else{
                $data = $questao;
                $type = 'questao';
            }

            //$this->verifyUserIsPermission($processo);

            return [
                'success' => true,
                'type' => $type,
                'data' => $data
            ];

        } catch (\Exception $e) {
            switch (get_class($e)) {
                case ModelNotFoundException::class:
                    return ['success' => false, 'messages' => 'Nenhum Registro localizado no Sistema ou você não tem permissão.'];
                    break;
                default:
                    return ['success' => false, 'messages' => $e->getMessage()];
                    break;
            }
        }
    }

    private function getProcessoDeptWithRelations($id, array $relations){
        return $this->processoRepository->with($relations)
            ->query()
            ->departamento()
            ->findOrFail($id);
    }
}