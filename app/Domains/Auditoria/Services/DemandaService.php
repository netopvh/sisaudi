<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 15/10/2018
 * Time: 18:52
 */

namespace App\Domains\Auditoria\Services;


use App\Domains\Auditoria\Events\DemandaCreated;
use App\Domains\Auditoria\Repositories\Contracts\DemandaRepository;
use App\Domains\Auditoria\Validators\DemandaValidator;
use App\Domains\Auditoria\Events\DemandaFileInserted;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;

class DemandaService
{

    protected $repository;

    protected $validator;

    public function __construct(
        DemandaValidator $validator,
        DemandaRepository $repository
    )
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    public function store(array $attributes)
    {
        try {
            $this->validator->with($attributes)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $demanda = $this->setFields($attributes);

            event(new DemandaCreated($demanda));

            return [
                'success' => true,
                'messages' => config('messages.create'),
                'data' => $demanda->id
            ];

        } catch (\Exception $e) {
            switch (get_class($e)) {
                case ValidatorException::class:
                    return ['success' => false, 'messages' => $e->getMessageBag()];
                    break;
                case GeneralException::class:
                    return ['success' => false, 'messages' => $e->getMessage()];
                    break;
                default:
                    return ['success' => false, 'messages' => $e->getMessage()];
                    break;
            }
        }
    }

    public function show($id)
    {
        try {

            $demanda = $this->repository->with('interessados')->find($id);
            return [
                'success' => true,
                'data' => $demanda
            ];

        } catch (\Exception $e) {
            switch (get_class($e)) {
                case ModelNotFoundException::class:
                    return ['success' => false, 'messages' => 'Nenhum Registro localizado no Sistema ou você não tem permissão.'];
                    break;
                default:
                    return ['success' => false, 'messages' => $e->getMessage()];
                    break;
            }
        }
    }

    public function storeFiles($id, Request $attributes)
    {
        try {

            if ($attributes->tipo_file == 'A'){
                $result = $this->insertUploaded($id, $attributes);

                event(new DemandaFileInserted($result));

                return [
                    'success' => true,
                    'messages' => config('messages.create'),
                    'data' => $result->id
                ];
            }elseif($attributes->tipo_file == 'T'){
                $result = $this->insertText($id, $attributes->all());

                event(new DemandaFileInserted($result));

                return [
                    'success' => true,
                    'messages' => config('messages.create'),
                    'data' => $result->id
                ];
            }

        } catch (\Exception $e) {
            switch (get_class($e)) {
                case ModelNotFoundException::class:
                    return ['success' => false, 'messages' => 'Nenhum Registro localizado no Sistema ou você não tem permissão.'];
                    break;
                default:
                    return ['success' => false, 'messages' => $e->getMessage()];
                    break;
            }
        }
    }

    public function showFile($id, $file)
    {
        try {

            $demanda = $this->repository->with('files')->find($id);
            $file = $demanda->files()->find($file);
            return [
                'success' => true,
                'data' => $file
            ];

        } catch (\Exception $e) {
            switch (get_class($e)) {
                case ModelNotFoundException::class:
                    return ['success' => false, 'messages' => 'Nenhum Registro localizado no Sistema ou você não tem permissão.'];
                    break;
                default:
                    return ['success' => false, 'messages' => $e->getMessage()];
                    break;
            }
        }
    }

    private function insertText($id, array $attributes)
    {
        unset($attributes['path']);
        $demanda = $this->repository->find($id);
        $demanda->files()->create($attributes);

        return $demanda;
    }

    private function insertUploaded($id, Request $attributes)
    {
        $demanda = $this->repository->find($id);
        $demanda->files()->create([
            'tipo_doc_id' => $attributes->tipo_doc_id,
            'tipo_file' => $attributes->tipo_file,
            'descricao' => $attributes->descricao,
            'path' => $this->fileUpload($attributes),
            'prazo' => ($attributes->has('prazo') && $attributes->get('prazo') != '')? $attributes->get('prazo') : 15
        ]);
        return $demanda;
    }

    private function fileUpload(Request $request)
    {
        $fileName =  $request->file('path')->store('demanda','public');
        return $fileName;
    }

    private function setFields(array $fields)
    {
        $fields['criador_id'] = auth()->user()->id;
        $demanda = $this->repository->create($fields);
        if ($demanda){
            $demanda->interessados()->attach($fields['interessados']);
            return $demanda;
        }
    }

}