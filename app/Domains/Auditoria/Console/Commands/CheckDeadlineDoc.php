<?php

namespace App\Domains\Auditoria\Console\Commands;

use App\Domains\Access\Models\User;
use App\Domains\Auditoria\Models\Demanda;
use App\Domains\Auditoria\Notifications\SendNotificationDeadline;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class CheckDeadlineDoc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:docs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando que verifica os prazos dos documentos cadastrados';

    protected $demanda;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Demanda $demanda)
    {
        parent::__construct();

        $this->demanda = $demanda;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $allDemands = $this->demanda
            ->with('files:demanda_id,descricao,prazo,created_at')
            ->get(['id','criador_id']);

        $this->checkDate($allDemands);
        //Notification::

        //Log::info($allFIles);
    }

    private function checkDate($data)
    {
        $today = Carbon::now()->format('Y-m-d');
        $items = collect();
        foreach ($data as $demand){
            foreach ($demand->files as $file) {
                if($result = $this->diffDate($file->created_at->format('Y-m-d'),$today)){
                    if($result < $file->prazo){
                        $items->push(['name' => $file->descricao,'vencimento' => Carbon::instance($file->created_at)->addDays($file->prazo)->format('d/m/Y')]);
                        //User::find($demand->criador->id)->notify(new SendNotificationDeadline($items));
                    }
                }
            }
        }
        $this->info($items);
    }

    private function diffDate($start, $end)
    {
        $first = date_create($start);
        $last = date_create($end);
        $diff = date_diff($first, $last);

        return $diff->format('%a');
    }
}
