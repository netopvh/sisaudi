<?php

namespace App\Domains\Auditoria\Events;

use App\Domains\Auditoria\Models\Demanda;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DemandaCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $demanda;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Demanda $demanda)
    {
        $this->demanda = $demanda;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
