<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 29/08/2018
 * Time: 10:41
 */

namespace App\Domains\Auditoria\ViewComposers;

use Illuminate\View\View;
use App\Domains\Auditoria\Enum\ModalidadeAuditoriaEnum;

class ModalidadeAuditoriaComposer
{

    protected $attributes;

    public function __construct()
    {
        $this->attributes = ModalidadeAuditoriaEnum::toArray();
    }

    public function compose(View $view)
    {
        $view->with('modalidades', $this->attributes);
    }

}