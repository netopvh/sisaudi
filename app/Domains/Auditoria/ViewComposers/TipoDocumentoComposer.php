<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 29/08/2018
 * Time: 10:41
 */

namespace App\Domains\Auditoria\ViewComposers;

use App\Domains\Auditoria\Enum\ProcessoTipoEnum;
use App\Domains\Auditoria\Repositories\Contracts\TipoDocumentoRepository;
use Illuminate\View\View;

class TipoDocumentoComposer
{

    protected $repository;

    public function __construct(TipoDocumentoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function compose(View $view)
    {
        $view->with('tipoDocs', $this->repository->all());
    }

}