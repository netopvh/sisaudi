<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 29/08/2018
 * Time: 10:14
 */

namespace App\Domains\Auditoria\ViewComposers;

use Illuminate\View\View;
use App\Domains\Auditoria\Repositories\Contracts\UnidadeAdministrativaRepository;

class UnidadeAdministrativaComposer
{

    /**
     * @var UnidadeAdministrativaRepository
     */
    protected $repository;

    /**
     * UnidadeAdministrativaComposer constructor.
     * @param UnidadeAdministrativaRepository $repository
     */
    public function __construct(UnidadeAdministrativaRepository $repository)
    {
        $this->repository = $repository;
    }

    public function compose(View $view)
    {
        $view->with('unidadesAdministrativas', $this->repository->all());
    }
    
}