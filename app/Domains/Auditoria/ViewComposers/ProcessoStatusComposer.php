<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 29/08/2018
 * Time: 10:41
 */

namespace App\Domains\Auditoria\ViewComposers;

use App\Domains\Auditoria\Enum\ProcessoStatusEnum;
use Illuminate\View\View;

class ProcessoStatusComposer
{

    protected $attributes;

    public function __construct()
    {
        $this->attributes = ProcessoStatusEnum::toArray();
    }

    public function compose(View $view)
    {
        $view->with('status', $this->attributes);
    }

}