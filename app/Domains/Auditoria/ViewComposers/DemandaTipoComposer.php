<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 29/08/2018
 * Time: 10:41
 */

namespace App\Domains\Auditoria\ViewComposers;

use App\Domains\Auditoria\Enum\DemandaTipoEnum;

use Illuminate\View\View;

class DemandaTipoComposer
{

    protected $attributes;

    public function __construct()
    {
        $this->attributes = DemandaTipoEnum::toArray();
    }

    public function compose(View $view)
    {
        $view->with('demandaTipos', $this->attributes);
    }

}