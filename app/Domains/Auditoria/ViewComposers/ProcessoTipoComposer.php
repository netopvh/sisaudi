<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 29/08/2018
 * Time: 10:41
 */

namespace App\Domains\Auditoria\ViewComposers;

use App\Domains\Auditoria\Enum\ProcessoTipoEnum;
use Illuminate\View\View;

class ProcessoTipoComposer
{

    protected $attributes;

    public function __construct()
    {
        $this->attributes = ProcessoTipoEnum::toArray();
    }

    public function compose(View $view)
    {
        $view->with('processoTipos', $this->attributes);
    }

}