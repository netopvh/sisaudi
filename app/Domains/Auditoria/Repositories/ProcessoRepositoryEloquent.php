<?php

namespace App\Domains\Auditoria\Repositories;

use App\Core\Exceptions\GeneralException;
use App\Core\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Domains\Auditoria\Repositories\Contracts\ProcessoRepository;
use App\Domains\Auditoria\Models\Processo;
use App\Domains\Auditoria\Validators\ProcessoValidator;
use Carbon\Carbon;
use Prettus\Validator\Contracts\ValidatorInterface;

/**
 * Class ProcessoRepositoryEloquent.
 *
 * @package namespace App\Domains\Auditoria\Repositories;
 */
class ProcessoRepositoryEloquent extends BaseRepository implements ProcessoRepository
{

    private $numeroProcesso = '';

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Processo::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
