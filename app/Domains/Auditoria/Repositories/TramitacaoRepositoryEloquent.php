<?php

namespace App\Domains\Auditoria\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Domains\Auditoria\Repositories\Contracts\TramitacaoRepository;
use App\Domains\Auditoria\Models\Tramitacao;
use App\Domains\Auditoria\Validators\TramitacaoValidator;

/**
 * Class TramitacaoRepositoryEloquent.
 *
 * @package namespace App\Domains\Auditoria\Repositories;
 */
class TramitacaoRepositoryEloquent extends BaseRepository implements TramitacaoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Tramitacao::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
