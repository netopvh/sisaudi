<?php

namespace App\Domains\Auditoria\Repositories;

use App\Core\Repositories\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Domains\Auditoria\Repositories\Contracts\RiscoRepository;
use App\Domains\Auditoria\Models\Risco;
use App\Domains\Auditoria\Validators\RiscoValidator;

/**
 * Class RiscoRepositoryEloquent.
 *
 * @package namespace App\Domains\Auditoria\Repositories;
 */
class RiscoRepositoryEloquent extends BaseRepository implements RiscoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Risco::class;
    }

    public function validator()
    {
        return RiscoValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
