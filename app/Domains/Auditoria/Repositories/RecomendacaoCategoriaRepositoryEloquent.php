<?php

namespace App\Domains\Auditoria\Repositories;

use App\Core\Repositories\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Domains\Auditoria\Repositories\Contracts\RecomendacaoCategoriaRepository;
use App\Domains\Auditoria\Models\RecomendacaoCategoria;
use App\Domains\Auditoria\Validators\RecomendacaoCategoriaValidator;

/**
 * Class RecomendacaoCategoriaRepositoryEloquent.
 *
 * @package namespace App\Domains\Auditoria\Repositories;
 */
class RecomendacaoCategoriaRepositoryEloquent extends BaseRepository implements RecomendacaoCategoriaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RecomendacaoCategoria::class;
    }

    /**
     * @return string
     */
    public function validator()
    {
        return RecomendacaoCategoriaValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
