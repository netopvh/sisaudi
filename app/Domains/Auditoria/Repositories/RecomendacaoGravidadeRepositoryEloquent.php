<?php

namespace App\Domains\Auditoria\Repositories;

use App\Core\Repositories\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Domains\Auditoria\Repositories\Contracts\RecomendacaoGravidadeRepository;
use App\Domains\Auditoria\Models\RecomendacaoGravidade;
use App\Domains\Auditoria\Validators\RecomendacaoGravidadeValidator;

/**
 * Class RecomendacaoGravidadeRepositoryEloquent.
 *
 * @package namespace App\Domains\Auditoria\Repositories;
 */
class RecomendacaoGravidadeRepositoryEloquent extends BaseRepository implements RecomendacaoGravidadeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RecomendacaoGravidade::class;
    }

    public function validator()
    {
        return RecomendacaoGravidadeValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
