<?php

namespace App\Domains\Auditoria\Repositories;

use App\Core\Repositories\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Domains\Auditoria\Repositories\Contracts\AuditoriaRepository;
use App\Domains\Auditoria\Models\Auditoria;
use App\Domains\Auditoria\Validators\AuditoriaValidator;

/**
 * Class AuditoriaRepositoryEloquent.
 *
 * @package namespace App\Domains\Auditoria\Repositories;
 */
class AuditoriaRepositoryEloquent extends BaseRepository implements AuditoriaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Auditoria::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
