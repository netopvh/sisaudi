<?php

namespace App\Domains\Auditoria\Repositories;

use App\Core\Repositories\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Domains\Auditoria\Repositories\Contracts\UnidadeAdministrativaRepository;
use App\Domains\Auditoria\Models\UnidadeAdministrativa;
use App\Domains\Auditoria\Validators\UnidadeAdministrativaValidator;

/**
 * Class UnidadeAdministrativaRepositoryEloquent.
 *
 * @package namespace App\Domains\Auditoria\Repositories;
 */
class UnidadeAdministrativaRepositoryEloquent extends BaseRepository implements UnidadeAdministrativaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UnidadeAdministrativa::class;
    }

    public function validator()
    {
        return UnidadeAdministrativaValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
