<?php

namespace App\Domains\Auditoria\Repositories\Contracts;

use App\Core\Repositories\BaseRepositoryContract;

/**
 * Interface RecomendacaoCategoriaRepository.
 *
 * @package namespace App\Domains\Auditoria\Repositories\Contracts;
 */
interface RecomendacaoCategoriaRepository extends BaseRepositoryContract
{
    //
}
