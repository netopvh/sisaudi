<?php

namespace App\Domains\Auditoria\Repositories\Contracts;

use App\Core\Repositories\BaseRepositoryContract;

/**
 * Interface RecomendacaoTipoRepository.
 *
 * @package namespace App\Domains\Auditoria\Repositories\Contracts;
 */
interface RecomendacaoTipoRepository extends BaseRepositoryContract
{
    //
}
