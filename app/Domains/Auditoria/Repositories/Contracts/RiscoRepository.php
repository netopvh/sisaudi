<?php

namespace App\Domains\Auditoria\Repositories\Contracts;

use App\Core\Repositories\BaseRepositoryContract;

/**
 * Interface RiscoRepository.
 *
 * @package namespace App\Domains\Auditoria\Repositories\Contracts;
 */
interface RiscoRepository extends BaseRepositoryContract
{
    //
}
