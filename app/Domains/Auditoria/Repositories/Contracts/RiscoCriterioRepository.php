<?php

namespace App\Domains\Auditoria\Repositories\Contracts;

use App\Core\Repositories\BaseRepositoryContract;

/**
 * Interface RiscoCriterioRepository.
 *
 * @package namespace App\Domains\Auditoria\Repositories\Contracts;
 */
interface RiscoCriterioRepository extends BaseRepositoryContract
{
    //
}
