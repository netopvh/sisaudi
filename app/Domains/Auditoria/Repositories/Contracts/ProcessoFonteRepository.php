<?php

namespace App\Domains\Auditoria\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProcessoFonteRepository.
 *
 * @package namespace App\Domains\Auditoria\Repositories\Contracts;
 */
interface ProcessoFonteRepository extends RepositoryInterface
{
    //
}
