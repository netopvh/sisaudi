<?php

namespace App\Domains\Auditoria\Repositories\Contracts;

use App\Core\Repositories\BaseRepositoryContract;

/**
 * Interface FollowUpStatusRepository.
 *
 * @package namespace App\Domains\Auditoria\Repositories\Contracts;
 */
interface FollowUpStatusRepository extends BaseRepositoryContract
{
    //
}
