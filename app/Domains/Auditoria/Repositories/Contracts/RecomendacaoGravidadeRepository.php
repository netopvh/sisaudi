<?php

namespace App\Domains\Auditoria\Repositories\Contracts;

use App\Core\Repositories\BaseRepositoryContract;

/**
 * Interface RecomendacaoGravidadeRepository.
 *
 * @package namespace App\Domains\Auditoria\Repositories\Contracts;
 */
interface RecomendacaoGravidadeRepository extends BaseRepositoryContract
{
    //
}
