<?php

namespace App\Domains\Auditoria\Repositories\Contracts;

use App\Core\Repositories\BaseRepositoryContract;

/**
 * Interface DemandaFileRepository.
 *
 * @package namespace App\Domains\Auditoria\Repositories\Contracts;
 */
interface DemandaFileRepository extends BaseRepositoryContract
{
    //
}
