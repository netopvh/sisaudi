<?php

namespace App\Domains\Auditoria\Repositories\Contracts;

use App\Core\Repositories\BaseRepositoryContract;

/**
 * Interface UnidadeAdministrativaRepository.
 *
 * @package namespace App\Domains\Auditoria\Repositories\Contracts;
 */
interface UnidadeAdministrativaRepository extends BaseRepositoryContract
{
    //
}
