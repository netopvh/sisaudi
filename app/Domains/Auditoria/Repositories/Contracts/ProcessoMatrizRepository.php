<?php

namespace App\Domains\Auditoria\Repositories\Contracts;

use App\Core\Repositories\BaseRepositoryContract;

/**
 * Interface ProcessoMatrizRepository.
 *
 * @package namespace App\Domains\Auditoria\Repositories\Contracts;
 */
interface ProcessoMatrizRepository extends BaseRepositoryContract
{
    //
}
