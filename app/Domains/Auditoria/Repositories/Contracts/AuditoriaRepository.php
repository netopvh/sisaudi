<?php

namespace App\Domains\Auditoria\Repositories\Contracts;

use App\Core\Repositories\BaseRepositoryContract;

/**
 * Interface AuditoriaRepository.
 *
 * @package namespace App\Domains\Auditoria\Repositories\Contracts;
 */
interface AuditoriaRepository extends BaseRepositoryContract
{
    //
}
