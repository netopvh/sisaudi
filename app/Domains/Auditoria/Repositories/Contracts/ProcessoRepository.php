<?php

namespace App\Domains\Auditoria\Repositories\Contracts;

use App\Core\Repositories\BaseRepositoryContract;

/**
 * Interface ProcessoRepository.
 *
 * @package namespace App\Domains\Auditoria\Repositories\Contracts;
 */
interface ProcessoRepository extends BaseRepositoryContract
{

}
