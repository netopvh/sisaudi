<?php

namespace App\Domains\Auditoria\Repositories\Contracts;

use App\Core\Repositories\BaseRepositoryContract;

/**
 * Interface PossivelAchadoRepository.
 *
 * @package namespace App\Domains\Auditoria\Repositories\Contracts;
 */
interface PossivelAchadoRepository extends BaseRepositoryContract
{
    //
}
