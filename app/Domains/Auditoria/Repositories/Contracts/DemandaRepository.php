<?php

namespace App\Domains\Auditoria\Repositories\Contracts;

use App\Core\Repositories\BaseRepositoryContract;

/**
 * Interface DemandaRepository.
 *
 * @package namespace App\Domains\Auditoria\Repositories\Contracts;
 */
interface DemandaRepository extends BaseRepositoryContract
{
    //
}
