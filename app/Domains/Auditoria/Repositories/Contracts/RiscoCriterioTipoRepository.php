<?php

namespace App\Domains\Auditoria\Repositories\Contracts;

use App\Core\Repositories\BaseRepositoryContract;

/**
 * Interface RiscoCriterioTipoRepository.
 *
 * @package namespace App\Domains\Auditoria\Repositories\Contracts;
 */
interface RiscoCriterioTipoRepository extends BaseRepositoryContract
{
    //
}
