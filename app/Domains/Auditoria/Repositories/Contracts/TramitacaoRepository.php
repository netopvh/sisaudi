<?php

namespace App\Domains\Auditoria\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TramitacaoRepository.
 *
 * @package namespace App\Domains\Auditoria\Repositories\Contracts;
 */
interface TramitacaoRepository extends RepositoryInterface
{
    //
}
