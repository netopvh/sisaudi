<?php

namespace App\Domains\Auditoria\Repositories\Contracts;

use App\Core\Repositories\BaseRepositoryContract;

/**
 * Interface TipoDocumentoRepository.
 *
 * @package namespace App\Domains\Auditoria\Repositories\Contracts;
 */
interface TipoDocumentoRepository extends BaseRepositoryContract
{
    //
}
