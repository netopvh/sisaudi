<?php

namespace App\Domains\Auditoria\Repositories;

use App\Core\Repositories\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Domains\Auditoria\Repositories\Contracts\FollowUpStatusRepository;
use App\Domains\Auditoria\Models\FollowUpStatus;
use App\Domains\Auditoria\Validators\FollowUpStatusValidator;

/**
 * Class FollowUpStatusRepositoryEloquent.
 *
 * @package namespace App\Domains\Auditoria\Repositories;
 */
class FollowUpStatusRepositoryEloquent extends BaseRepository implements FollowUpStatusRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return FollowUpStatus::class;
    }

    public function validator()
    {
        return FollowUpStatusValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
