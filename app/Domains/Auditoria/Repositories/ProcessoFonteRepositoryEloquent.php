<?php

namespace App\Domains\Auditoria\Repositories;

use App\Core\Repositories\BaseRepository;
use App\Domains\Auditoria\Validators\ProcessoValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Domains\Auditoria\Repositories\Contracts\ProcessoFonteRepository;
use App\Domains\Auditoria\Models\ProcessoFonte;
use App\Domains\Auditoria\Validators\ProcessoFonteValidator;

/**
 * Class ProcessoFonteRepositoryEloquent.
 *
 * @package namespace App\Domains\Auditoria\Repositories;
 */
class ProcessoFonteRepositoryEloquent extends BaseRepository implements ProcessoFonteRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProcessoFonte::class;
    }

    /**
     * @return null|string
     */
    public function validator()
    {
        return ProcessoValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
