<?php

namespace App\Domains\Auditoria\Repositories;

use App\Core\Repositories\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Domains\Auditoria\Repositories\Contracts\RecomendacaoTipoRepository;
use App\Domains\Auditoria\Models\RecomendacaoTipo;
use App\Domains\Auditoria\Validators\RecomendacaoTipoValidator;

/**
 * Class RecomendacaoTipoRepositoryEloquent.
 *
 * @package namespace App\Domains\Auditoria\Repositories;
 */
class RecomendacaoTipoRepositoryEloquent extends BaseRepository implements RecomendacaoTipoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RecomendacaoTipo::class;
    }

    public function validator()
    {
        return RecomendacaoTipoValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
