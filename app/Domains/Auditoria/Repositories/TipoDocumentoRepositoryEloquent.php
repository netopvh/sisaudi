<?php

namespace App\Domains\Auditoria\Repositories;

use App\Core\Repositories\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Domains\Auditoria\Repositories\Contracts\TipoDocumentoRepository;
use App\Domains\Auditoria\Models\TipoDocumento;
use App\Domains\Auditoria\Validators\TipoDocumentoValidator;

/**
 * Class TipoDocumentoRepositoryEloquent.
 *
 * @package namespace App\Domains\Auditoria\Repositories;
 */
class TipoDocumentoRepositoryEloquent extends BaseRepository implements TipoDocumentoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TipoDocumento::class;
    }

    public function validator()
    {
        return TipoDocumentoValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
