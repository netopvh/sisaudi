<?php

namespace App\Domains\Auditoria\Repositories;

use App\Core\Repositories\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Domains\Auditoria\Repositories\Contracts\DemandaRepository;
use App\Domains\Auditoria\Models\Demanda;
use App\Domains\Auditoria\Validators\DemandaValidator;

/**
 * Class DemandaRepositoryEloquent.
 *
 * @package namespace App\Domains\Auditoria\Repositories;
 */
class DemandaRepositoryEloquent extends BaseRepository implements DemandaRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Demanda::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
