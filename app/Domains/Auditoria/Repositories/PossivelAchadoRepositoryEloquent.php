<?php

namespace App\Domains\Auditoria\Repositories;

use App\Core\Repositories\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Domains\Auditoria\Repositories\Contracts\PossivelAchadoRepository;
use App\Domains\Auditoria\Models\PossivelAchado;
use App\Domains\Auditoria\Validators\PossivelAchadoValidator;

/**
 * Class PossivelAchadoRepositoryEloquent.
 *
 * @package namespace App\Domains\Auditoria\Repositories;
 */
class PossivelAchadoRepositoryEloquent extends BaseRepository implements PossivelAchadoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PossivelAchado::class;
    }

    public function validator()
    {
        return PossivelAchadoValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
