<?php

namespace App\Domains\Auditoria\Repositories;

use App\Core\Repositories\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Domains\Auditoria\Repositories\Contracts\DemandaFileRepository;
use App\Domains\Auditoria\Models\DemandaFile;
use App\Domains\Auditoria\Validators\DemandaFileValidator;

/**
 * Class DemandaFileRepositoryEloquent.
 *
 * @package namespace App\Domains\Auditoria\Repositories;
 */
class DemandaFileRepositoryEloquent extends BaseRepository implements DemandaFileRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return DemandaFile::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
