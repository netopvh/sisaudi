<?php

namespace App\Domains\Auditoria\Repositories;

use App\Core\Repositories\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Domains\Auditoria\Repositories\Contracts\ProcessoMatrizRepository;
use App\Domains\Auditoria\Models\ProcessoMatriz;
use App\Domains\Auditoria\Validators\ProcessoMatrizValidator;

/**
 * Class ProcessoMatrizRepositoryEloquent.
 *
 * @package namespace App\Domains\Auditoria\Repositories;
 */
class ProcessoMatrizRepositoryEloquent extends BaseRepository implements ProcessoMatrizRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProcessoMatriz::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
