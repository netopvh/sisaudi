<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 14/08/2018
 * Time: 17:57
 */

namespace App\Domains\Auditoria\Traits;


trait BaseAttributesTrait
{
    /**
     * Deixa registro em Maiúsculo
     *
     * @param $value
     */
    public function setNomeAttribute($value)
    {
        $this->attributes['nome'] = mb_strtoupper($value, "UTF-8");
    }

    /**
     * Deixa registro em Maiúsculo
     *
     * @param $value
     */
    public function setDescricaoAttribute($value)
    {
        $this->attributes['descricao'] = mb_strtoupper($value, "UTF-8");
    }
}