<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 08/08/2018
 * Time: 18:45
 */

namespace App\Core\Providers;

use App\Domains\Access\ViewComposers\DepartamentoComposer;
use App\Domains\Access\ViewComposers\UserComposer;
use App\Domains\Auditoria\ViewComposers\DemandaTipoComposer;
use App\Domains\Auditoria\ViewComposers\ProcessoStatusComposer;
use App\Domains\Auditoria\ViewComposers\ProcessoTipoComposer;
use App\Domains\Auditoria\ViewComposers\TipoDocumentoComposer;
use App\Domains\Auditoria\ViewComposers\UnidadeAdministrativaComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        View::composer(['processo.auditoria.create','processo.auditoria.edit','auth.register','auditoria.create'], DepartamentoComposer::class);
        View::composer(['processo.auditoria.create','processo.auditoria.edit','auditoria.auditoria.create','demanda.index'], UserComposer::class);
        View::composer(['processo.auditoria.create','processo.auditoria.edit','auditoria.create','demanda.create'], UnidadeAdministrativaComposer::class);
        View::composer(['processo.auditoria.create','processo.auditoria.edit'], ProcessoTipoComposer::class);
        View::composer(['processo.auditoria._contents.matriz','processo.auditoria._contents.dados'], ProcessoStatusComposer::class);
        View::composer(['demanda.create'], DemandaTipoComposer::class);
        View::composer(['demanda.files.create'], TipoDocumentoComposer::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}