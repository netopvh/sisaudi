<?php
/**
 * Created by PhpStorm.
 * User: Neto
 * Date: 28/08/2018
 * Time: 19:32
 */

namespace App\Core\Services;


abstract class BaseService
{
    abstract protected function getAll(array $attributes = ['*']);
    abstract protected function findExists(string $field, string $value);
}